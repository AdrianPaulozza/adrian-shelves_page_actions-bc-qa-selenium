import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system


from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from pages.core.v2.shelves import ShelvesPage


@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C69369: Shelves availability results page")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/70418", "TestRail")
class TestC69369:
    def test_C69369(self):
        # Perform a Shelves Availability search on a shelf with many items of different format and availability status.
        # Verify that each result on the Find Available Titles search has an Available status
        self.name ="carl5555"
        self.password = "5555"
        base_url = "https://coaldale.demo.bibliocommons.com"
        home_page = HomePage(self.driver, base_url).open()
        home_page.header.log_in(self.name, self.password)
        home_page.header.login_state_user_logged_in.click()

        # Go to shelf with available items
        home_page.header.completed_shelf.click()
        shelves_page = ShelvesPage(self.driver)

        # Click Find Available Titles button
        shelves_page.find_available_button.click()
        search_results = SearchResultsPage(self.driver)

        #Assert that search results header shows correct shelf
        search_results.shelves_availability_header.text.should.equal("Titles from my Completed shelf")

        search_results.wait.until(lambda s: len(search_results.search_result_items) > 0)

        # Check that all of the items are available.
        # NOTICE - CURRENTLY _ALL_ DIGITAL ITEMS (EBOOKS, AUDIOBOOKS ETC) WILL SHOW, EVEN WHEN UNAVAILABLE
        for item in(search_results.search_result_items):

            if "eBook" or "Downloadable Audiobook" or "Streaming Music" not in item.availability_available.text:

                (item.availability_available.text.startswith("Available")).should.be.true
