import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.bib import BibPage
from pages.core.v2.holds import HoldsPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46024: Select a hold and cancel from /v2/holds")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46024", "TestRail")
class TestC46024:
    def test_C46024(self):
        self.item_id = "2000853126"
        self.barcode = "D059874977"
        self.pin = "60605"
        self.base_url = "https://chipublib.demo.bibliocommons.com"

        bib_page = BibPage(self.driver, self.base_url, item_id=self.item_id).open()
        bib_page.wait.until(lambda s: bib_page.circulation.place_hold.is_displayed())
        bib_page.circulation.place_hold.click()
        bib_page.overlay.login.barcode_input_form.send_keys(self.barcode)
        bib_page.overlay.login.pin_input_form.send_keys(self.pin)
        bib_page.overlay.login.login_button.click()
        bib_page.wait.until(lambda ch: bib_page.circulation.confirm_hold.is_displayed)

        bib_page.circulation.confirm_hold.click()
        bib_page.wait.until(lambda s: bib_page.circulation.cancel_hold.is_displayed())

        holds_page = HoldsPage(self.driver, self.base_url).open()
        holds_page.items[0].cancel.click()
        holds_page.items[0].confirm.click()
        holds_page.wait.until(lambda s: len(holds_page.items) == 0)
        holds_page.wait.until(lambda s: holds_page.holds_notification.is_displayed())
        holds_page.is_success_notification_displayed.should.be.true
        bib_page = BibPage(self.driver, self.base_url, item_id=self.item_id).open()
        bib_page.wait.until(lambda s: bib_page.circulation.place_hold.is_displayed())
        bib_page.circulation.is_place_hold_button_displayed.should.be.true
