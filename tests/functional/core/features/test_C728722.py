import pytest
import allure
import re
import sure
import configuration.system
from pages.core.home import HomePage
from pages.core.v2.checked_out import CheckedOutPage
from utils.bc_test_connector import TestConnector
from utils.bc_api_gateway import BCAPIGatewayBibs


@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C72872: Quick Renewal on Checked out Page")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C728722", "TestRail")


class TestC728722:
    def test_C728722(self):

        site = "https://epltest.stage.bibliocommons.com"

        # Using the api_gateway give me a list of bibs
        bibs = BCAPIGatewayBibs(site).search(term='fc:[*%20TO%20*]', search_type='bl')

        checked_out_bibs = []
        for bib in bibs[:9]:
            checked_out_bibs.append(bib.id)

        self.user = TestConnector.User(base_url=site).create(self.driver, checkouts={
                                                                        'bibs':
                                                                        checked_out_bibs,
                                                                        'due_dates': [
                                                                        '2019-06-05',
                                                                        '2019-06-04',
                                                                        '2019-06-11',
                                                                        '2019-06-11',
                                                                        '2019-06-12',
                                                                        '2019-06-13',
                                                                        '2019-06-13',
                                                                        '2019-06-12',
                                                                        '2019-07-10'
                                                                        ]
                                                                        })

        home_page = HomePage(self.driver, site).open()
        home_page.header.log_in(self.user.barcode, self.user.pin)

        # Navigate to the checked out page and wait until the list is displayed
        checked_out_page = CheckedOutPage(self.driver, site).open()
        checked_out_page.wait.until(lambda s: checked_out_page.is_list_displayed)

        # Click on the overdue filter from the Borrowing navigation bar
        checked_out_page.my_borrowing.checked_out_side_bar.over_due.click()
        checked_out_page.wait.until(lambda s: checked_out_page.is_over_due_title_displayed)

        # Click on the quick renew dropdown and renew all "overdue" items
        checked_out_page.quick_renew_to("overdue")

        # Check that the first item in the list has been renewed
        checked_out_page.wait.until(lambda s: checked_out_page.items[0].is_renew_status_displayed)

        # Convert the 'Renewed' string to an integer
        renewed_count = re.search('\d+', checked_out_page.items[0].renew_status.text)
        renewed_count = int(renewed_count[0])

        # Verify that bib has been renewed
        renewed_count.should.be.greater_than(0)
