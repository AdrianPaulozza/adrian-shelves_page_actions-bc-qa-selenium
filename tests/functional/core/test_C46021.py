import pytest
import allure
from mimesis import Text
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.user_profile import UserProfilePage
from pages.core.messages import MessagesPage
from pages.core.messages_show import ShowMessagePage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46021: Send message")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46021", "TestRail")
class TestC46021:
    def test_C46021(self):
        self.base_url = "https://epl.demo.bibliocommons.com"

        home_page = HomePage(self.driver, self.base_url).open()
        home_page.header.log_in("21221011111111", "1234") # Eplbiblion
        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page.header.set_search_type_to("User")
        search_term = "lq12"
        user_profile_page = home_page.header.search_for(search_term)
        user_profile_page.message.click()
        message_body = Text('en').sentence()
        user_profile_page.message_modal.body.send_keys(message_body)
        user_profile_page.message_modal.send.click()
        user_profile_page.wait.until(lambda message_sent: user_profile_page.is_top_message_container_displayed)
        user_profile_page.header.log_out()
        home_page = HomePage(self.driver, self.base_url).open()
        home_page.header.log_in("21221015114108", "1234") # lq12
        home_page.header.login_state_user_logged_in.click()
        home_page.header.messages.text.should.equal("Messages\n1")
        home_page.header.messages.click()
        messages_page = MessagesPage(self.driver)
        messages_page.messages[0].title.click()
        show_message_page = ShowMessagePage(self.driver)
        show_message_page.received_message.text.should.equal(message_body)

        # Delete the received message:
        messages_page = MessagesPage(self.driver, self.base_url).open()
        messages_page.messages[0].checkbox.click()
        messages_page.delete.click()
        messages_page.wait.until(lambda message_deleted: messages_page.is_top_message_container_displayed)
