import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.bib import BibPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C47052: Click availability link for the bib")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C47052", "TestRail")
class TestC47052:
    def test_C47052(self):
        self.base_url = "https://pickering.demo.bibliocommons.com"
        self.item_id = '2083452005'

        bib_page = BibPage(self.driver, self.base_url, item_id = self.item_id).open()
        bib_page.circulation.availability_by_location.click()
        bib_page.wait.until(lambda s: bib_page.overlay.availability.is_availability_status_displayed)
        bib_page.overlay.availability.library_table.text.should.contain("Location")
