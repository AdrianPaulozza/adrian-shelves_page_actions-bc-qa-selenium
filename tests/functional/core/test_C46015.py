import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46015: Search by Title or Keyword")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46015", "TestRail")
class TestC46015:
    def test_C46015(self):
        self.base_url = "https://epl.demo.bibliocommons.com"

        home_page = HomePage(self.driver, self.base_url).open()
        home_page.header.log_in("21221011111111", "1234")
        search_term = "Mandela"
        search_results_page = home_page.header.search_for(search_term) # , advanced_search = True)
        # Wait for search results to be greater than 0:
        search_results_page = SearchResultsPage(self.driver)
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.driver.current_url.should.equal("{}/v2/search?query={}&searchType=smart".format(self.base_url, search_term))
        for item in search_results_page.search_result_items:
# TODO: We need to revisit this, made add a function to get title/author/subtitle in one
            try:
                item.bib_title.text.should.match(search_term)
            except AssertionError as e:
                #Search for subtitles as well
                if search_term in item.bib_subtitle.text:
                    item.bib_subtitle.text.should.match(search_term)
                elif search_term in item.bib_author.text:
                    item.bib_author.text.should.match(search_term)
                else:
                    raise e('There are some titles that do not match based on Title, Subtitle, or Author')
