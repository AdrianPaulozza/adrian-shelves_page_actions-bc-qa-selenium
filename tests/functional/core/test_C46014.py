import pytest
import allure
import sure
import sys
sys.path.append('tests')
import datetime
import configuration.system

from pages.core.home import HomePage
from pages.core.user_dashboard import UserDashboardPage
from pages.core.suggested_purchases import SuggestedPurchasesPage
from selenium.common.exceptions import TimeoutException
from pages.core.admin_suggested_purchases import AdminSuggestedPurchasesPage
from selenium.webdriver.support.select import Select
from pages.core.components.overlay import Overlay

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46014: Login as Lib Admin and go to Suggested Purchases")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46014", "TestRail")
class TestC46014:
    def test_C46014(self):
        ### This test requires an admin account. The admin account creates a suggestion, denies and removes it

        # bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        # bib_page.header.log_in(configuration.user.name, configuration.user.password)

        home_page = HomePage(self.driver, "https://coaldale.demo.bibliocommons.com").open()
        home_page.header.log_in("carlbiblio", "6666")

        # Go to setting page and click on suggested purchases

        home_page.header.login_state_user_logged_in.click()
        home_page.header.my_library_dashboard.click()
        dashboard_page = UserDashboardPage(self.driver)
        self.driver.refresh()

        # Start filling in the suggest for purchase with test data
        # The test data that will be entered in the suggestion fields in this test
        # Using `Bibliocommons Test Entry - YYYY-MM-DD` as the bib title for easy identification later,
        # Bibliocommons as author, 2018 as year
        date = datetime.datetime.today().strftime('%Y-%m-%d')
        title = "Bibliocommons Test Entry - " + date

        dashboard_page.wait.until(lambda s: dashboard_page.is_submit_suggestion_displayed)
        dashboard_page.submit_suggestion.click()

        dashboard_page.wait.until(lambda s: dashboard_page.overlay.submit_suggestion.is_loaded)

        dashboard_page.overlay.submit_suggestion.title.send_keys(title)
        dashboard_page.overlay.submit_suggestion.author.send_keys("Bibliocommons")
        dashboard_page.overlay.submit_suggestion.publication_year.send_keys("2018")

        # This block is used to make sure selenium hasn't clicked the "Next" or "Submit" buttons too quickly.
        # Compare the URL before and after clicking to see if the button worked. Tries again one time.
        prior_url = self.driver.current_url
        dashboard_page.overlay.submit_suggestion.next_step.click()
        try:
            dashboard_page.wait.until(lambda s: self.driver.current_url != prior_url, 0.8)
        except TimeoutException:
            dashboard_page.overlay.submit_suggestion.next_step.click()
            dashboard_page.wait.until(lambda s: self.driver.current_url != prior_url, 0.8)
        #

        # On the next page, select values from the required dropdown menus
        dashboard_page.wait.until(lambda s: dashboard_page.overlay.submit_suggestion.is_dropdown_displayed)

        dashboard_page.overlay.submit_suggestion.select_from_dropdown(list='format', value='DVD')
        dashboard_page.overlay.submit_suggestion.select_from_dropdown(list='audience', value='Adult')
        dashboard_page.overlay.submit_suggestion.select_from_dropdown(list='content', value='Fiction')

        # Click next and click submit. Ensure Both clicks worked properly
        #
        prior_url = self.driver.current_url
        dashboard_page.overlay.submit_suggestion.next_step.click()
        try:
            dashboard_page.wait.until(lambda s: self.driver.current_url != prior_url, 0.8)
        except TimeoutException:
            dashboard_page.overlay.submit_suggestion.next_step.click()
            dashboard_page.wait.until(lambda s: self.driver.current_url != prior_url)
        #

        dashboard_page.wait.until(lambda s: dashboard_page.overlay.submit_suggestion.is_submit_button_displayed)

        #
        prior_url = self.driver.current_url
        dashboard_page.overlay.submit_suggestion.submit_button.click()
        try:
            dashboard_page.wait.until(lambda s: self.driver.current_url != prior_url, 0.8)
        except TimeoutException:
            dashboard_page.overlay.submit_suggestion.submit_button.click()
            dashboard_page.wait.until(lambda s: self.driver.current_url != prior_url)
        #

        # Navigate to the Suggested Purchases page from the user dashboard
        # Sometimes Selenium attempts to click this link while the overlay is still closing
        dashboard_page.wait.until_not(lambda s: dashboard_page.overlay.submit_suggestion.is_submit_button_displayed)
        dashboard_page.wait.until(lambda s: dashboard_page.is_suggest_for_purchase_link_displayed)
        dashboard_page.suggest_for_purchase_link.click()

        suggested_purchases_page = SuggestedPurchasesPage(self.driver)

        suggested_purchases_page.suggestion_counter.text.should.equal("Showing 1 suggestion.")

        # * Reviewing a suggestion as an administrator

        # Navigation to the admin suggest for purchase page

        suggested_purchases_page.header.admin_dropdown.click()
        suggested_purchases_page.wait.until(lambda s: suggested_purchases_page.header.is_admin_suggested_purchases_displayed)
        suggested_purchases_page.header.admin_suggested_purchases.click()
        admin_suggested_purchases_page = AdminSuggestedPurchasesPage(self.driver)

        # Find an entry that is an exact match, so we know it is the test submission

        admin_suggested_purchases_page.wait.until(lambda s: admin_suggested_purchases_page.is_search_suggestions_box_displayed)
        admin_suggested_purchases_page.search_suggestions_box.send_keys(title)
        admin_suggested_purchases_page.search_suggestions_button.click()

        # The resulting list of suggestions should only contain the suggestion we have just made.
        # If this check should fail, either the suggestion did not make it in, or the results list includes extra items
        # from a previous test (or actual patron suggestions that have bizarre and unlikely title matches)

        admin_suggested_purchases_page.wait.until(lambda s: (len(admin_suggested_purchases_page.suggestions_list) > 0))
        len(admin_suggested_purchases_page.suggestions_list).should.equal(1)

        admin_suggested_purchases_page.deny_suggestion_button.click()

        admin_suggested_purchases_page.wait.until(
            lambda s: admin_suggested_purchases_page.overlay.review_suggested_purchases.is_deny_suggestion_loaded)

        admin_suggested_purchases_page.overlay.review_suggested_purchases.suggested_purchase_title.text.should.equal(title)

        # Verification process done - now deny the suggestion with a custom message

        admin_suggested_purchases_page.overlay.review_suggested_purchases.suggestion_custom_response_radio.click()
        admin_suggested_purchases_page.wait.until(lambda s: admin_suggested_purchases_page.
                                        overlay.review_suggested_purchases.is_suggestion_custom_response_textbox_loaded)


        admin_suggested_purchases_page.overlay.review_suggested_purchases.suggestion_custom_response_textbox.send_keys(
            "This is a test rejection of a test suggestion")
        admin_suggested_purchases_page.overlay.review_suggested_purchases.suggestion_overlay_deny_button.click()

        # Confirm that the matching suggestion is gone now.
        admin_suggested_purchases_page.wait.until(lambda s: admin_suggested_purchases_page.
                                                  is_zero_suggestions_displayed)

        # Go back to the user dashboard

        admin_suggested_purchases_page.header.login_state_user_logged_in.click()
        admin_suggested_purchases_page.header.my_library_dashboard.click()
        dashboard_page = UserDashboardPage(self.driver)

        dashboard_page.wait.until(lambda s: dashboard_page.is_suggest_for_purchase_link_displayed)
        dashboard_page.suggest_for_purchase_link.click()
        suggested_purchases_page = SuggestedPurchasesPage(self.driver)

        # Assert the suggestion has been denied, and remove it

        suggested_purchases_page.wait.until(lambda s: suggested_purchases_page.is_remove_suggestion_displayed)
        suggested_purchases_page.remove_suggestion.click()

        suggested_purchases_page.wait.until(lambda s: suggested_purchases_page.is_cancel_confirmation_displayed)
        suggested_purchases_page.cancel_confirmation.click()

        suggested_purchases_page.wait.until(lambda s: suggested_purchases_page.suggestion_counter.text == "Showing 0 suggestions.")
        suggested_purchases_page.suggestion_counter.text.should.equal("Showing 0 suggestions.")
