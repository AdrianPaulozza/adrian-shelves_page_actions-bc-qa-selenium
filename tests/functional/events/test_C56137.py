from pages.core.home import HomePage
from pages.events.home_page_events import EventsHome
from pages.events.events_locations import EventLocationPage
from mimesis import Address
import pytest
import allure
import sys
# noinspection PyUnresolvedReferences
import sure
sys.path.append('tests')


@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C56137 - Find by address or ZIP code - invalid")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56137", "TestRail")
class TestC56137:
    def test_c56137(self):
        self.base_url = "https://chipublib.demo.bibliocommons.com/events"
        HomePage(self.driver, self.base_url).open()

        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_hours_and_location_link_displayed)

        home_page_events.hours_and_location_link.click()
        home_page_events.wait.until(lambda s: home_page_events.is_hours_and_location_textbox_displayed)

        # added a cause zip code returned a true response half the time
        language = Address('en')
        zip_code = 'a'+language.postal_code()
        home_page_events.hours_and_location_textbox.send_keys(zip_code)

        home_page_events.wait.until(lambda s: home_page_events.is_hours_and_location_search_link_displayed)
        home_page_events.hours_and_location_search_link.click()

        location_page = EventLocationPage(self.driver)
        location_page.wait.until(lambda s: location_page.is_events_location_invalid_search_displayed)

        location_page.events_location_invalid_search_text.text.should.match(zip_code)
