import pytest
import allure
import sys
sys.path.append('tests')
# noinspection PyUnresolvedReferences
import sure
from pages.core.home import HomePage
from pages.events.home_page_events import EventsHome


@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C56046: Search Box - search non-existing(Desktop / Mobile)")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56046", "TestRail")
class TestC56046:
    def test_c56046(self):
        self.base_url = "https://chipublib.demo.bibliocommons.com/events"

        home_page = HomePage(self.driver, self.base_url).open()
        home_page.header.log_in("cpltest3", "60643")
        home_page_events = EventsHome(self.driver)

        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()

        search_term = "z2w1@o#l$e#"
        home_page_events.search_textbox.send_keys(search_term)
        home_page_events.search_textbox_icon.click()
        home_page_events.wait.until(lambda s: home_page_events.is_search_for_another_displayed)
        home_page_events.is_search_for_another_displayed.should.be.true
        home_page_events.search_for_another.click()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        home_page_events.is_show_more_displayed.should.be.true
