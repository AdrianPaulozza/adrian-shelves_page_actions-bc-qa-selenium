import pytest
import allure
import sys
sys.path.append('tests')
# noinspection PyUnresolvedReferences
import sure
from pages.events.events_locations import EventLocationPage
from pages.events.events_locations_list import EventLocationListPage


@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("A-Z Locations")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56144", "TestRail")
class TestC56144:
    def test_c56144(self):
        # location_name = "Albany Park"
        self.base_url = "https://chipublib.demo.bibliocommons.com/locations"
        EventLocationPage(self.driver, self.base_url).open()

        location_page = EventLocationPage(self.driver)
        location_page.wait.until(lambda s: location_page.is_events_location_first_library_in_list_displayed)
        location_page.a_to_z_locations_link.click()

        location_list_page = EventLocationListPage(self.driver)
        location_list_page.wait.until(lambda s: location_list_page.is_events_location_list_dropdown_displayed)

        location_list_page.is_location_information_list_displayed.should.be.true
        location_list_page.is_library_list_of_hours_displayed.should.be.true
        location_list_page.is_library_emails_list_displayed.should.be.true
        location_list_page.is_library_phone_numbers_list_displayed.should.be.true
        location_list_page.is_library_features_and_facilities_displayed.should.be.true

        location_name = location_list_page.first_location_link[0].text
        location_list_page.first_location_link[0].click()
        location_list_page.wait.until(lambda s: location_list_page.is_detail_location_page_heading_displayed)

        location_list_page.detail_location_page_heading.text.should.match(location_name)
