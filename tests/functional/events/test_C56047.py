import pytest
import allure
# noinspection PyUnresolvedReferences
import sure
from pages.core.home import HomePage
from pages.events.events_admin import AdminEventPage
from pages.events.home_page_events import EventsHome
from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC


@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C56047 Search Box - search existing")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56047", "TestRail")
class TestC56047:
    def test_c56047(self):
        self.base_url = "https://chipublib.demo.bibliocommons.com/events"

        # log in
        home_page = HomePage(self.driver, self.base_url).open()
        home_page.header.log_in("cpltest3", "60643")
        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        # navigate to events page
        events_admin = AdminEventPage(self.driver)
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_previous_page_icon_displayed)

        # Create events
        events_admin.create_event_button.click()
        events_admin.wait.until(lambda s: events_admin.is_title_textbox_displayed)  

        title_name = "test_C56047"

        events_admin.title_textbox.send_keys(title_name)
        events_admin.type_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_type_dropdown_business_displayed)
        events_admin.type_dropdown_business.click()
        events_admin.audience_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_type_audience_dropdown_families_displayed)
        events_admin.type_audience_dropdown_families.click()
        events_admin.description_field.send_keys("An event about a comic book")
        events_admin.time_selector.click()
        events_admin.wait.until(lambda s: events_admin.start_time[80].is_displayed())
        events_admin.start_time[80].click()

        # location and publish
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_displayed)
        events_admin.location_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_archer_height_displayed)
        events_admin.location_dropdown_archer.click()

        events_admin.wait.until(lambda s: EC.element_to_be_clickable(events_admin.is_save_and_publish_displayed))
        events_admin.save_and_publish.click()
        events_admin.wait.until(lambda s: events_admin.is_left_chevron_button_displayed)

        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page_events.search_textbox.send_keys(title_name)
        home_page_events.search_textbox_icon.click()
        home_page_events.wait.until(lambda s: events_admin.is_event_name_last_word_displayed)
        events_admin.event_name_last_word.text.should.equal(title_name)

        # deleting events
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        events_admin.admin_published_tab.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_events_search_textbox_displayed)

        events_admin.retry_search_until_results_appear(title_name)

        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)

        delete_button = events_admin.admin_delete_button
        ActionChains(self.driver).move_to_element(delete_button).perform()
        events_admin.wait.until(lambda s: events_admin.is_admin_delete_button_displayed)
        events_admin.admin_delete_button.click()
        events_admin.wait.until(lambda s: events_admin.is_confirm_delete_button_displayed)

        events_admin.confirm_delete_button.click()
        events_admin.wait.until(lambda s: events_admin.is_close_delete_overlay_button_displayed)
        events_admin.close_delete_overlay_button.click()
        events_admin.wait.until(lambda s: events_admin.no_results_displayed)
        events_admin.assert_is_no_results_displayed.should.be.true
