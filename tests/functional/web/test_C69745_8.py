import pytest
import allure
import sure
import configuration.user
import configuration.system
from selenium.common.exceptions import StaleElementReferenceException, ElementNotVisibleException, NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from mimesis import Text
from utils.image_download_helper import *
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_new_content.create_new_blog_post import CreateNewBlogPostPage
from pages.web.page_builder import PageBuilderPage
from pages.web.user import UserPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderSingleCard
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_blog_posts import AllBlogPostsPage
from pages.web.wpadmin.v3.edit_blog_post import EditBlogPostPage
from pages.web.blog_post import BlogPostPage
from utils.db_connector import DBConnector
from utils.selenium_helpers import click

PAGE = "bibliocommons-settings"
BLOG_TITLE = '-'.join(Text('en').words(quantity=3))
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
IMAGE_TITLE = Text('en').words(quantity=1)[0]
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")
BLOG_TITLE_UPDATED = ''.join(Text('en').words(quantity=3))
DESCRIPTION = Text('en').words(quantity=1)
TAXONOMIES = ['Audience', 'Related Format', 'Programs and Campaigns', 'Genre', 'Topic']
taxonomies_terms = []


@pytest.fixture(scope='class')
def login_and_setup(request, selenium_setup_and_teardown):

    driver = request.cls.driver

    # Logging in as network admin
    login_page = LoginPage(driver, configuration.system.base_url_web).open()
    login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])
    # Setting V3 status as implementing
    settings_system_settings_tab = SettingsSystemSettingsTabPage(driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
    click(settings_system_settings_tab.v3_status_implementing)
    settings_system_settings_tab.save_changes.click()

    new_blog_post = CreateNewBlogPostPage(driver, configuration.system.base_url_web, post_type='post').open()

    # Creating a dynamic list of taxonomy terms to select in the test
    for index, taxonomy in enumerate(TAXONOMIES):
        click(new_blog_post.taxonomy(taxonomy))
        taxonomies_terms.append(new_blog_post.taxonomy_terms(taxonomy)[0])
        if index == len(TAXONOMIES) - 1:
            break
        new_blog_post.taxonomies_evergreen_checkbox.click()


@pytest.mark.usefixtures('login_and_setup')
@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
class Tests:
    @allure.title("C69745: Blog Post - Create New")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69745", "TestRail")
    def test_C69745(self):
        # Logging out
        self.driver.delete_all_cookies()

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        download_image("https://bit.ly/2EdPMqG", IMAGE_PATH)

        # PART 1- Create new Blog Post/Card
        new_blog_post = CreateNewBlogPostPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        new_blog_post.title.send_keys(BLOG_TITLE)
        new_blog_post.choose_blog_categories("uncategorized").click()
        click(new_blog_post.visual)
        visual = new_blog_post.visual_area

        # Adding a FAQ widget
        visual.related_faq_button()
        visual.related_faq.select_category("Most Recent")
        visual.related_faq.insert_shortcode.click()
        visual.is_related_faq_form_visible.should.be.false

        # Adding a location widget
        visual.add_library()
        visual.add_library_locations.choose_library_locations("Albany Park").click()
        visual.add_library_locations.choose_library_locations("Archer Heights").click()
        visual.add_library_locations.insert_into_post.click()
        visual.is_add_library_locations_form_visible.should.be.false

        click(new_blog_post.add_media)
        new_blog_post.insert_media.upload_image.send_keys(IMAGE_PATH)
        new_blog_post.insert_media.add_image_to_widget_button.click()
        click(new_blog_post.autofill_text_fields)
        new_blog_post.taxonomies_featured_checkbox.click()
        new_blog_post.taxonomies_evergreen_checkbox.click()
        new_blog_post.card_image.click()
        new_blog_post.select_widget_image.select_image_from_list(0).click()
        click(new_blog_post.select_widget_image.add_image_to_widget_button)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2)
        wait.until(lambda condition: new_blog_post.image_cropper.is_cropper_modal_visible is True)
        wait.until(lambda condition: new_blog_post.image_cropper.is_crop_one_visible is True)
        wait.until(lambda condition: new_blog_post.image_cropper.is_cropper_box_visible is True)
        new_blog_post.image_cropper.image_one_crop()
        new_blog_post.image_cropper.crop_image.click()
        new_blog_post.image_cropper.next.click()
        wait.until(lambda condition: new_blog_post.image_cropper.is_cropper_box_visible is True)
        wait.until(lambda condition: new_blog_post.image_cropper.is_crop_one_visible is False)
        wait.until(lambda condition: new_blog_post.image_cropper.is_crop_two_visible is True)
        new_blog_post.image_cropper.image_two_crop()
        new_blog_post.image_cropper.crop_image.click()
        new_blog_post.image_cropper.done.click()
        wait.until(lambda condition: new_blog_post.image_cropper.is_cropper_modal_visible is False)
        new_blog_post.featured_image.click()
        new_blog_post.select_widget_image.select_image_from_list(0).click()
        new_blog_post.select_widget_image.add_image_to_widget_button.click()

        for index, taxonomy in enumerate(TAXONOMIES):
            new_blog_post.taxonomy(TAXONOMIES[index]).click()
            new_blog_post.select_taxonomy(TAXONOMIES[index], taxonomies_terms[index]).click()

        new_blog_post.scroll_to_top()
        click(new_blog_post.publish)
        new_blog_post.text.click()
        _ = ["img", "relatedfaqs", "library_location"]
        for index, information in enumerate(_):
            new_blog_post.text_area.text_body.get_attribute("value").should.contain(information)

        delete_downloaded_image(IMAGE_PATH)

        # Opening a new tab
        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # PART 2 - Create a New Page
        create_new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        create_new_page.title.send_keys(PAGE_TITLE)
        create_new_page.page_builder_section.click()
        click(create_new_page.publish)
        create_new_page.wait.until(lambda condition: self.driver.page_source.should.contain("Update"))

        # Opening the new page in the frontend
        new_page = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        new_page.wpheader.page_builder.click()
        if new_page.builder_panel.is_panel_visible is False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        # Adding a single card to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.single_card, new_page.page_builder.body).perform()

        # Selecting the blog post card that was created
        single_card = PageBuilderSingleCard(new_page)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Blog Post Card")
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Blog Post")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(BLOG_TITLE).click()
        single_card.edit_card.save.click()
        single_card.save()
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=IndexError)
        wait.until(lambda s: new_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        len(new_page.user_facing_modules.single_cards).should.equal(1)
        len(new_page.user_facing_modules.single_cards[0].cards).should.equal(1)
        new_page.page_builder.done.click()
        new_page.wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()

        # Asserting that the required information from the blog post card is present in the page after the module is published
        _ = [BLOG_TITLE, "Archer Heights", "Albany Park", "o-card__image"]
        for index, information in enumerate(_):
            self.driver.page_source.should.contain(information)
        for index, taxonomy in enumerate(TAXONOMIES):
            self.driver.page_source.should.contain(taxonomies_terms[index])

        self.driver.close()

        # Switching to tab 1
        self.driver.switch_to_window(self.driver.window_handles[0])

    @allure.title("C69746: Blog Post/Card - View")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69746", "TestRail")
    def test_C69746(self):
        # Logging out
        self.driver.delete_all_cookies()

        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 as enabled
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        click(settings_system_settings_tab.v3_status_enabled)
        settings_system_settings_tab.save_changes.click()

        # Logging out
        self.driver.delete_all_cookies()

        # Opening the blog post as a patron
        BlogPostPage(self.driver, configuration.system.base_url_web, title=BLOG_TITLE).open()

        # Asserting that the required information from the blog post is present
        _ = ["data-wp-image", "Related FAQs", "Albany Park", "Archer Heights"]
        for index, information in enumerate(_):
            self.driver.page_source.should.contain(information)

        # Opening the page containing the blog post card as a patron
        UserPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()

        # Asserting that the required information from the blog post card is present
        _ = [BLOG_TITLE, "Archer Heights", "Albany Park", "o-card__image"]
        for index, information in enumerate(_):
            self.driver.page_source.should.contain(information)
        for index, taxonomy in enumerate(TAXONOMIES):
            self.driver.page_source.should.contain(taxonomies_terms[index])

    @allure.title("Blog Post - Update- Blog card (with taxonomies) already created and added to a PB page")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69747", "TestRail")
    def test_C69747(self):
        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as implementing
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        click(settings_system_settings_tab.v3_status_implementing)
        settings_system_settings_tab.save_changes.click()

        # Logging out
        self.driver.delete_all_cookies()

        # Logging in as lib admin
        login_page.open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        # Getting the blog post ID
        all_blogs_page = AllBlogPostsPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        all_blogs_page.search_input.send_keys(BLOG_TITLE)
        all_blogs_page.search_button.click()
        all_blogs_page.rows[0].title.get_attribute("textContent").should.match(BLOG_TITLE)
        _ = all_blogs_page.rows[0].title.get_attribute("href")
        blog_post_id = _[58:-12]

        # Checking that the created blog post contains all the expected information
        edit_blog_post = EditBlogPostPage(self.driver, configuration.system.base_url_web, post=blog_post_id, action='edit').open()

        edit_blog_post.image_cropper.is_crop_one_preview_visible.should.be.true
        edit_blog_post.image_cropper.is_crop_two_preview_visible.should.be.true

        for index, term in enumerate(taxonomies_terms):
            edit_blog_post.selected_taxonomy_terms[index].get_attribute("textContent").casefold().should.equal(term.casefold())

        # Editing the blog post
        edit_blog_post.title.clear()
        edit_blog_post.title.send_keys(BLOG_TITLE_UPDATED)

        edit_blog_post.text.click()
        text = edit_blog_post.text_area
        text.text_body.click()
        text.text_body.send_keys(DESCRIPTION)

        edit_blog_post.visual.click()

        # Firefox doesn't need this block of code, but for some reason Chrome fails without it.
        if self.driver.capabilities['browserName'] is 'chrome':
            edit_blog_post.scroll_to_autofill_text_fields()

        click(edit_blog_post.autofill_text_fields)

        # The following occurs sometimes. The driver sometimes clicks the image_cropper element, which opens up the modal.
        # Trying to mitigate that with the below block of code.
        try:
            if edit_blog_post.image_cropper.cancel.is_displayed() is True:
                edit_blog_post.image_cropper.cancel.click()
        except NoSuchElementException:
            pass

        edit_blog_post.scroll_to_top()
        click(edit_blog_post.update)

        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # Opening the blog post while still logged in as Staff
        blog_post_page = BlogPostPage(self.driver, configuration.system.base_url_web, title=BLOG_TITLE).open()

        wait = WebDriverWait(self.driver, 10, ignored_exceptions=[ElementNotVisibleException, StaleElementReferenceException, NoSuchElementException])
        self.driver.refresh()
        wait.until(lambda condition: blog_post_page.blog_title.get_attribute("textContent") == BLOG_TITLE_UPDATED)

        self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])

        # Logging out
        self.driver.delete_all_cookies()

    @allure.title("Blog Post - Update - Blog card (with taxonomies) already created and added to a PB page")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69748", "TestRail")
    def test_C69748(self):
        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as enabled
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        click(settings_system_settings_tab.v3_status_enabled)
        settings_system_settings_tab.save_changes.click()

        self.driver.delete_all_cookies()

        # Logging in as lib admin
        login_page.open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        # Getting the blog post ID
        all_blogs_page = AllBlogPostsPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        all_blogs_page.search_input.send_keys(BLOG_TITLE_UPDATED)
        all_blogs_page.search_button.click()
        all_blogs_page.rows[0].title.get_attribute("textContent").should.match(BLOG_TITLE_UPDATED)
        _ = all_blogs_page.rows[0].title.get_attribute("href")
        blog_post_id = _[58:-12]

        # Checking the DB if the blog's title is updated
        check_db = DBConnector()
        check_db.check_updated_post_title(BLOG_TITLE_UPDATED, blog_post_id).should.be.true

        # Deleting the uploaded image from the media library
        new_blog_post = CreateNewBlogPostPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        click(new_blog_post.add_media)
        new_blog_post.insert_media.select_image_from_list(0).click()
        new_blog_post.insert_media.delete_image_link.click()
        new_blog_post.insert_media.accept_alert_to_delete_image()
        new_blog_post.insert_media.media_library_tab.click()
        new_blog_post.insert_media.close_button.click()

        # Deleting the created blog post
        all_blogs_page = AllBlogPostsPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        all_blogs_page.search_input.send_keys(BLOG_TITLE_UPDATED)
        all_blogs_page.search_button.click()
        all_blogs_page.rows[0].title.get_attribute("textContent").should.match(BLOG_TITLE_UPDATED)
        all_blogs_page.rows[0].hover_on_title()
        all_blogs_page.rows[0].delete.click()
        all_blogs_page.rows.should.be.empty

        # Deleting the created page
        all_pages = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages.search_input.send_keys(PAGE_TITLE)
        all_pages.search_button.click()
        all_pages.rows[0].title.get_attribute("textContent").should.match(PAGE_TITLE)
        all_pages.rows[0].hover_on_title()
        all_pages.rows[0].delete.click()
        all_pages.rows.should.be.empty
