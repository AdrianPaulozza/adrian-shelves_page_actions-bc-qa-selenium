import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text
from selenium.webdriver.common.keys import Keys
from utils.selenium_helpers import click
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_catalog_comment_card import CreateNewCatalogCommentCard
from pages.web.staff_base import StaffBasePage
from pages.web.page_builder import PageBuilderPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderSingleCard
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.core.bib_comment import CommentPage


PAGE = 'bibliocommons-settings'
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Catalog Comment"
ITEM_ID = "1125621126"
COMMENT_ID = "93608025"

@pytest.fixture(scope='class')
def login_and_setup(request, selenium_setup_and_teardown):

    driver = request.cls.driver

    # Logged in as Network Admin
    login_page = LoginPage(driver, configuration.system.urls_web[3]).open()
    login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
    settings_general_tab = SettingsGeneralTabPage(driver, configuration.system.urls_web[3], page=PAGE, tab='generic').open()
    click(settings_general_tab.disable_structured_tags)
    settings_general_tab.save_changes.click()
    driver.delete_all_cookies()


@pytest.mark.stage
@pytest.mark.usefixtures('login_and_setup')
class TestC74550:
    @allure.title("C79405: Catalog Comment Card (140 chars or less) - Create")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/79405", "TestRail")
    def test_C79405(self):

        # Change V3 Status = Implementing
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.urls_web[3], page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()
        settings_system_settings_tab.wpsidemenu.is_menu_switch_to_v2_menu_displayed.should.be.true
        self.driver.delete_all_cookies()

        # PART 1 - Create a Catalog Comment Card
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        comment_page = CommentPage(self.driver, "https://chipublib.stage.bibliocommons.com/", item_id=ITEM_ID, comment_id=COMMENT_ID).open()
        permalink_url = self.driver.current_url
        bib_title = comment_page.bib_title.text
        bib_author = comment_page.bib_author.text.split(", ")
        bib_author.reverse()
        bib_author = " ".join(bib_author)
        bib_image_link = comment_page.bib_jacket_cover.get_attribute("src")
        bib_comment_author = comment_page.user_id.text
        bib_comment_content = comment_page.comment.text

        new_catalog_comment_card = CreateNewCatalogCommentCard(self.driver, configuration.system.urls_web[3], post_type='bw_catalog_comment').open()
        new_catalog_comment_card.catalog_comment_url.send_keys(permalink_url)
        new_catalog_comment_card.grab_comment_info.click()
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda condition: new_catalog_comment_card.is_validator_message_displayed)
        new_catalog_comment_card.is_success_message_displayed.should.be.true
        new_catalog_comment_card.success_message.text.should.match("Catalog Comment information successfully grabbed!")

        new_catalog_comment_card.is_free_text_tags_field_displayed.should.be.true
        num_of_taxonomies = 3
        for i in range(num_of_taxonomies):
            tag = Text('en').words(quantity=1)[0]
            new_catalog_comment_card.free_text_tags.send_keys(tag)
            new_catalog_comment_card.add_free_text_tags.click()
        new_catalog_comment_card.are_free_text_tags_added.should.be.true

        new_catalog_comment_card.scroll_to_top()
        new_catalog_comment_card.publish.click()

        # PART 2 - Verify if it's actually published
        # Verify card is added to All Cards page
        all_cards_page = AllCardsPage(self.driver, configuration.system.urls_web[3], page='bw-content-card').open()
        all_cards_page.search_input.send_keys(bib_title, Keys.RETURN)
        all_cards_page.rows[0].title.text.should.match(bib_title)
        all_cards_page.rows[0].card_type.text.should.match(CONTENT_TYPE)
        taxonomies = all_cards_page.rows[0].taxonomies
        len(taxonomies).should.equal(num_of_taxonomies)

        # PART 3 - Create a New Page
        new_page = CreateNewPagePage(self.driver, configuration.system.urls_web[3], post_type='page').open()
        new_page.title.send_keys(PAGE_TITLE)
        new_page.page_builder_section.click()
        click(new_page.publish)

        page_staff = PageBuilderPage(self.driver, configuration.system.urls_web[3] + PAGE_TITLE).open()
        page_staff.wait.until(lambda s: page_staff.wpheader.is_wp_admin_header_displayed)
        page_staff.wpheader.page_builder.click()

        # Select single card module
        if page_staff.builder_panel.is_panel_visible is False:
            page_staff.wpheader.add_content.click()
        page_staff.builder_panel.modules_tab.click()
        ActionChains(self.driver).drag_and_drop(page_staff.builder_panel.single_card, page_staff.page_builder.body).perform()
        single_card = PageBuilderSingleCard(page_staff)
        click(single_card.advanced_tab)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Single Card - " + CONTENT_TYPE)
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type(CONTENT_TYPE)
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(bib_title).click()
        single_card.edit_card.save.click()
        single_card.save()
        wait.until(lambda s: page_staff.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)

        page_staff.page_builder.done.click()
        page_staff.wait.until(lambda condition: page_staff.page_builder.publish.is_displayed())
        page_staff.page_builder.publish.click()
        self.driver.execute_script("arguments[0].className = 'menupop with-avatar hover'",
                                   page_staff.wpheader.my_account)
        page_staff.wpheader.wait.until(lambda s: page_staff.wpheader.is_my_account_submenu_displayed)
        page_staff.wpheader.my_account_log_out.click()

        # PART 4 - Patron view
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.urls_web[3], page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()
        settings_system_settings_tab.wpsidemenu.is_menu_switch_to_v2_menu_displayed.should.be.false
        self.driver.delete_all_cookies()

        page_patron = StaffBasePage(self.driver, configuration.system.urls_web[3] + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false

        len(page_patron.user_facing_modules.single_cards).should.equal(1)
        len(page_patron.user_facing_modules.single_cards[0].cards[0].card_tags).should.equal(num_of_taxonomies)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_image.get_attribute("src").should.equal(bib_image_link)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_title.text.should.equal(bib_title)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_bib_author.text.should.equal(bib_author)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_comment_author.text.should.equal(bib_comment_author)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_comment.text.should.equal(bib_comment_content)
        page_patron.user_facing_modules.single_cards[0].cards[0].is_card_read_more_displayed.should.be.false

        self.driver.delete_all_cookies()

        # PART 5 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        all_cards_page = AllCardsPage(self.driver, configuration.system.urls_web[3], page='bw-content-card').open()
        all_cards_page.search_input.send_keys(bib_title, Keys.RETURN)
        all_cards_page.rows[0].hover_on_title()
        all_cards_page.rows[0].delete.click()
        all_cards_page.rows.should.be.empty

        all_pages_page = AllPagesPage(self.driver, configuration.system.urls_web[3], post_type='page').open()
        all_pages_page.search_input.send_keys(PAGE_TITLE, Keys.RETURN)
        all_pages_page.rows[0].title.text.should.match(PAGE_TITLE)
        all_pages_page.rows[0].hover_on_title()
        all_pages_page.rows[0].delete.click()
        all_pages_page.rows.should.be.empty
