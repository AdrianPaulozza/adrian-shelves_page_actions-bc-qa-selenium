import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text
from selenium.common.exceptions import ElementNotInteractableException, NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.core.search_lists_results import SearchListsResultsPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_list_card import CreateNewListCard
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.page_builder import PageBuilderPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderSingleCard
from utils.selenium_helpers import click, enter_text
from selenium.webdriver.common.action_chains import ActionChains


PAGE = "bibliocommons-settings"
CORE_BASE_URL = "https://chipublib-sandbox-local.stage.bibliocommons.com"
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
LIST_SEARCH_TERM = Text('en').word()
LIST_INFO = {
    'title': '-'.join(Text('en').words(quantity=3)),
    'description': Text('en').sentence()
}


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C74549: Create new card - List Card")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/74549", "TestRail")
class TestC74549:
    def test_C74549(self):
        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as implementing
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()

        self.driver.delete_all_cookies()

        # Fetching a list from Core
        search_lists_page = SearchListsResultsPage(self.driver, CORE_BASE_URL, query=LIST_SEARCH_TERM).open()
        try:
            list_href = search_lists_page.lists[0].title.get_attribute("pathname")
            list_url = CORE_BASE_URL + list_href
        except NoSuchElementException:
            raise NoSuchElementException

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        # Creating a new list card
        new_list_card = CreateNewListCard(self.driver, configuration.system.base_url_web, post_type='bw_list').open()
        enter_text(new_list_card.list_url, list_url)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=[IndexError, ElementNotInteractableException])
        # Adding an extra check to ensure that the list url is properly entered into the field.
        wait.until(lambda condition: list_url in new_list_card.list_url.get_attribute("value"))
        new_list_card.grab_list_info.click()
        wait.until(lambda condition: "List information successfully grabbed!" in self.driver.page_source)
        wait.until(lambda condition: new_list_card.image_cropper.is_crop_one_preview_visible is True)
        new_list_card.card_title.clear()
        new_list_card.card_title.send_keys(LIST_INFO['title'])
        new_list_card.card_description.clear()
        new_list_card.card_description.send_keys(LIST_INFO['description'])
        new_list_card.scroll_to_top()
        new_list_card.publish.click()

        # Creating a new page
        create_new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        create_new_page.title.send_keys(PAGE_TITLE)
        create_new_page.page_builder_section.click()
        click(create_new_page.publish)
        create_new_page.wait.until(lambda condition: "Update" in self.driver.page_source)

        # Opening a new tab
        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # Adding a single card to the page from Page Builder
        new_page = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        new_page.wpheader.page_builder.click()
        if new_page.builder_panel.is_panel_visible is False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.single_card, new_page.page_builder.body).perform()

        # Selecting the list card that was created
        single_card = PageBuilderSingleCard(new_page)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("List Card")
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("List")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(LIST_INFO['title']).click()
        single_card.edit_card.save.click()
        single_card.save()
        wait.until(lambda s: len(new_page.user_facing_modules.single_cards) is 1)
        wait.until(lambda s: new_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        len(new_page.user_facing_modules.single_cards).should.equal(1)
        len(new_page.user_facing_modules.single_cards[0].cards).should.equal(1)
        new_page.page_builder.done.click()
        new_page.wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()
        wait.until(lambda condition: new_page.wpheader.is_page_builder_menu_item_displayed)
        wait.until(lambda condition: len(new_page.user_facing_modules.single_cards).should.equal(1))
        new_page.open()
        wait.until(lambda condition: new_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        new_page.user_facing_modules.single_cards[0].cards[0].card_title.get_attribute("textContent").should.equal(LIST_INFO['title'])
        new_page.user_facing_modules.single_cards[0].cards[0].card_description.get_attribute("textContent").should.equal(LIST_INFO['description'])

        self.driver.close()

        # Switching to tab 1
        self.driver.switch_to_window(self.driver.window_handles[0])

        # Deleting the created page
        all_pages = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages.search_input.send_keys(PAGE_TITLE)
        all_pages.search_button.click()
        all_pages.rows[0].title.get_attribute("textContent").should.match(PAGE_TITLE)
        all_pages.rows[0].hover_on_title()
        all_pages.rows[0].delete.click()
        all_pages.rows.should.be.empty

        # Deleting the created card
        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_input.send_keys(LIST_INFO['title'])
        all_cards_page.search_button.click()
        all_cards_page.rows[0].title.get_attribute("textContent").should.match(LIST_INFO['title'])
        all_cards_page.rows[0].hover_on_title()
        all_cards_page.rows[0].delete.click()
        all_cards_page.rows.should.be.empty
