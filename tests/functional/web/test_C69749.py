import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text
from selenium.common.exceptions import ElementNotInteractableException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.forms.create_new_form import CreateNewFormPage
from pages.web.wpadmin.v3.all_contents_page.all_forms import AllFormsPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.forms.edit_form import EditFormPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_poll_card import CreateNewPollCard
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.page_builder import PageBuilderPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderSingleCard
from utils.image_download_helper import *
from utils.selenium_helpers import click
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


PAGE = "bibliocommons-settings"
FORM_INFO = {
    'title': ' '.join(Text('en').words(quantity=3)),
    'description': Text('en').sentence()
}
POLL_INFO = {
    'description': Text('en').sentence(),
    'question': ' '.join(Text('en').words(quantity=3)),
    'choice_one': Text('en').word(),
    'choice_two': Text('en').word(),
    'choice_three': Text('en').word(),
}
IMAGE_TITLE = Text('en').words(quantity=1)[0]
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C69749: Form + Poll Card - New")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69749", "TestRail")
class TestC69749:
    def test_C69749(self):

        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as enabled
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        self.driver.delete_all_cookies()

        download_image("https://bit.ly/2EdPMqG", IMAGE_PATH)

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        # Creating a new form
        new_form = CreateNewFormPage(self.driver, configuration.system.base_url_web).open()
        new_form.form_title.send_keys(FORM_INFO['title'])
        new_form.form_description.send_keys(FORM_INFO['description'])
        new_form.create_form.click()
        wait = WebDriverWait(self.driver, 6, poll_frequency=2, ignored_exceptions=IndexError)
        wait.until(lambda condition: new_form.visibility_of_form_window is False)

        # Getting the ID of the created form
        all_forms_page = AllFormsPage(self.driver, configuration.system.base_url_web).open()
        all_forms_page.search_forms_input.send_keys(FORM_INFO['title'])
        all_forms_page.search_forms_button.click()
        all_forms_page.rows[0].title.get_attribute("textContent").should.match(FORM_INFO['title'])
        form_id = all_forms_page.rows[0].id.get_attribute("textContent")

        # Opening the created form
        edit_form_page = EditFormPage(self.driver, configuration.system.base_url_web, form_id=form_id).open()
        edit_form_page.advanced_fields_heading.click()
        wait.until(lambda condition: edit_form_page.advanced_fields.poll.is_displayed())

        # Creating the poll in the form
        ActionChains(self.driver).drag_and_drop(edit_form_page.advanced_fields.poll, edit_form_page.body).perform()

        wait.until(lambda condition: edit_form_page.poll.poll_body.is_displayed())
        edit_form_page.hover_on_element(edit_form_page.poll.poll_body)
        edit_form_page.poll.poll_body.click()
        edit_form_page.wait.until(lambda condition: edit_form_page.poll.general_tab.is_displayed())
        edit_form_page.poll.general.description.send_keys(POLL_INFO['description'])
        edit_form_page.poll.general.poll_question.clear()
        edit_form_page.poll.general.poll_question.send_keys(POLL_INFO['question'])
        edit_form_page.poll.general.select_poll_type("Radio Buttons")
        edit_form_page.poll.general.poll_choice[0].text.clear()
        edit_form_page.poll.general.poll_choice[0].text.send_keys(POLL_INFO['choice_one'])
        edit_form_page.poll.general.poll_choice[0].check_default.click()
        edit_form_page.poll.general.poll_choice[1].text.clear()
        edit_form_page.poll.general.poll_choice[1].text.send_keys(POLL_INFO['choice_two'])
        edit_form_page.poll.general.poll_choice[2].text.clear()
        edit_form_page.poll.general.poll_choice[2].text.send_keys(POLL_INFO['choice_three'])
        edit_form_page.poll.general.required.click()
        edit_form_page.update.click()

        # Creating the poll card
        create_new_poll_card = CreateNewPollCard(self.driver, configuration.system.base_url_web, post_type='bw_poll').open()
        create_new_poll_card.select_a_poll_dropdown.click()
        create_new_poll_card.select_a_poll(POLL_INFO['question']).click()
        create_new_poll_card.card_image.click()
        create_new_poll_card.select_widget_image.upload_image.send_keys(IMAGE_PATH)
        create_new_poll_card.select_widget_image.add_image_to_widget_button.click()
        wait.until(lambda condition: create_new_poll_card.image_cropper.is_cropper_modal_visible is True)
        wait.until(lambda condition: create_new_poll_card.image_cropper.is_crop_one_visible is True)
        wait.until(lambda condition: create_new_poll_card.image_cropper.is_cropper_box_visible is True)
        create_new_poll_card.image_cropper.image_one_crop()
        create_new_poll_card.image_cropper.crop_image.click()
        create_new_poll_card.image_cropper.next.click()
        wait.until(lambda condition: create_new_poll_card.image_cropper.is_cropper_box_visible is True)
        wait.until(lambda condition: create_new_poll_card.image_cropper.is_crop_one_visible is False)
        wait.until(lambda condition: create_new_poll_card.image_cropper.is_crop_two_visible is True)
        create_new_poll_card.image_cropper.image_two_crop()
        create_new_poll_card.image_cropper.crop_image.click()
        create_new_poll_card.image_cropper.done.click()
        wait.until(lambda condition: create_new_poll_card.image_cropper.is_cropper_modal_visible is False)

        create_new_poll_card.publish.click()

        delete_downloaded_image(IMAGE_PATH)

        # Creating a new page
        create_new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        create_new_page.title.send_keys(PAGE_TITLE)
        create_new_page.page_builder_section.click()
        click(create_new_page.publish)
        create_new_page.wait.until(lambda condition: self.driver.page_source.should.contain("Update"))

        # Opening a new tab
        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # Adding a single card to the page from Page Builder
        new_page = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        new_page.wpheader.page_builder.click()
        if new_page.builder_panel.is_panel_visible is False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.single_card, new_page.page_builder.body).perform()

        # Selecting the poll card that was created
        single_card = PageBuilderSingleCard(new_page)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Poll Card")
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Poll")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(POLL_INFO['question']).click()
        single_card.edit_card.save.click()
        single_card.save()
        wait.until(lambda s: len(new_page.user_facing_modules.single_cards) is 1)
        wait.until(lambda s: new_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        len(new_page.user_facing_modules.single_cards).should.equal(1)
        len(new_page.user_facing_modules.single_cards[0].cards).should.equal(1)
        new_page.page_builder.done.click()
        new_page.wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()
        wait.until(lambda condition: new_page.wpheader.is_page_builder_menu_item_displayed)
        wait.until(lambda condition: len(new_page.user_facing_modules.single_cards) is 1)
        new_page.open()
        wait.until(lambda condition: new_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        new_page.user_facing_modules.single_cards[0].cards[0].card_title.get_attribute("textContent").should.equal(POLL_INFO['question'])

        self.driver.close()

        # Switching to tab 1
        self.driver.switch_to_window(self.driver.window_handles[0])

        # Deleting the created page
        all_pages = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages.search_input.send_keys(PAGE_TITLE)
        all_pages.search_button.click()
        all_pages.rows[0].title.get_attribute("textContent").should.match(PAGE_TITLE)
        all_pages.rows[0].hover_on_title()
        all_pages.rows[0].delete.click()
        all_pages.rows.should.be.empty

        all_forms_page.open()
        all_forms_page.search_forms_input.send_keys(FORM_INFO['title'])
        all_forms_page.search_forms_button.click()
        all_forms_page.rows[0].title.get_attribute("textContent").should.match(FORM_INFO['title'])
        all_forms_page.rows[0].hover_on_title()
        all_forms_page.rows[0].trash.click()
        all_forms_page.rows.should.be.empty

        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_input.send_keys(POLL_INFO['question'])
        all_cards_page.search_button.click()
        all_cards_page.rows[0].title.get_attribute("textContent").should.match(POLL_INFO['question'])
        all_cards_page.rows[0].hover_on_title()
        all_cards_page.rows[0].delete.click()
        all_cards_page.rows.should.be.empty

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_input.send_keys(IMAGE_TITLE, Keys.RETURN)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=[IndexError, ElementNotInteractableException])
        wait.until(lambda condition: len(media_library_page.rows) is 1)
        media_library_page.rows[0].hover_on_title()
        wait.until(lambda condition: media_library_page.rows[0].delete.is_displayed())
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        wait.until(lambda condition: media_library_page.rows.should.be.empty)
