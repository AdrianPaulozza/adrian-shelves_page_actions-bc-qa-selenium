import pytest
import allure
import sure
import configuration.system
import configuration.user
from mimesis import Text, Internet
from selenium.common.exceptions import ElementNotVisibleException, NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.page_builder import PageBuilderPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_custom_card import CreateNewCustomCard
from pages.web.wpadmin.v3.create_new_custom_page import CreateNewCustomPage
from utils.image_download_helper import *
from pages.web.components.page_builder.page_builder_modules import PageBuilderSingleCard
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.create_new_content.create_new_blog_post import CreateNewBlogPostPage
from utils.selenium_helpers import click


PAGE = "bibliocommons-settings"
CUSTOM_PAGE_TITLE = ''.join(Text('en').words(quantity=3))
CUSTOM_CARD_INFO = {
    'title': ''.join(Text('en').words(quantity=3)),
    'url': Internet('en').home_page(),
    'description': ' '.join(Text('en').words(quantity=6))
}
IMAGE_TITLE = Text('en').words(quantity=1)[0]
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C69756: Scheduled/Expiry labels")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69756", "TestRail")
class TestC69756:
    def test_C69756(self):

        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as implementing
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()

        # Logging out
        self.driver.delete_all_cookies()

        download_image("https://bit.ly/2EdPMqG", IMAGE_PATH)

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        # Creating a new custom card
        new_custom_card = CreateNewCustomCard(self.driver, configuration.system.base_url_web, post_type='bw_custom_card').open()
        new_custom_card.card_title.send_keys(CUSTOM_CARD_INFO['title'])
        new_custom_card.card_image.click()
        new_custom_card.select_widget_image.upload_image.send_keys(IMAGE_PATH)
        click(new_custom_card.select_widget_image.add_image_to_widget_button)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=NoSuchElementException)
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible is True)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible is True)
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible is True)
        new_custom_card.image_cropper.image_one_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.next.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible is True)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible is False)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_visible is True)
        new_custom_card.image_cropper.image_two_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.done.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible is False)
        new_custom_card.image_cropper.is_crop_one_preview_visible.should.be.true
        new_custom_card.image_cropper.is_crop_two_preview_visible.should.be.true
        new_custom_card.card_url.send_keys(CUSTOM_CARD_INFO['url'])
        new_custom_card.card_description.send_keys(CUSTOM_CARD_INFO['description'])
        new_custom_card.select_resource_type("Catalog Search")
        new_custom_card.scroll_to_top()
        new_custom_card.publish.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_preview_visible is True)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_preview_visible is True)
        self.driver.refresh()
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_preview_visible is True)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_preview_visible is True)

        delete_downloaded_image(IMAGE_PATH)

        # Creating a new custom page
        create_new_custom_page = CreateNewCustomPage(self.driver, configuration.system.base_url_web, post_type='page', page_type='custom').open()
        create_new_custom_page.title.send_keys(CUSTOM_PAGE_TITLE)
        create_new_custom_page.publish.click()
        create_new_custom_page.wait.until(lambda condition: self.driver.page_source.should.contain("Update"))

        # Opening a new tab
        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # Opening the custom page in the frontend
        custom_page = PageBuilderPage(self.driver, configuration.system.base_url_web + CUSTOM_PAGE_TITLE).open()
        custom_page.wpheader.page_builder.click()
        if custom_page.builder_panel.is_panel_visible is False:
            custom_page.page_builder.add_content.click()
        custom_page.builder_panel.modules_tab.click()

        # Adding a single card to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(custom_page.builder_panel.single_card, custom_page.page_builder.body).perform()

        # Adding the custom card that was created, and setting 'Active' label to it
        single_card = PageBuilderSingleCard(custom_page)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Custom Card")
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Custom Card")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(CUSTOM_CARD_INFO['title']).click()
        single_card.edit_card.starting.click()
        single_card.edit_card.schedule_picker.today.click()
        single_card.edit_card.save.click()
        # Asserting that the 'Active' label is displayed
        single_card.edit_card.active_label[0].is_displayed().should.be.true
        single_card.edit_card.active_label[0].get_attribute("textContent").should.contain("Active")
        single_card.save()
        custom_page.page_builder.done.click()
        custom_page.wait.until(lambda condition: custom_page.page_builder.publish.is_displayed())
        custom_page.page_builder.publish.click()
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=[ElementNotVisibleException, IndexError])
        wait.until(lambda condition: custom_page.wpheader.is_page_builder_menu_item_displayed)
        wait.until(lambda condition: len(custom_page.user_facing_modules.single_cards) is 1)
        custom_page.open()
        wait.until(lambda condition: len(custom_page.user_facing_modules.single_cards) is 1)

        custom_page.wpheader.page_builder.click()
        if custom_page.builder_panel.is_panel_visible is False:
            custom_page.page_builder.add_content.click()
        custom_page.builder_panel.modules_tab.click()

        # Adding a single card to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(custom_page.builder_panel.single_card, custom_page.page_builder.body).perform()

        # Adding the custom card that was created, and setting 'Active until' label to it
        single_card = PageBuilderSingleCard(custom_page)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Custom Card")
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Custom Card")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(CUSTOM_CARD_INFO['title']).click()
        single_card.edit_card.ending.click()
        single_card.edit_card.schedule_picker.days(41).click()
        single_card.edit_card.save.click()
        # Asserting that the 'Active until' label is displayed
        single_card.edit_card.active_label[0].is_displayed().should.be.true
        single_card.edit_card.active_label[0].get_attribute("textContent").should.contain("Active until")
        single_card.save()
        custom_page.page_builder.done.click()
        custom_page.wait.until(lambda condition: custom_page.page_builder.publish.is_displayed())
        custom_page.page_builder.publish.click()
        wait.until(lambda condition: custom_page.wpheader.is_page_builder_menu_item_displayed)
        wait.until(lambda condition: len(custom_page.user_facing_modules.single_cards) is 2)
        custom_page.open()
        wait.until(lambda condition: len(custom_page.user_facing_modules.single_cards) is 2)

        custom_page.wpheader.page_builder.click()
        if custom_page.builder_panel.is_panel_visible is False:
            custom_page.page_builder.add_content.click()
        custom_page.builder_panel.modules_tab.click()

        # Adding a single card to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(custom_page.builder_panel.single_card, custom_page.page_builder.body).perform()

        # Adding the custom card that was created, and setting 'Expired' label to it
        single_card = PageBuilderSingleCard(custom_page)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Custom Card")
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Custom Card")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(CUSTOM_CARD_INFO['title']).click()
        single_card.edit_card.ending.click()
        single_card.edit_card.schedule_picker.previous_month.click()
        single_card.edit_card.schedule_picker.days(0).click()
        single_card.edit_card.save.click()
        # Asserting that the 'Expired' label is displayed
        single_card.edit_card.expired_label[0].is_displayed().should.be.true
        single_card.edit_card.expired_label[0].get_attribute("textContent").should.contain("Expired")
        single_card.save()
        custom_page.page_builder.done.click()
        custom_page.wait.until(lambda condition: custom_page.page_builder.publish.is_displayed())
        custom_page.page_builder.publish.click()
        wait.until(lambda condition: custom_page.wpheader.is_page_builder_menu_item_displayed)
        wait.until(lambda condition: len(custom_page.user_facing_modules.single_cards) is 3)
        custom_page.open()
        wait.until(lambda condition: len(custom_page.user_facing_modules.single_cards) is 3)

        custom_page.wpheader.page_builder.click()
        if custom_page.builder_panel.is_panel_visible is False:
            custom_page.page_builder.add_content.click()
        custom_page.builder_panel.modules_tab.click()

        # Adding a single card to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(custom_page.builder_panel.single_card, custom_page.page_builder.body).perform()

        # Adding the custom card that was created, and setting 'Expired' label to it
        single_card = PageBuilderSingleCard(custom_page)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Custom Card")
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Custom Card")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(CUSTOM_CARD_INFO['title']).click()
        single_card.edit_card.starting.click()
        single_card.edit_card.schedule_picker.next_month.click()
        single_card.edit_card.schedule_picker.days(10).click()
        single_card.edit_card.save.click()
        # Asserting that the 'Scheduled for' label is displayed
        single_card.edit_card.scheduled_for_label[0].is_displayed().should.be.true
        single_card.edit_card.scheduled_for_label[0].get_attribute("textContent").should.contain("Scheduled for")
        single_card.save()
        custom_page.page_builder.done.click()
        custom_page.wait.until(lambda condition: custom_page.page_builder.publish.is_displayed())
        custom_page.page_builder.publish.click()
        wait.until(lambda condition: custom_page.wpheader.is_page_builder_menu_item_displayed)
        wait.until(lambda condition: len(custom_page.user_facing_modules.single_cards) is 4)
        custom_page.open()
        wait.until(lambda condition: len(custom_page.user_facing_modules.single_cards) is 4)

        self.driver.close()

        # Switching to tab 1
        self.driver.switch_to_window(self.driver.window_handles[0])

        # Logging out
        self.driver.delete_all_cookies()

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        # Deleting the uploaded image from the media library
        new_blog_post = CreateNewBlogPostPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        click(new_blog_post.add_media)
        new_blog_post.insert_media.select_image_from_list(0).click()
        new_blog_post.insert_media.delete_image_link.click()
        new_blog_post.insert_media.accept_alert_to_delete_image()
        new_blog_post.insert_media.media_library_tab.click()
        new_blog_post.insert_media.close_button.click()

        # Deleting the created custom card
        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_input.send_keys(CUSTOM_CARD_INFO['title'])
        all_cards_page.search_button.click()
        all_cards_page.rows[0].title.get_attribute("textContent").should.match(CUSTOM_CARD_INFO['title'])
        all_cards_page.rows[0].hover_on_title()
        all_cards_page.rows[0].delete.click()
        all_cards_page.rows.should.be.empty

        # Deleting the created page
        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_input.send_keys(CUSTOM_PAGE_TITLE)
        all_pages_page.search_button.click()
        all_pages_page.rows[0].title.get_attribute("textContent").should.match(CUSTOM_PAGE_TITLE)
        all_pages_page.rows[0].hover_on_title()
        all_pages_page.rows[0].delete.click()
        all_pages_page.rows.should.be.empty
