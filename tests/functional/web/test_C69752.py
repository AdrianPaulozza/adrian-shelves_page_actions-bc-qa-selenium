import pytest
import allure
import sure
import configuration.system
import configuration.user
from mimesis import Text
from selenium.common.exceptions import ElementNotInteractableException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from utils.image_download_helper import *
from utils.selenium_helpers import click
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.create_new_content.create_new_news_post import CreateNewNewsPostPage
from pages.web.page_builder import PageBuilderPage
from pages.web.user import UserPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderSingleCard
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.all_contents_page.all_news import AllNewsPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage


PAGE = 'bibliocommons-settings'
NEWS_TITLE = '-'.join(Text('en').words(quantity=3))
NEWS_SUBTITLE = ' '.join(Text('en').words(quantity=3))
NEWS_DESCRIPTION = Text('en').text(quantity=5)
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
IMAGE_TITLE = ' '.join(Text('en').words(quantity=3))
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")
CONTENT_TYPE = "News"


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C69752: News Card - Create")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69752", "TestRail")
class TestC69752:
    def test_C69752(self):

        # Logged in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.urls_web[1]).open()
        login_page.log_in(configuration.user.user['web']['stage']['admin']['name'], configuration.user.user['web']['stage']['admin']['password'])

        settings_general_tab = SettingsGeneralTabPage(self.driver, configuration.system.urls_web[1], page=PAGE, tab='generic').open()
        click(settings_general_tab.disable_structured_tags)
        settings_general_tab.save_changes.click()

        # Change V3 Status = Implementing
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.urls_web[1], page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()
        # The below code is in case the V3 menu is displayed instead of the V2 menu
        if settings_system_settings_tab.wpsidemenu.is_menu_switch_to_v2_menu_displayed is False:
            settings_system_settings_tab.wpsidemenu.menu_switch_to_v3_menu.click()
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=ElementNotInteractableException)
        wait.until(lambda condition: settings_system_settings_tab.wpsidemenu.is_menu_switch_to_v2_menu_displayed is True)
        self.driver.delete_all_cookies()

        # PART 1 - Create a new News Post
        login_page = LoginPage(self.driver, configuration.system.urls_web[1]).open()
        login_page.log_in(configuration.user.user['web']['stage']['libadmin']['name'], configuration.user.user['web']['stage']['libadmin']['password'])

        download_image("https://bit.ly/2IOmAJs", IMAGE_PATH)
        new_news_post = CreateNewNewsPostPage(self.driver, configuration.system.urls_web[1], post_type='bccms_news').open()
        new_news_post.page_heading.text.should.match("Add New Article")
        new_news_post.title.send_keys(NEWS_TITLE)
        new_news_post.subtitle.send_keys(NEWS_SUBTITLE)

        click(new_news_post.visual)
        new_news_post.add_media.click()
        new_news_post.insert_media.upload_image.send_keys(IMAGE_PATH)
        new_news_post.insert_media.add_image_to_widget_button.click()
        self.driver.switch_to.frame(new_news_post.visual_area.iframe)
        new_news_post.visual_area.body.send_keys(NEWS_DESCRIPTION)
        self.driver.switch_to.default_content()

        new_news_post.is_free_text_tags_field_displayed.should.be.true
        num_of_taxonomies = 3
        for i in range(num_of_taxonomies):
            tag = Text('en').words(quantity=1)[0]
            new_news_post.free_text_tags.send_keys(tag)
            new_news_post.add_free_text_tags.click()
        new_news_post.are_free_text_tags_added.should.be.true

        click(new_news_post.autofill_text_fields)
        new_news_post.card_image.click()
        new_news_post.select_widget_image.select_image_from_list(0).click()
        click(new_news_post.select_widget_image.add_image_to_widget_button)
        wait.until(lambda condition: new_news_post.image_cropper.is_cropper_modal_visible is True)
        wait.until(lambda condition: new_news_post.image_cropper.is_crop_one_visible is True)
        wait.until(lambda condition: new_news_post.image_cropper.is_cropper_box_visible is True)
        new_news_post.image_cropper.image_one_crop()
        new_news_post.image_cropper.crop_image.click()
        new_news_post.image_cropper.next.click()
        wait.until(lambda condition: new_news_post.image_cropper.is_cropper_box_visible is True)
        wait.until(lambda condition: new_news_post.image_cropper.is_crop_one_visible is False)
        wait.until(lambda condition: new_news_post.image_cropper.is_crop_two_visible is True)
        new_news_post.image_cropper.image_two_crop()
        new_news_post.image_cropper.crop_image.click()
        new_news_post.image_cropper.done.click()
        wait.until(lambda condition: new_news_post.image_cropper.is_cropper_modal_visible is False)
        new_news_post.scroll_to_top()
        click(new_news_post.publish)

        # Verify news is added to All News page
        all_news_page = AllNewsPage(self.driver, configuration.system.urls_web[1], post_type='bccms_news').open()
        all_news_page.search_input.send_keys(NEWS_TITLE)
        all_news_page.search_button.click()
        all_news_page.rows[0].title.text.should.match(NEWS_TITLE)
        len(all_news_page.rows[0].tags).should.equal(num_of_taxonomies)

        # Verify news is added to All Cards page
        all_cards_page = AllCardsPage(self.driver, configuration.system.urls_web[1], page='bw-content-card').open()
        all_cards_page.search_input.send_keys(NEWS_TITLE)
        all_cards_page.search_button.click()
        all_cards_page.rows[0].title.text.should.match(NEWS_TITLE)
        all_cards_page.rows[0].card_type.text.should.match(CONTENT_TYPE)
        taxonomies = all_cards_page.rows[0].taxonomies
        len(taxonomies).should.equal(num_of_taxonomies)

        # Verify image is added to Media Library page with associated content
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[1], mode='list').open()
        media_library_page.search_input.send_keys(IMAGE_TITLE, Keys.RETURN)
        media_library_page.rows[0].title.text.should.match(IMAGE_TITLE)
        media_library_page.rows[0].uploaded_to_content_title.text.should.match(NEWS_TITLE)

        # PART 2 - Create a New Page
        new_page = CreateNewPagePage(self.driver, configuration.system.urls_web[1], post_type='page').open()
        new_page.title.send_keys(PAGE_TITLE)
        new_page.page_builder_section.click()
        click(new_page.publish)

        page_staff = PageBuilderPage(self.driver, configuration.system.urls_web[1] + PAGE_TITLE).open()
        page_staff.wait.until(lambda s: page_staff.wpheader.is_wp_admin_header_displayed)
        page_staff.wpheader.page_builder.click()
        if page_staff.builder_panel.is_panel_visible is False:
            page_staff.page_builder.add_content.click()
        page_staff.builder_panel.modules_tab.click()

        # Adding a card module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(page_staff.builder_panel.single_card,
                                                page_staff.page_builder.body).perform()

        # Select single card module
        single_card = PageBuilderSingleCard(page_staff)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Single Card - " + CONTENT_TYPE)
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type(CONTENT_TYPE)
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(NEWS_TITLE).click()
        single_card.edit_card.save.click()
        single_card.save()
        wait.until(lambda s: len(page_staff.user_facing_modules.single_cards) is 1)
        wait.until(lambda s: page_staff.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        page_staff.page_builder.done.click()
        page_staff.wait.until(lambda condition: page_staff.page_builder.publish.is_displayed())
        page_staff.page_builder.publish.click()
        self.driver.execute_script("arguments[0].className = 'menupop with-avatar hover'", page_staff.wpheader.my_account)
        page_staff.wpheader.wait.until(lambda s: page_staff.wpheader.is_my_account_submenu_displayed)
        page_staff.wpheader.my_account_log_out.click()

        # PART 3 - Patron view
        login_page.open()
        login_page.log_in(configuration.user.user['web']['stage']['admin']['name'], configuration.user.user['web']['stage']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.urls_web[1], page=PAGE, tab='system').open()
        click(settings_system_settings_tab.v3_status_enabled)
        settings_system_settings_tab.save_changes.click()
        settings_system_settings_tab.wpsidemenu.is_menu_switch_to_v2_menu_displayed.should.be.false
        self.driver.delete_all_cookies()

        page_patron = UserPage(self.driver, configuration.system.urls_web[1] + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.single_cards).should.equal(1)
        len(page_patron.user_facing_modules.single_cards[0].cards).should.equal(1)
        page_patron.user_facing_modules.single_cards[0].cards[0].is_card_content_type_displayed.should.be.true
        page_patron.user_facing_modules.single_cards[0].cards[0].card_content_type.text.should.match(CONTENT_TYPE.upper())
        page_patron.user_facing_modules.single_cards[0].cards[0].card_title.text.should.match(NEWS_TITLE)
        self.driver.delete_all_cookies()

        # PART 4 - Delete contents
        login_page.open()
        login_page.log_in(configuration.user.user['web']['stage']['libadmin']['name'], configuration.user.user['web']['stage']['libadmin']['password'])

        all_cards_page = AllCardsPage(self.driver, configuration.system.urls_web[1], page='bw-content-card').open()
        all_cards_page.search_input.send_keys(NEWS_TITLE, Keys.RETURN)
        all_cards_page.rows[0].hover_on_title()
        all_cards_page.rows[0].delete.click()
        all_cards_page.rows.should.be.empty

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[1], mode='list').open()
        media_library_page.search_input.send_keys(IMAGE_TITLE, Keys.RETURN)
        media_library_page.rows[0].hover_on_title()
        wait.until(lambda condition: media_library_page.rows[0].delete.is_displayed())
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        media_library_page.rows.should.be.empty

        all_pages_page = AllPagesPage(self.driver, configuration.system.urls_web[1], post_type='page').open()
        all_pages_page.search_input.send_keys(PAGE_TITLE, Keys.RETURN)
        all_pages_page.rows[0].title.text.should.match(PAGE_TITLE)
        all_pages_page.rows[0].hover_on_title()
        all_pages_page.rows[0].delete.click()
        all_pages_page.rows.should.be.empty

        delete_downloaded_image(IMAGE_PATH)

        self.driver.delete_all_cookies()
