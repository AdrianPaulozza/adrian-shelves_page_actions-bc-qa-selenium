import pytest
import allure
import sure
import configuration.system
import configuration.user
from mimesis import Text, Internet
from selenium.webdriver.common.keys import Keys
from utils.image_download_helper import *
from utils.selenium_helpers import click
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from random import choice
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_custom_card import CreateNewCustomCard
from pages.web.staff_base import StaffBasePage
from pages.web.page_builder import PageBuilderPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderSingleCard
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage


PAGE = 'bibliocommons-settings'
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Custom Card"
RESOURCE_TYPE = choice(["Catalog Search", "Website", "Article", "Video", "Exhibit", "Archive Collection", "Photo Gallery"])
IMAGE_TITLE = ' '.join(Text('en').words(quantity=3))
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")
CUSTOM_CARD_INFO = {
    'title': '-'.join(Text('en').words(quantity=3)),
    'url': Internet('en').home_page(),
    'description': ' '.join(Text('en').words(quantity=15))
}


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C76117: Custom Card - Create - With label")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/76117", "TestRail")
class TestC76117:
    def test_C76117(self):
        # Logged in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])

        settings_general_tab = SettingsGeneralTabPage(self.driver, configuration.system.urls_web[3], page=PAGE, tab='generic').open()
        click(settings_general_tab.disable_structured_tags)
        settings_general_tab.save_changes.click()
        self.driver.delete_all_cookies()

        # Change V3 Status = Implementing
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.urls_web[3], page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()
        settings_system_settings_tab.wpsidemenu.is_menu_switch_to_v2_menu_displayed.should.be.true
        self.driver.delete_all_cookies()

        # PART 1 - Create a Custom Card (random type)
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        download_image("https://bit.ly/2IOmAJs", IMAGE_PATH)
        new_custom_card = CreateNewCustomCard(self.driver, configuration.system.urls_web[3], post_type='bw_custom_card').open()
        new_custom_card.card_title.send_keys(CUSTOM_CARD_INFO['title'])
        new_custom_card.card_url.send_keys(CUSTOM_CARD_INFO['url'])
        new_custom_card.card_description.send_keys(CUSTOM_CARD_INFO['description'])
        new_custom_card.select_resource_type(RESOURCE_TYPE)

        new_custom_card.is_free_text_tags_field_displayed.should.be.true
        num_of_taxonomies = 3
        for i in range(num_of_taxonomies):
            tag = Text('en').words(quantity=1)[0]
            new_custom_card.free_text_tags.send_keys(tag)
            new_custom_card.add_free_text_tags.click()
        new_custom_card.are_free_text_tags_added.should.be.true

        click(new_custom_card.card_image)
        new_custom_card.select_widget_image.upload_files_tab.click()
        new_custom_card.select_widget_image.upload_image.send_keys(IMAGE_PATH)
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda condition: new_custom_card.select_widget_image.add_image_to_widget_button.is_displayed())
        click(new_custom_card.select_widget_image.add_image_to_widget_button)
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible is True)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible is True)
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible is True)
        new_custom_card.image_cropper.next.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible is True)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible is False)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_visible is True)
        new_custom_card.image_cropper.image_two_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.done.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible is False)
        new_custom_card.scroll_to_top()
        click(new_custom_card.publish)

        # PART 2 - Verify if it's actually published
        # Verify card is added to All Cards page
        all_cards_page = AllCardsPage(self.driver, configuration.system.urls_web[3], page='bw-content-card').open()
        all_cards_page.search_input.send_keys(CUSTOM_CARD_INFO['title'], Keys.RETURN)
        all_cards_page.rows[0].title.text.should.match(CUSTOM_CARD_INFO['title'])
        all_cards_page.rows[0].card_type.text.should.match(CONTENT_TYPE)
        taxonomies = all_cards_page.rows[0].taxonomies
        len(taxonomies).should.equal(num_of_taxonomies)

        # Verify image is added to Media Library page with associated content
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.search_input.send_keys(IMAGE_TITLE, Keys.RETURN)
        media_library_page.rows[0].title.text.should.match(IMAGE_TITLE)
        media_library_page.rows[0].uploaded_to_content_title.text.should.match(CUSTOM_CARD_INFO['title'])

        # PART 3 - Create a New Page
        new_page = CreateNewPagePage(self.driver, configuration.system.urls_web[3], post_type='page').open()
        new_page.title.send_keys(PAGE_TITLE)
        new_page.page_builder_section.click()
        click(new_page.publish)

        page_staff = PageBuilderPage(self.driver, configuration.system.urls_web[3] + PAGE_TITLE).open()
        page_staff.wait.until(lambda s: page_staff.wpheader.is_wp_admin_header_displayed)
        page_staff.wpheader.page_builder.click()

        # Select single card module
        if page_staff.builder_panel.is_panel_visible is False:
            page_staff.wpheader.add_content.click()
        page_staff.builder_panel.modules_tab.click()
        ActionChains(self.driver).drag_and_drop(page_staff.builder_panel.single_card, page_staff.page_builder.body).perform()
        single_card = PageBuilderSingleCard(page_staff)
        click(single_card.advanced_tab)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Single Card - " + CONTENT_TYPE)
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type(CONTENT_TYPE)
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(CUSTOM_CARD_INFO['title']).click()
        single_card.edit_card.save.click()
        single_card.save()
        wait.until(lambda s: page_staff.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)

        page_staff.page_builder.done.click()
        page_staff.wait.until(lambda condition: page_staff.page_builder.publish.is_displayed())
        page_staff.page_builder.publish.click()
        self.driver.execute_script("arguments[0].className = 'menupop with-avatar hover'",
                                   page_staff.wpheader.my_account)
        page_staff.wpheader.wait.until(lambda s: page_staff.wpheader.is_my_account_submenu_displayed)
        page_staff.wpheader.my_account_log_out.click()

        # PART 4 - Patron view
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.urls_web[3], page=PAGE, tab='system').open()

        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()
        settings_system_settings_tab.wpsidemenu.is_menu_switch_to_v2_menu_displayed.should.be.false
        self.driver.delete_all_cookies()

        page_patron = StaffBasePage(self.driver, configuration.system.urls_web[3] + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.single_cards).should.equal(1)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_title.text.should.match(CUSTOM_CARD_INFO['title'])
        page_patron.user_facing_modules.single_cards[0].cards[0].is_card_content_type_displayed.should.be.true
        page_patron.user_facing_modules.single_cards[0].cards[0].card_content_type.text.should.match(RESOURCE_TYPE.upper())

        self.driver.delete_all_cookies()

        # PART 5 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        all_cards_page = AllCardsPage(self.driver, configuration.system.urls_web[3], page='bw-content-card').open()
        all_cards_page.search_input.send_keys(CUSTOM_CARD_INFO['title'], Keys.RETURN)
        all_cards_page.rows[0].hover_on_title()
        all_cards_page.rows[0].delete.click()
        all_cards_page.rows.should.be.empty

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.search_input.send_keys(IMAGE_TITLE, Keys.RETURN)
        media_library_page.rows[0].hover_on_title()
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        media_library_page.rows.should.be.empty

        all_pages_page = AllPagesPage(self.driver, configuration.system.urls_web[3], post_type='page').open()
        all_pages_page.search_input.send_keys(PAGE_TITLE, Keys.RETURN)
        all_pages_page.rows[0].title.text.should.match(PAGE_TITLE)
        all_pages_page.rows[0].hover_on_title()
        all_pages_page.rows[0].delete.click()
        all_pages_page.rows.should.be.empty

        delete_downloaded_image(IMAGE_PATH)

        self.driver.delete_all_cookies()
