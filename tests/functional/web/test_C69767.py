import pytest
import allure
import sure
from mimesis import Text
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.dashboard import DashboardPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.explore import ExplorePagePage
from pages.web.page_builder import PageBuilderPage
from pages.web.user import UserPage
from selenium.common.exceptions import ElementNotInteractableException
from selenium.webdriver.support.ui import WebDriverWait
from utils.selenium_helpers import click


BASE_URL = "http://chicago.local.bibliocms.com/"
PAGE = "bibliocommons-settings"
NETWORK_ADMIN = "admin"
NETWORK_ADMIN_PASSWORD = "password"
LIB_ADMIN = "libadmin"
LIB_ADMIN_PASSWORD = "qoR52^9rEDRm2pNm"
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))


@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C69767: Launch PB 2 - Through PB link")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69767", "TestRail")
class TestC69767:
    def test_C69767(self):
        # Logging in as network admin
        login_page = LoginPage(self.driver, BASE_URL).open()
        login_page.log_in(NETWORK_ADMIN, NETWORK_ADMIN_PASSWORD)

        # Setting V3 status as Implementing
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, BASE_URL, page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()
        self.driver.delete_all_cookies()

        # Logging as lib admin
        login_page = LoginPage(self.driver, BASE_URL).open()
        login_page.log_in(LIB_ADMIN, LIB_ADMIN_PASSWORD)

        dashboard_page = DashboardPage(self.driver, BASE_URL, page="bw-dashboard").open()
        dashboard_page.wait.until(lambda s: dashboard_page.is_page_heading_displayed)
        dashboard_page.page_heading.text.should.equal("Dashboard")
        if dashboard_page.wpsidemenu.is_menu_switch_to_v3_menu_displayed is False:
            dashboard_page.wpsidemenu.menu_switch_to_v2_menu.click()
        wait = WebDriverWait(self.driver, 15, poll_frequency=2,
                             ignored_exceptions=[AssertionError, ElementNotInteractableException])
        wait.until(lambda condition: dashboard_page.wpsidemenu.is_menu_switch_to_v3_menu_displayed)
        dashboard_page.wpsidemenu.is_menu_switch_to_v3_menu_displayed.should.be.true
        self.driver.execute_script("arguments[0].className = 'menupop hover'", dashboard_page.wpheader.add_new_content)
        dashboard_page.wpheader.wait.until(lambda s: dashboard_page.wpheader.is_add_new_content_submenu_displayed)
        len(dashboard_page.wpheader.add_new_content_submenu_list).should.equal(10) # To check whether the V3 items is displayed in dropdown
        click(dashboard_page.wpheader.add_new_content_submenu_list[1])

        # Creating new page
        create_new_page = CreateNewPagePage(self.driver)
        create_new_page.wait.until(lambda s: create_new_page.is_heading_displayed)
        create_new_page.heading.text.should.contain("New Page")
        create_new_page.title.send_keys(PAGE_TITLE)
        click(create_new_page.publish)
        create_new_page.wait.until(lambda s: create_new_page.is_success_message_displayed)
        create_new_page.is_success_message_displayed.should.be.true
        if create_new_page.wpsidemenu.is_menu_switch_to_v2_menu_displayed is False:
            create_new_page.wpsidemenu.menu_switch_to_v3_menu.click()
        wait = WebDriverWait(self.driver, 15, poll_frequency=2, ignored_exceptions=[AssertionError, ElementNotInteractableException])
        wait.until(lambda condition: create_new_page.wpsidemenu.is_menu_switch_to_v2_menu_displayed)
        create_new_page.wpsidemenu.is_menu_switch_to_v2_menu_displayed.should.be.true
        create_new_page.wpsidemenu.menu_all_content.click()
        create_new_page.wpsidemenu.submenu_all_pages.click()

        # Launching PB from all pages
        all_pages = AllPagesPage(self.driver)
        all_pages.wait.until(lambda s: all_pages.page_builder_filter.is_displayed())
        all_pages.page_builder_filter.click()
        all_pages.search_input.send_keys(PAGE_TITLE)
        all_pages.search_button.click()
        all_pages.wait.until(lambda s: all_pages.is_search_results_displayed)
        all_pages.rows[0].hover_on_title()
        all_pages.rows[0].page_builder.click()

        # Page Builder
        page_builder_page = PageBuilderPage(self.driver)
        page_builder_page.page_builder.done.click()
        #page_builder_page.wait.until(lambda condition: page_builder_page.page_builder.publish.is_displayed())
        click(page_builder_page.page_builder.publish)
        self.driver.delete_all_cookies()

        # As patron
        user_page = UserPage(self.driver, BASE_URL + PAGE_TITLE).open()
        user_page.is_error_title_displayed.should.be.true

        # Explore Page
        explore_page = ExplorePagePage(self.driver, BASE_URL + "explore").open()
        explore_page.is_explore_heading_displayed.should.be.false
