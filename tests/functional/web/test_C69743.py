import pytest
import allure
import sure
import configuration.user
import configuration.system
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.create_new_content.create_new_blog_post import CreateNewBlogPostPage
from pages.web.wpadmin.v3.create_new_content.create_new_news_post import CreateNewNewsPostPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_list_card import CreateNewListCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_catalog_comment_card import CreateNewCatalogCommentCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_event_card import CreateNewEventCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_online_resource_card import CreateNewOnlineResourceCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_poll_card import CreateNewPollCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_twitter_card import CreateNewTwitterCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_custom_card import CreateNewCustomCard
from pages.web.wpadmin.v3.create_new_featured_block.create_new_banner import CreateNewBannerPage
from pages.web.wpadmin.v3.create_new_featured_block.create_new_hero_slide import CreateNewHeroSlidePage

PAGE = "bibliocommons-settings"

TAXONOMIES = ['Audience', 'Related Format', 'Programs and Campaigns', 'Genre', 'Topic', 'Tags']
taxonomies_terms = []


@pytest.fixture(scope='class')
def login_and_setup(request, selenium_setup_and_teardown):
    driver = request.cls.driver

    login_page = LoginPage(driver, configuration.system.base_url_web).open()
    login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])
    settings_system_settings_tab = SettingsSystemSettingsTabPage(driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
    settings_system_settings_tab.v3_status_implementing.click()
    settings_system_settings_tab.save_changes.click()

    settings_general_tab = SettingsGeneralTabPage(driver, configuration.system.base_url_web, page=PAGE, tab='generic').open()
    settings_general_tab.enable_make_tags_a_structured_taxonomy.click()
    settings_general_tab.save_changes.click()

    new_blog_post = CreateNewBlogPostPage(driver, configuration.system.base_url_web, post_type='post').open()

    # Creating a dynamic list of taxonomy terms, so that the test can be run on any environment
    for index, taxonomy in enumerate(TAXONOMIES):
        new_blog_post.taxonomy(taxonomy).click()
        taxonomies_terms.append(new_blog_post.taxonomy_terms(taxonomy)[0])
        if index == len(TAXONOMIES) - 1:
            break
        new_blog_post.taxonomies_evergreen_checkbox.click()

    driver.delete_all_cookies()

    # Logging in as lib admin
    login_page = LoginPage(driver, configuration.system.base_url_web).open()
    login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('login_and_setup')
@allure.title("C69743: Structured Tags taxonomy")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69743", "TestRail")
class TestC69743:
    def test_C69743_1(self):
        new_blog_post = CreateNewBlogPostPage(self.driver, configuration.system.base_url_web, post_type='post').open()

        for i in range(len(TAXONOMIES) - 1):
            new_blog_post.taxonomy(TAXONOMIES[i]).click()
            new_blog_post.select_taxonomy(TAXONOMIES[i], taxonomies_terms[i]).click()

        new_blog_post.is_tags_taxonomy_displayed.should.be.true
        new_blog_post.taxonomy("tags").click()
        new_blog_post.select_taxonomy("tags", taxonomies_terms[5]).click()
        new_blog_post.is_tags_taxonomy_term_selected.should.be.true

    def test_C69743_2(self):
        new_news_post = CreateNewNewsPostPage(self.driver, configuration.system.base_url_web, post_type='bccms_news').open()

        for i in range(len(TAXONOMIES) - 1):
            new_news_post.taxonomy(TAXONOMIES[i]).click()
            new_news_post.select_taxonomy(TAXONOMIES[i], taxonomies_terms[i]).click()

        new_news_post.is_tags_taxonomy_displayed.should.be.true
        new_news_post.taxonomy("tags").click()
        new_news_post.select_taxonomy("tags", taxonomies_terms[5]).click()
        new_news_post.is_tags_taxonomy_term_selected.should.be.true

    def test_C69743_3(self):
        new_list_card = CreateNewListCard(self.driver, configuration.system.base_url_web, post_type='bw_list').open()

        for i in range(len(TAXONOMIES) - 1):
            new_list_card.taxonomy(TAXONOMIES[i]).click()
            new_list_card.select_taxonomy(TAXONOMIES[i], taxonomies_terms[i]).click()

        new_list_card.is_tags_taxonomy_displayed.should.be.true
        new_list_card.taxonomy("tags").click()
        new_list_card.select_taxonomy("tags", taxonomies_terms[5]).click()
        new_list_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C69743_4(self):
        new_catalog_comment_card = CreateNewCatalogCommentCard(self.driver, configuration.system.base_url_web, post_type='bw_catalog_comment').open()

        for i in range(len(TAXONOMIES) - 1):
            new_catalog_comment_card.taxonomy(TAXONOMIES[i]).click()
            new_catalog_comment_card.select_taxonomy(TAXONOMIES[i], taxonomies_terms[i]).click()

        new_catalog_comment_card.is_tags_taxonomy_displayed.should.be.true
        new_catalog_comment_card.taxonomy("tags").click()
        new_catalog_comment_card.select_taxonomy("tags", taxonomies_terms[5]).click()
        new_catalog_comment_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C69743_5(self):
        new_event_card = CreateNewEventCard(self.driver, configuration.system.base_url_web, post_type='bw_event').open()

        for i in range(len(TAXONOMIES) - 1):
            new_event_card.taxonomy(TAXONOMIES[i]).click()
            new_event_card.select_taxonomy(TAXONOMIES[i], taxonomies_terms[i]).click()

        new_event_card.is_tags_taxonomy_displayed.should.be.true
        new_event_card.taxonomy("tags").click()
        new_event_card.select_taxonomy("tags", taxonomies_terms[5]).click()
        new_event_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C69743_6(self):
        new_online_resource_card = CreateNewOnlineResourceCard(self.driver, configuration.system.base_url_web, post_type='bw_or_card').open()

        for i in range(len(TAXONOMIES) - 1):
            new_online_resource_card.taxonomy(TAXONOMIES[i]).click()
            new_online_resource_card.select_taxonomy(TAXONOMIES[i], taxonomies_terms[i]).click()

        new_online_resource_card.is_tags_taxonomy_displayed.should.be.true
        new_online_resource_card.taxonomy("tags").click()
        new_online_resource_card.select_taxonomy("tags", taxonomies_terms[5]).click()
        new_online_resource_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C69743_7(self):
        new_poll_card = CreateNewPollCard(self.driver, configuration.system.base_url_web, post_type='bw_poll').open()

        for i in range(len(TAXONOMIES) - 1):
            new_poll_card.taxonomy(TAXONOMIES[i]).click()
            new_poll_card.select_taxonomy(TAXONOMIES[i], taxonomies_terms[i]).click()

        new_poll_card.is_tags_taxonomy_displayed.should.be.true
        new_poll_card.taxonomy("tags").click()
        new_poll_card.select_taxonomy("tags", taxonomies_terms[5]).click()
        new_poll_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C69743_8(self):
        new_twitter_card = CreateNewTwitterCard(self.driver, configuration.system.base_url_web, post_type='bw_twitter').open()

        for i in range(len(TAXONOMIES) - 1):
            new_twitter_card.taxonomy(TAXONOMIES[i]).click()
            new_twitter_card.select_taxonomy(TAXONOMIES[i], taxonomies_terms[i]).click()

        new_twitter_card.is_tags_taxonomy_displayed.should.be.true
        new_twitter_card.taxonomy("tags").click()
        new_twitter_card.select_taxonomy("tags", taxonomies_terms[5]).click()
        new_twitter_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C69743_9(self):
        new_custom_card = CreateNewCustomCard(self.driver, configuration.system.base_url_web, post_type='bw_custom_card').open()

        for i in range(len(TAXONOMIES) - 1):
            new_custom_card.taxonomy(TAXONOMIES[i]).click()
            new_custom_card.select_taxonomy(TAXONOMIES[i], taxonomies_terms[i]).click()

        new_custom_card.is_tags_taxonomy_displayed.should.be.true
        new_custom_card.taxonomy("tags").click()
        new_custom_card.select_taxonomy("tags", taxonomies_terms[5]).click()
        new_custom_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C69743_10(self):
        new_banner = CreateNewBannerPage(self.driver, configuration.system.base_url_web, post_type='bw_banner').open()

        for i in range(len(TAXONOMIES) - 1):
            new_banner.taxonomy(TAXONOMIES[i]).click()
            new_banner.select_taxonomy(TAXONOMIES[i], taxonomies_terms[i]).click()

        new_banner.is_tags_taxonomy_displayed.should.be.true
        new_banner.taxonomy("tags").click()
        new_banner.select_taxonomy("tags", taxonomies_terms[5]).click()
        new_banner.is_tags_taxonomy_term_selected.should.be.true

    def test_C69743_11(self):
        new_hero_slide = CreateNewHeroSlidePage(self.driver, configuration.system.base_url_web, post_type='bw_hero_slide').open()

        for i in range(len(TAXONOMIES)):
            new_hero_slide.is_taxonomy_displayed(TAXONOMIES[i]).should.be.false
