import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text, Internet
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_event_card import CreateNewEventCard
from utils.image_download_helper import *
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.page_builder import PageBuilderPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderSingleCard
from utils.selenium_helpers import click
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


PAGE = "bibliocommons-settings"
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
EVENT_CARD_INFO = {
    'title': '-'.join(Text('en').words(quantity=3)),
    'url': Internet('en').home_page(),
    'location': ' '.join(Text('en').words(quantity=3))
}
IMAGE_TITLE = Text('en').words(quantity=1)[0]
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C74551: Create new card - Event Card (Manual)")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/74551", "TestRail")
class TestC74551:
    def test_C74551(self):
        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as implementing
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()

        self.driver.delete_all_cookies()

        download_image("https://bit.ly/2EdPMqG", IMAGE_PATH)

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        # Creating a new event card
        new_event_card = CreateNewEventCard(self.driver, configuration.system.base_url_web, post_type='bw_event').open()
        new_event_card.manually_enter_event_details.click()
        new_event_card.manual_event_fields.card_image.click()
        new_event_card.manual_event_fields.select_widget_image.upload_image.send_keys(IMAGE_PATH)
        click(new_event_card.manual_event_fields.select_widget_image.add_image_to_widget_button)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=NoSuchElementException)
        wait.until(lambda condition: new_event_card.image_cropper.is_cropper_modal_visible is True)
        wait.until(lambda condition: new_event_card.image_cropper.is_crop_one_visible is True)
        wait.until(lambda condition: new_event_card.image_cropper.is_cropper_box_visible is True)
        new_event_card.image_cropper.image_one_crop()
        new_event_card.image_cropper.crop_image.click()
        new_event_card.image_cropper.next.click()
        wait.until(lambda condition: new_event_card.image_cropper.is_cropper_box_visible is True)
        wait.until(lambda condition: new_event_card.image_cropper.is_crop_one_visible is False)
        wait.until(lambda condition: new_event_card.image_cropper.is_crop_two_visible is True)
        new_event_card.image_cropper.image_two_crop()
        new_event_card.image_cropper.crop_image.click()
        new_event_card.image_cropper.done.click()
        wait.until(lambda condition: new_event_card.image_cropper.is_cropper_modal_visible is False)
        new_event_card.image_cropper.is_crop_one_preview_visible.should.be.true
        new_event_card.image_cropper.is_crop_two_preview_visible.should.be.true

        delete_downloaded_image(IMAGE_PATH)

        new_event_card.manual_event_fields.card_title.send_keys(EVENT_CARD_INFO['title'])
        new_event_card.manual_event_fields.event_url.send_keys(EVENT_CARD_INFO['url'])
        new_event_card.manual_event_fields.location.send_keys(EVENT_CARD_INFO['location'])
        new_event_card.scroll_to_top()
        new_event_card.publish.click()

        # Creating a new page
        create_new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        create_new_page.title.send_keys(PAGE_TITLE)
        create_new_page.page_builder_section.click()
        click(create_new_page.publish)
        create_new_page.wait.until(lambda condition: self.driver.page_source.should.contain("Update"))

        # Opening a new tab
        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # Adding a single card to the page from Page Builder
        new_page = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        new_page.wpheader.page_builder.click()
        if new_page.builder_panel.is_panel_visible is False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.single_card, new_page.page_builder.body).perform()

        # Selecting the event card that was created
        single_card = PageBuilderSingleCard(new_page)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Event Card")
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Event")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(EVENT_CARD_INFO['title']).click()
        single_card.edit_card.save.click()
        single_card.save()
        wait.until(lambda s: len(new_page.user_facing_modules.single_cards) is 1)
        wait.until(lambda s: new_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        len(new_page.user_facing_modules.single_cards).should.equal(1)
        len(new_page.user_facing_modules.single_cards[0].cards).should.equal(1)
        new_page.page_builder.done.click()
        new_page.wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()
        wait.until(lambda condition: new_page.wpheader.is_page_builder_menu_item_displayed)
        wait.until(lambda condition: len(new_page.user_facing_modules.single_cards) is 1)
        new_page.open()
        wait.until(lambda condition: new_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        new_page.user_facing_modules.single_cards[0].cards[0].card_title.get_attribute("textContent").should.equal(EVENT_CARD_INFO['title'])

        self.driver.close()

        # Switching to tab 1
        self.driver.switch_to_window(self.driver.window_handles[0])

        # Deleting the uploaded image
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_input.send_keys(IMAGE_TITLE, Keys.RETURN)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=[IndexError, ElementNotInteractableException])
        wait.until(lambda condition: len(media_library_page.rows) is 1)
        media_library_page.rows[0].hover_on_title()
        wait.until(lambda condition: media_library_page.rows[0].delete.is_displayed())
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        wait.until(lambda condition: len(media_library_page.rows) is 0)

        # Deleting the created page
        all_pages = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages.search_input.send_keys(PAGE_TITLE)
        all_pages.search_button.click()
        all_pages.rows[0].title.get_attribute("textContent").should.match(PAGE_TITLE)
        all_pages.rows[0].hover_on_title()
        all_pages.rows[0].delete.click()
        all_pages.rows.should.be.empty

        # Deleting the created card
        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_input.send_keys(EVENT_CARD_INFO['title'])
        all_cards_page.search_button.click()
        all_cards_page.rows[0].title.get_attribute("textContent").should.match(EVENT_CARD_INFO['title'])
        all_cards_page.rows[0].hover_on_title()
        all_cards_page.rows[0].delete.click()
        all_cards_page.rows.should.be.empty
