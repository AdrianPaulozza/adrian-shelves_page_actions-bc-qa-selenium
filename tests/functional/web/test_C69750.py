import pytest
import allure
import sure
import configuration.system
import configuration.user
from mimesis import Text
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.dashboard import DashboardPage
from pages.web.wpadmin.v3.create_new_content.create_new_online_resource import CreateNewOnlineResourcePage
from pages.web.wpadmin.v3.create_a_new_card.create_new_online_resource_card import CreateNewOnlineResourceCard
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderSingleCard
from pages.web.page_builder import PageBuilderPage
from selenium.webdriver.common.action_chains import ActionChains
from utils.selenium_helpers import click


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C69750: Online Resource Card - New")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69750", "TestRail")
class TestC69750:
    def test_C69750(self):
        login_page = LoginPage(self.driver, configuration.system.urls_web[2]).open()
        login_page.log_in(configuration.user.user['web']['stage']['libadmin']['name'], configuration.user.user['web']['stage']['libadmin']['password'])
        dashboard_page = DashboardPage(self.driver)
        dashboard_page.wait.until(lambda s: dashboard_page.is_page_heading_displayed)
        dashboard_page.content_blocks[5].create_new_button_content_resource.click()

        # Creating new Online Resource
        new_online_resource = CreateNewOnlineResourcePage(self.driver)
        new_online_resource.wait.until(lambda s: new_online_resource.is_resource_heading_displayed)
        resource_title = '-'.join(Text('en').words(quantity=3))  # Converting list to a string
        new_online_resource.resource_title.send_keys(resource_title)
        new_online_resource.driver.switch_to.frame(new_online_resource.visual.iframe)
        resource_description = ' '.join(Text('en').words())
        new_online_resource.visual.body.send_keys(resource_description)
        new_online_resource.driver.switch_to.default_content()
        if new_online_resource.mark_as_featured.is_selected() is False:
            new_online_resource.mark_as_featured.click()
        if new_online_resource.mark_as_new.is_selected() is False:
            new_online_resource.mark_as_new.click()
        if new_online_resource.ios_app_checkbox.is_selected() is False:
            new_online_resource.ios_app_checkbox.click()
        new_online_resource.ios_app_link.send_keys("https://apple.com")
        new_online_resource.resource_link.send_keys("https://google.ca")
        click(new_online_resource.publish)
        new_online_resource.wait.until(lambda s: new_online_resource.is_success_message_displayed)
        new_online_resource.is_success_message_displayed.should.be.true
        new_online_resource.wpsidemenu.menu_dashboard.click()

        dashboard_page = DashboardPage(self.driver)
        dashboard_page.wait.until(lambda s: dashboard_page.is_page_heading_displayed)
        dashboard_page.card_blocks[5].create_new_button_card_resource.click()

        # Creating Online Resource Card
        create_new_resource_card = CreateNewOnlineResourceCard(self.driver)
        create_new_resource_card.select_an_online_resource_dropdown.click()
        create_new_resource_card.select_an_online_resource(resource_title).click()
        create_new_resource_card.select_an_online_resource(resource_title).text.should.equal(resource_title)
        create_new_resource_card.grab_info.click()
        create_new_resource_card.card_image.click()

        # Adding image and selecting different image crops
        create_new_resource_card.select_widget_image.media_library_tab.click()
        create_new_resource_card.select_widget_image.select_image_from_list(0).click()
        click(create_new_resource_card.select_widget_image.add_image_to_widget_button)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2)
        wait.until(lambda condition: create_new_resource_card.image_cropper.is_cropper_modal_visible is True)
        wait.until(lambda condition: create_new_resource_card.image_cropper.is_crop_one_visible is True)
        wait.until(lambda condition: create_new_resource_card.image_cropper.is_cropper_box_visible is True)
        create_new_resource_card.image_cropper.image_one_crop()
        create_new_resource_card.image_cropper.crop_image.click()
        create_new_resource_card.image_cropper.next.click()
        wait.until(lambda condition: create_new_resource_card.image_cropper.is_cropper_box_visible is True)
        wait.until(lambda condition: create_new_resource_card.image_cropper.is_crop_one_visible is False)
        wait.until(lambda condition: create_new_resource_card.image_cropper.is_crop_two_visible is True)
        create_new_resource_card.image_cropper.image_two_crop()
        create_new_resource_card.image_cropper.crop_image.click()
        create_new_resource_card.image_cropper.done.click()
        wait.until(lambda condition: create_new_resource_card.image_cropper.is_cropper_modal_visible is False)
        create_new_resource_card.publish.click()
        create_new_resource_card.wait.until(lambda s: create_new_resource_card.is_success_message_displayed)
        create_new_resource_card.wpsidemenu.menu_all_content.click()
        create_new_resource_card.wpsidemenu.submenu_all_pages.click()

        # Launching PB from existing page
        all_pages = AllPagesPage(self.driver)
        all_pages.wait.until(lambda s: all_pages.page_builder_filter.is_displayed())
        all_pages.page_builder_filter.click()
        all_pages.rows[0].hover_on_title()
        all_pages.rows[0].page_builder.click()

        # Checking card on Page Builder
        page_builder_page = PageBuilderPage(self.driver)
        if page_builder_page.builder_panel.is_panel_visible is False:
            page_builder_page.page_builder.add_content.click()
        page_builder_page.builder_panel.modules_tab.click()

        # Adding a single card to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(page_builder_page.builder_panel.single_card,
                                                page_builder_page.page_builder.body).perform()

        # Selecting the single card that was created
        single_card = PageBuilderSingleCard(page_builder_page)
        single_card.advanced_tab.click()
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Online Resource Card")
        single_card.edit_card_button.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Online Resource")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(resource_title).click()
        single_card.edit_card.select_card(resource_title).text.should.equal(resource_title)
