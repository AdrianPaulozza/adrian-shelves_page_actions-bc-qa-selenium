import pytest
import allure
import sure
import configuration.system
import configuration.user
from mimesis import Text
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.dashboard import DashboardPage
from pages.web.wpadmin.v3.all_contents_page.all_online_resources import AllOnlineResourcesPage
from pages.web.wpadmin.v3.edit_online_resource import EditOnlineResourcePage
from utils.db_connector import DBConnector
from utils.selenium_helpers import click


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C69751: Update V2 Content - Online Resource")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69751", "TestRail")
class TestC69751:
    def test_C69751(self):
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])
        dashboard_page = DashboardPage(self.driver)
        dashboard_page.wait.until(lambda s: dashboard_page.is_page_heading_displayed)
        dashboard_page.content_blocks[5].view_all_link_content_resource.click()

        # Edit an existing Online Resource
        all_online_resources = AllOnlineResourcesPage(self.driver)
        all_online_resources.rows[0].hover_on_title()
        all_online_resources.rows[0].edit.click()

        edit_online_resource = EditOnlineResourcePage(self.driver)
        edit_online_resource.wait.until(lambda s: edit_online_resource.is_resource_heading_displayed)
        edit_online_resource.is_resource_heading_displayed.should.be.true
        edit_online_resource.resource_title.clear()
        update_resource_title = '-'.join(Text('en').words(quantity=3))
        edit_online_resource.resource_title.send_keys(update_resource_title)
        edit_online_resource.driver.switch_to.frame(edit_online_resource.visual.iframe)
        edit_online_resource.visual.body.clear()
        update_resource_description = ' '.join(Text('en').words())
        edit_online_resource.visual.body.send_keys(update_resource_description)
        edit_online_resource.driver.switch_to.default_content()
        if edit_online_resource.online_resource_checkboxes[0].is_selected() is False:
            click(edit_online_resource.online_resource_checkboxes[0])
        edit_online_resource.update.click()
        edit_online_resource.wait.until(lambda s: edit_online_resource.is_success_message_displayed)
        edit_online_resource.is_success_message_displayed.should.be.true
        edit_online_resource.resource_title.get_attribute("value").should.equal(update_resource_title)
        edit_online_resource.driver.switch_to.frame(edit_online_resource.visual.iframe)
        edit_online_resource.visual.body.get_attribute("textContent").should.equal(update_resource_description)
        edit_online_resource.driver.switch_to.default_content()

        # Getting the Resource ID
        all_online_resources = AllOnlineResourcesPage(self.driver, configuration.system.base_url_web, post_type='bccms_online_link').open()
        all_online_resources.search_input.send_keys(update_resource_title)
        all_online_resources.search_button.click()
        all_online_resources.rows[0].title.get_attribute("textContent").should.match(update_resource_title)
        post_href = all_online_resources.rows[0].title.get_attribute("href")
        resource_id = post_href[58:-12]

        # Checking the DB if the resource title and description are updated
        check_db = DBConnector()
        check_db.check_updated_post_title(update_resource_title, resource_id).should.be.true
        check_db.check_updated_post_description(update_resource_description, resource_id).should.be.true
