import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C45994: Advanced Search")
@allure.testcase("", "TestRail")
class TestC45994:
    def test_C45994(self):
            home_page = HomePage(self.driver, configuration.system.base_url).open()
            search_results_page = home_page.header.search_for("Mandela", advanced_search = True)
            # Wait for search results to be greater than 0:
            search_results_page = SearchResultsPage(self.driver)
            search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
            # Assert that between 1 and 10 (max per page) search results were returned:
            len(search_results_page.search_result_items).should.be.within(1, 10)
