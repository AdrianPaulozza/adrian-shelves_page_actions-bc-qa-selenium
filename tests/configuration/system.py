librariesQA = [
    "https://chipublib.qa.bcommons.net",
    "https://jocolibrary.qa.bcommons.net",
    "https://kcls.qa.bcommons.net",
    "https://skokielibrary.qa.bcommons.net",
    "https://hclib.qa.bcommons.net"
]

base_url = librariesQA[2]

urls_web = [
    "https://calgary-stage.bibliocms.com/",
    "https://paloalto-stage.bibliocms.com/",
    "https://chicago-stage.bibliocms.com/",
    "http://chicago.local.bibliocms.com/"
]

base_url_web = urls_web[3]

wordpress_db_details = ["127.0.0.1", "root", "wordpress"]
