from tenacity import *
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.webdriver.support.ui import WebDriverWait


@retry(retry=retry_if_exception_type(ElementClickInterceptedException),
       stop=stop_after_attempt(10),
       wait=wait_fixed(0.5)
       )
def click(element):
    jquery_wait = WebDriverWait(element.parent, 5, poll_frequency=0.5)
    jquery_wait.until(lambda condition: element.parent.execute_script("return jQuery.active==0"))
    WebDriverWait(element.parent, 5, poll_frequency=1).until(lambda condition: element.is_enabled())
    element.click()
    jquery_wait.until(lambda condition: element.parent.execute_script("return jQuery.active==0"))


# Retry 10 times max, waiting 1 second between attempts:
@retry(stop=stop_after_attempt(10),
       wait=wait_fixed(1)
       )
def enter_text(element, value):
    # Click element before sending our desired value:
    element.click()
    # Use send_keys to enter our desired value:
    element.send_keys(value)
    # Check if the element value does not match our given value:
    if element.get_attribute('value') != value:
        # Clear the existing/incomplete element text:
        element.clear()
        # Raise an exception to trigger a retry:
        raise Exception


def highlight(element, clear=False, delay=0.5):
    driver = element.parent
    original_style = element.get_attribute('style')
    driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                          element,
                          "background: yellow; border: 2px solid red;")
    if clear is True:
        import time
        time.sleep(delay)
        driver.execute_script("arguments[0].setAttribute('style', arguments[1]);", element, original_style)
