from selenium.webdriver.common.by import By
from pypom import Region
from pages.web.wpadmin.v3.all_contents_page.all_contents_base import AllContentsBasePage
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException


class AllBlogPostsPage(AllContentsBasePage):

    _rows_locator = (By.CSS_SELECTOR, "tr[id*=post-]")

    @property
    def rows(self):
        return [Row(self, element) for element in self.find_elements(*self._rows_locator)]


class Row(Region):

    _checkbox_locator = (By.CSS_SELECTOR, "th.check-column > input[type='checkbox']")
    _title_locator = (By.CSS_SELECTOR, "a.row-title")
    _post_state_locator = (By.CSS_SELECTOR, "span.post-state")
    _edit_locator = (By.CSS_SELECTOR, "span.edit > a")
    _quick_edit_locator = (By.CSS_SELECTOR, "span > a[class*='editinline']")
    _delete_locator = (By.CSS_SELECTOR, "span.trash > a")
    _view_locator = (By.CSS_SELECTOR, "span.view > a")
    _tags_locator = (By.CSS_SELECTOR, "td[data-colname='Tags'] > a")

    def hover_on_title(self):
        ActionChains(self.driver).move_to_element(self.title).perform()

    @property
    def checkbox(self):
        return self.find_element(*self._checkbox_locator)

    @property
    def title(self):
        return self.find_element(*self._title_locator)

    @property
    def is_post_state_displayed(self):
        try:
            return self.find_element(*self._post_state_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def edit(self):
        return self.find_element(*self._edit_locator)

    @property
    def quick_edit(self):
        return self.find_element(*self._quick_edit_locator)

    @property
    def delete(self):
        return self.find_element(*self._delete_locator)

    @property
    def view(self):
        return self.find_element(*self._view_locator)

    @property
    def tags(self):
        return self.find_elements(*self._tags_locator)
