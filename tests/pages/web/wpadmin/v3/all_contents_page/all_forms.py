from selenium.webdriver.common.by import By
from pages.web.staff_base import StaffBasePage
from pypom import Region
from selenium.webdriver.common.action_chains import ActionChains


class AllFormsPage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/admin.php?page=gf_edit_forms"

    _add_new_locator = (By.CSS_SELECTOR, "a.add-new-h2")
    _all_locator = (By.CSS_SELECTOR, "li.all > a")
    _active_locator = (By.CSS_SELECTOR, "li.active > a")
    _inactive_locator = (By.CSS_SELECTOR, "li.inactive > a")
    _trash_locator = (By.CSS_SELECTOR, "li.trash > a")
    _search_forms_input_locator = (By.CSS_SELECTOR, "#form-search-input")
    _search_forms_button_locator = (By.CSS_SELECTOR, "#search-submit")
    _row_locator = (By.CSS_SELECTOR, "tr.gf-locking")

    @property
    def add_new(self):
        return self.find_element(*self._add_new_locator)

    @property
    def all(self):
        return self.find_element(*self._all_locator)

    @property
    def active(self):
        return self.find_element(*self._active_locator)

    @property
    def inactive(self):
        return self.find_element(*self._inactive_locator)

    @property
    def trash(self):
        return self.find_element(*self._trash_locator)

    @property
    def search_forms_input(self):
        return self.find_element(*self._search_forms_input_locator)

    @property
    def search_forms_button(self):
        return self.find_element(*self._search_forms_button_locator)

    @property
    def rows(self):
        return [Row(self, element) for element in self.find_elements(*self._row_locator)]


class Row(Region):

    _title_locator = (By.CSS_SELECTOR, "td[data-colname='Title'] > strong > a")
    _edit_locator = (By.CSS_SELECTOR, "div > span.gf_form_toolbar_editor > a")
    _settings_locator = (By.CSS_SELECTOR, "div > span.gf_form_toolbar_settings.gf_form_action_has_submenu > a")
    _settings_submenu_locator = (By.CSS_SELECTOR, "div > span.gf_form_toolbar_settings.gf_form_action_has_submenu > div > ul > li > a")
    _entries_locator = (By.CSS_SELECTOR, "div > span.gf_form_toolbar_entries > a")
    _preview_locator = (By.CSS_SELECTOR, "div > span.gf_form_toolbar_preview > a")
    _trash_locator = (By.CSS_SELECTOR, "div > span.trash > a")
    _id_locator = (By.CSS_SELECTOR, "td.id.column-id > a")

    @property
    def title(self):
        return self.find_element(*self._title_locator)

    @property
    def edit(self):
        return self.find_element(*self._edit_locator)

    @property
    def settings(self):
        return self.find_element(*self._settings_locator)

    @property
    def form_settings(self):
        _ = self.find_elements(*self._settings_submenu_locator)
        return _[0]

    @property
    def confirmations(self):
        _ = self.find_elements(*self._settings_submenu_locator)
        return _[1]

    @property
    def notifications(self):
        _ = self.find_elements(*self._settings_submenu_locator)
        return _[2]

    @property
    def personal_data(self):
        _ = self.find_elements(*self._settings_submenu_locator)
        return _[3]

    @property
    def mailchimp(self):
        _ = self.find_elements(*self._settings_submenu_locator)
        return _[4]

    @property
    def entries(self):
        return self.find_element(*self._entries_locator)

    @property
    def preview(self):
        return self.find_element(*self._preview_locator)

    @property
    def trash(self):
        return self.find_element(*self._trash_locator)

    @property
    def id(self):
        return self.find_element(*self._id_locator)

    def hover_on_title(self):
        ActionChains(self.driver).move_to_element(self.title).perform()
