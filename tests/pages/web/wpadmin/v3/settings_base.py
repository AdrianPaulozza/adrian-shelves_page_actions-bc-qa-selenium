from pypom import Region
from selenium.webdriver.common.by import By
from pages.web.staff_base import StaffBasePage


class SettingsBasePage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/options-general.php"

    _save_changes_button_locator = (By.CSS_SELECTOR, "input[id='submit']")

    @property
    def save_changes(self):
        return self.find_element(*self._save_changes_button_locator)

    class TabNavigation(Region):

        _general_locator = (By.CSS_SELECTOR, "a[id='nav-tab-generic")
        _system_settings_locator = (By.CSS_SELECTOR, "a[id='nav-tab-system")
        _homepage_settings_locator = (By.CSS_SELECTOR, "a[id='nav-tab-homepage")
        _consortium_settings_locator = (By.CSS_SELECTOR, "a[id='nav-tab-consortium")
        _search_landing_page_locator = (By.CSS_SELECTOR, "a[id='nav-tab-search_page")

        @property
        def general(self):
            return self.find_element(*self._general_locator)

        @property
        def system_settings(self):
            return self.find_element(*self._system_settings_locator)

        @property
        def homepage_settings(self):
            return self.find_element(*self._homepage_settings_locator)

        @property
        def consortium_settings(self):
            return self.find_element(*self._consortium_settings_locator)

        @property
        def search_landing_page(self):
            return self.find_element(*self._search_landing_page_locator)
