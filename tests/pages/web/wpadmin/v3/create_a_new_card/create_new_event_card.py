from selenium.webdriver.common.by import By
from pypom import Region
from pages.web.wpadmin.v3.create_a_new_card.new_card_base import NewCardBasePage
from pages.web.components.select_widget_image import SelectWidgetImage


class CreateNewEventCard(NewCardBasePage):

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _use_biblioevent_permalink_url_locator = (By.CSS_SELECTOR, "input#fm-bw_card_event-0-bw_event_type-0-1")
    _manually_enter_event_details_locators = (By.CSS_SELECTOR, "input#fm-bw_card_event-0-bw_event_type-0-2")

    @property
    def loaded(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def use_biblioevent_permalink_url(self):
        element = self.find_element(*self._use_biblioevent_permalink_url_locator)
        try:
            if element.is_enabled():
                return element
        except Exception as exception:
            raise exception

    @property
    def biblioevent_permalink_url_fields(self):
        return self.BibloEventPermalinkUrl(self)

    class BibloEventPermalinkUrl(Region):

        _card_image_locator = (By.CSS_SELECTOR, "button#js--open-crop")
        _event_url_locator = (By.CSS_SELECTOR, "input#fm-bw_card_event-0-bw_card_url-0")
        _grab_event_info_locator = (By.CSS_SELECTOR, "#js-grab-event-button")
        _card_title_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-title']")
        _card_description_locator = (By.CSS_SELECTOR, "textarea[data-key='settings-card-description']")
        _remove_locator = (By.CSS_SELECTOR, "a[data-key='settings-crop-remove']")

        @property
        def card_image(self):
            return self.find_element(*self._card_image_locator)

        @property
        def event_url(self):
            return self.find_element(*self._event_url_locator)

        @property
        def grab_event_info(self):
            return self.find_element(*self._grab_event_info_locator)

        @property
        def card_title(self):
            return self.find_element(*self._card_title_locator)

        @property
        def card_description(self):
            return self.find_element(*self._card_description_locator)

        @property
        def remove(self):
            return self.find_element(*self._remove_locator)

        @property
        def select_widget_image(self):
            return SelectWidgetImage(self)

    @property
    def manually_enter_event_details(self):
        element = self.find_element(*self._manually_enter_event_details_locators)
        try:
            if element.is_enabled():
                return element
        except Exception as exception:
            raise exception

    @property
    def manual_event_fields(self):
        return self.ManuallyEnterEventDetails(self)

    class ManuallyEnterEventDetails(Region):

        _card_image_locator = (By.CSS_SELECTOR, "button#js--open-crop")
        _card_title_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-title']")
        _event_url_locator = (By.CSS_SELECTOR, "input#fm-bw_card_event-0-bw_card_url-0")
        _date_time_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-date-time']")
        _card_expiry_date_locator = (By.CSS_SELECTOR, "input.fm-element.c-fm__datepicker-input.js-flatpickr.flatpickr-input.form-control.input")
        _location_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-location']")
        _location_url_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-location-url']")
        _card_description_locator = (By.CSS_SELECTOR, "textarea[data-key='settings-card-description']")
        _remove_locator = (By.CSS_SELECTOR, "a[data-key='settings-crop-remove']")

        @property
        def card_image(self):
            return self.find_element(*self._card_image_locator)

        @property
        def card_title(self):
            return self.find_element(*self._card_title_locator)

        @property
        def event_url(self):
            return self.find_element(*self._event_url_locator)

        @property
        def date_time(self):
            return self.find_element(*self._date_time_locator)

        # functionality of date-picker to be completed in the future
        @property
        def card_expiry_date(self):
            return self.find_element(*self._card_expiry_date_locator)

        @property
        def location(self):
            return self.find_element(*self._location_locator)

        @property
        def location_url(self):
            return self.find_element(*self._location_url_locator)

        @property
        def card_description(self):
            return self.find_element(*self._card_description_locator)

        @property
        def remove(self):
            return self.find_element(*self._remove_locator)

        @property
        def select_widget_image(self):
            return SelectWidgetImage(self)
