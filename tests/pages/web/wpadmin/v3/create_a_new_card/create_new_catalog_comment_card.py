from selenium.webdriver.common.by import By
from pages.web.wpadmin.v3.create_a_new_card.new_card_base import NewCardBasePage
from selenium.common.exceptions import NoSuchElementException


class CreateNewCatalogCommentCard(NewCardBasePage):

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _catalog_comment_url_locator = (By.CSS_SELECTOR, "input#fm-bw_card_catalog-comment-0-bw_card_url-0")
    _grab_comment_info_button_locator = (By.CSS_SELECTOR, "button#js-get-comment-data")
    _success_message_locator = (By.CSS_SELECTOR, "label[class*='bw-validator--success']")
    _error_message_locator = (By.CSS_SELECTOR, "label[class*='bw-validator--error']")
    _validator_message_locator = (By.CSS_SELECTOR, "label[class*='bw-validator']")

    @property
    def loaded(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def catalog_comment_url(self):
        return self.find_element(*self._catalog_comment_url_locator)

    @property
    def grab_comment_info(self):
        return self.find_element(*self._grab_comment_info_button_locator)

    @property
    def is_validator_message_displayed(self):
        try:
            return self.find_element(*self._validator_message_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def success_message(self):
        return self.find_element(*self._success_message_locator)

    @property
    def is_success_message_displayed(self):
        try:
            return self.find_element(*self._success_message_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def error_message(self):
        return self.find_element(*self._error_message_locator)

    @property
    def is_error_message_displayed(self):
        try:
            return self.find_element(*self._error_message_locator).is_displayed()
        except NoSuchElementException:
            return False
