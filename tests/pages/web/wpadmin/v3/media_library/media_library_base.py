from selenium.webdriver.common.by import By
from pages.web.staff_base import StaffBasePage


class MediaLibraryBasePage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/upload.php"

    _heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _add_new_locator = (By.CSS_SELECTOR, "a.page-title-action")
    _view_switch_list_button_locator = (By.ID, "view-switch-list")
    _view_switch_grid_button_locator = (By.ID, "view-switch-grid")
    _search_input_locator = (By.CSS_SELECTOR, "#media-search-input")

    @property
    def loaded(self):
        return self.find_element(*self._heading_locator)

    @property
    def add_new(self):
        return self.find_element(*self._add_new_locator)

    @property
    def view_switch_list_button(self):
        return self.find_element(*self._view_switch_list_button_locator)

    @property
    def view_switch_grid_button(self):
        return self.find_element(*self._view_switch_grid_button_locator)

    @property
    def search_input(self):
        return self.find_element(*self._search_input_locator)
