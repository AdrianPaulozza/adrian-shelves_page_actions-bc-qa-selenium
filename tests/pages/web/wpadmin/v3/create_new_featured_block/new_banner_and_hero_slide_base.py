from selenium.webdriver.common.by import By

from pages.web.staff_base import StaffBasePage

class BannerHeroSlideBasePage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/post-new.php"

    # Publish
    _save_draft_locator = (By.CSS_SELECTOR, "input#save-post")
    _publish_locator = (By.CSS_SELECTOR, "input#publish")

    @property
    def save_draft(self):
        return self.find_element(*self._save_draft_locator)

    @property
    def publish(self):
        return self.find_element(*self._publish_locator)
