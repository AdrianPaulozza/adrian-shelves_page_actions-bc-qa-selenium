from pages.web.components.wpheader import WPHeader
from pages.web.components.wpsidemenu import WPSideMenu
from pages.web.components.user_facing_modules import Modules
from pages.core.base import BasePage
from pages.web.components.system_messages import SystemMessages


class StaffBasePage(BasePage):

    # If WordPress user logged in and navigated to a user-facing page,
    # user will have access to both BC Core header and WordPress.
    @property
    def wpheader(self):
        return WPHeader(self)

    @property
    def wpsidemenu(self):
        return WPSideMenu(self)

    @property
    def user_facing_modules(self):
        return Modules(self)

    @property
    def system_message(self):
        return SystemMessages(self)
