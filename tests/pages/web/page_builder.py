from pages.web.staff_base import StaffBasePage
from pages.web.components.page_builder.page_builder import PageBuilderHeader, BuilderPanel
from pages.web.components.user_facing_modules import Modules

class PageBuilderPage(StaffBasePage):

    @property
    def page_builder(self):
        return PageBuilderHeader(self)

    @property
    def builder_panel(self):
        return BuilderPanel(self)

    @property
    def user_facing_modules(self):
        return Modules(self)
