from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementNotSelectableException
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from pypom import Region


class PageBuilderModuleBase(Region):

    _save_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save.fl-builder-button.fl-builder-button-large")
    _save_as_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save-as.fl-builder-button.fl-builder-button-large")
    _cancel_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-cancel.fl-builder-button.fl-builder-button-large")

    # Content Tab
    _content_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=content]")
    _module_heading_locator = (By.CSS_SELECTOR, "input[name='module_heading[][title]']")
    _link_to_locator = (By.CSS_SELECTOR, "select[name='exit_link_type']")
    _edit_card_locator = (By.CSS_SELECTOR, "a.fl-form-field-edit")
    _add_card_locator = (By.CSS_SELECTOR, "a[data-field='bw_cards']")
    _content_type_locator = (By.CSS_SELECTOR, "#fl-field-bw_tax_content_type > td > div")
    _content_type_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-bw_tax_content_type > td > div > div > div > ul > li")
    _audience_locator = (By.CSS_SELECTOR, "#fl-field-bw_audience_category > td > div")
    _audience_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-bw_audience_category > td > div > div > div > ul > li")
    _related_format_locator = (By.CSS_SELECTOR, "#fl-field-bw_related_format > td > div")
    _related_format_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-bw_related_format > td > div > div > div > ul > li")
    _programs_and_campaigns_locator = (By.CSS_SELECTOR, "#fl-field-bc_program > td > div")
    _programs_and_campaigns_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-bc_program > td > div > div > div > ul > li")
    _genre_locator = (By.CSS_SELECTOR, "#fl-field-bw_tax_genre > td > div")
    _genre_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-bw_tax_genre > td > div > div > div > ul > li")
    _topic_locator = (By.CSS_SELECTOR, "#fl-field-bw_tax_topic > td > div")
    _topic_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-bw_tax_topic > td > div > div > div > ul > li")
    _tags_locator = (By.CSS_SELECTOR, "#fl-field-post_tag > td > div")
    _tags_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-post_tag > td > div > div > div > ul > li")
    _featured_locator = (By.CSS_SELECTOR, "select[name='bw_card_featured']")
    _evergreen_locator = (By.CSS_SELECTOR, "select[name='bw_card_evergreen']")

    # Display Tab
    _display_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=display]")
    _card_description_locator = (By.CSS_SELECTOR, "select[name='show_description']")
    _content_types_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-bw_tax_content_type")
    _audience_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-bw_audience_category")
    _related_format_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-bw_related_format")
    _programs_and_campaigns_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-bc_program")
    _genre_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-bw_tax_genre")
    _topic_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-bw_tax_topic")
    _tags_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-post_tag")

    # Styles Tab
    _styles_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=styles]")
    _size_locator = (By.CSS_SELECTOR, "select[name='module_heading_font_size']")
    _left_alignment_locator = (By.CSS_SELECTOR, "button[data-value='left']")
    _center_alignment_locator = (By.CSS_SELECTOR, "button[data-value='center']")
    _right_alignment_locator = (By.CSS_SELECTOR, "button[data-value='right']")
    _title_size_locator = (By.CSS_SELECTOR, "select[name='title_font_size']")
    _align_text_locator = (By.CSS_SELECTOR, "select[name='align_text']")
    _image_position_locator = (By.CSS_SELECTOR, "select[name='image_position']")
    _image_crop_locator = (By.CSS_SELECTOR, "select[name='image_size']")
    _border_style_locator = (By.CSS_SELECTOR, "select[name='show_border']")
    _poll_card_style_locator = (By.CSS_SELECTOR, "select[name='show_response_options']")

    # Advanced Tab
    _advanced_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=advanced]")
    _top_margin_locator = (By.CSS_SELECTOR, "input[name='margin_top']")
    _right_margin_locator = (By.CSS_SELECTOR, "input[name='margin_right']")
    _bottom_margin_locator = (By.CSS_SELECTOR, "input[name='margin_bottom']")
    _left_margin_locator = (By.CSS_SELECTOR, "input[name='margin_left']")
    _breakpoint_locator = (By.CSS_SELECTOR, "select[name='responsive_display']")
    _display_locator = (By.CSS_SELECTOR, "select[name='visibility_display']")

    @property
    def content_tab(self):
        return self.find_element(*self._content_tab_locator)

    @property
    def module_heading(self):
        return self.find_element(*self._module_heading_locator)

    @property
    def link_to(self):
        return self.find_element(*self._link_to_locator)

    def select_link_to(self, value):
        options = []

        select = Select(self.link_to)
        for option in select.options:
            options.append(option.get_attribute("textContent"))

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def edit_card_button(self):
        edit_card = self.find_elements(*self._edit_card_locator)
        return edit_card[len(edit_card) - 1]

    @property
    def edit_card(self):
        return PageBuilderBEditModuleBase(self)

    @property
    def add_card(self):
        return self.find_element(*self._add_card_locator)

    def taxonomy(self, taxonomy):
        taxonomies = {
            "audience": self._audience_locator,
            "related format": self._related_format_locator,
            "programs and campaigns": self._programs_and_campaigns_locator,
            "genre": self._genre_locator,
            "topic": self._topic_locator,
            "tags": self._tags_locator
        }

        return self.find_element(*(taxonomies.get(taxonomy.casefold())))

    def select_taxonomy(self, taxonomy_dropdown, taxonomy_name):
        taxonomies_dropdowns_results = {
            "audience": self._audience_dropdown_results_locator,
            "related format": self._related_format_dropdown_results_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_results_locator,
            "genre": self._genre_dropdown_results_locator,
            "topic": self._topic_dropdown_results_locator,
            "tags": self._tags_dropdown_results_locator
        }

        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        results = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))

        for index, result in enumerate(results):
            try:
                if result.get_attribute("textContent") == taxonomy_name:
                    self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    # Redefining temporary list to avoid StaleElementException
                    _ = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    return _[index]
            except Exception as exception:
                raise exception
        else:
            raise NoSuchElementException

    @property
    def featured(self):
        return self.find_element(*self._featured_locator)

    @property
    def evergreen(self):
        return self.find_element(*self._evergreen_locator)

    def select_featured_or_evergreen(self, option, value):
        options = ["Include Featured Content", "Limit to only Featured Content", "Exclude Featured Content"]
        if option.casefold() == "featured":
            select = Select(self.featured)
        elif option.casefold() == "evergreen":
            select = Select(self.evergreen)
        else:
            raise NoSuchElementException

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def display_tab(self):
        return self.find_element(*self._display_tab_locator)

    @property
    def card_description(self):
        return self.find_element(*self._card_description_locator)

    def select_card_description(self, value):
        options = []

        select = Select(self.card_description)
        for option in select.options:
            options.append(option.get_attribute("textContent"))

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def content_types(self):
        if self.find_element(*self._content_types_checkbox_locator).is_enabled():
            return self.find_element(*self._content_types_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def audience(self):
        if self.find_element(*self._audience_checkbox_locator).is_enabled():
            return self.find_element(*self._audience_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def related_format(self):
        if self.find_element(*self._related_format_checkbox_locator).is_enabled():
            return self.find_element(*self._related_format_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def programs_and_campaigns(self):
        if self.find_element(*self._programs_and_campaigns_checkbox_locator).is_enabled():
            return self.find_element(*self._programs_and_campaigns_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def genre(self):
        if self.find_element(*self._genre_checkbox_locator).is_enabled():
            return self.find_element(*self._genre_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def topic(self):
        if self.find_element(*self._topic_checkbox_locator).is_enabled():
            return self.find_element(*self._topic_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def tags(self):
        if self.find_element(*self._tags_checkbox_locator).is_enabled():
            return self.find_element(*self._tags_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def styles_tab(self):
        return self.find_element(*self._styles_tab_locator)

    @property
    def size(self):
        return self.find_element(*self._size_locator)

    def select_size(self, value):
        options = []

        select = Select(self.size)
        for option in select.options:
            options.append(option.get_attribute("textContent"))

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def left_alignment(self):
        return self.find_element(*self._left_alignment_locator)

    @property
    def right_alignment(self):
        return self.find_element(*self._right_alignment_locator)

    @property
    def center_alignment(self):
        return self.find_element(*self._center_alignment_locator)

    @property
    def title_size(self):
        return self.find_element(*self._title_size_locator)

    def select_title_size(self, value):
        options = []

        select = Select(self.title_size)
        for option in select.options:
            options.append(option.get_attribute("textContent"))

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def align_text(self):
        return self.find_element(*self._align_text_locator)

    def select_align_text(self, value):
        options = []

        select = Select(self.align_text)
        for option in select.options:
            options.append(option.get_attribute("textContent"))

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def image_position(self):
        return self.find_element(*self._image_position_locator)

    def select_image_position(self, value):
        options = []

        select = Select(self.image_position)
        for option in select.options:
            options.append(option.get_attribute("textContent"))

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def image_crop(self):
        return self.find_element(*self._image_crop_locator)

    def select_image_crop(self, value):
        options = []

        select = Select(self.image_crop)
        for option in select.options:
            options.append(option.get_attribute("textContent"))

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def border_style(self):
        return self.find_element(*self._border_style_locator)

    def select_border_style(self, value):
        options = []

        select = Select(self.border_style)
        for option in select.options:
            options.append(option.get_attribute("textContent"))

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def poll_card_style(self):
        return self.find_element(*self._poll_card_style_locator)

    def select_poll_card_style(self, value):
        options = []

        select = Select(self.poll_card_style)
        for option in select.options:
            options.append(option.get_attribute("textContent"))

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def advanced_tab(self):
        return self.find_element(*self._advanced_tab_locator)

    @property
    def top_margin(self):
        return self.find_element(*self._top_margin_locator)

    @property
    def right_margin(self):
        return self.find_element(*self._right_margin_locator)

    @property
    def bottom_margin(self):
        return self.find_element(*self._bottom_margin_locator)

    @property
    def left_margin(self):
        return self.find_element(*self._left_margin_locator)

    @property
    def breakpoint(self):
        return self.find_element(*self._breakpoint_locator)

    def select_breakpoint(self, value):
        options = []

        select = Select(self.breakpoint)
        for option in select.options:
            options.append(option.get_attribute("textContent"))

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def display(self):
        return self.find_element(*self._display_locator)

    def select_display(self, value):
        options = []

        select = Select(self.display)
        for option in select.options:
            options.append(option.get_attribute("textContent"))

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    def save(self):
        save = self.find_elements(*self._save_locator)
        self.driver.execute_script("arguments[0].click();", save[0])

    @property
    def save_as(self):
        return self.find_element(*self._save_as_locator)

    @property
    def cancel(self):
        cancel = self.find_elements(*self._cancel_locator)
        return cancel[0]


class PageBuilderBEditModuleBase(Region):

    _heading_locator = (By.CSS_SELECTOR, "form[data-type='bw_card_form_field']")
    _save_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save.fl-builder-button.fl-builder-button-large")
    _cancel_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-cancel.fl-builder-button.fl-builder-button-large")

    # General Tab
    _general_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=general]")
    _choose_card_type_locator = (By.CSS_SELECTOR, "select[name='post_type']")
    _choose_card_locator = (By.CSS_SELECTOR, "#fl-field-post_id > td > div > div")
    _choose_card_dropdown_locator = (By.CSS_SELECTOR, "#fl-field-post_id > td > div > div > div > ul > li")
    _active_label_locator = (By.CSS_SELECTOR, "div.fl-form-field-after.bw-scheduling-info.bw-scheduling-info--active")
    _expired_label_locator = (By.CSS_SELECTOR, "div.fl-form-field-after.bw-scheduling-info.bw-scheduling-info--expired")
    _scheduled_for_label_locator = (By.CSS_SELECTOR, "div.fl-form-field-after.bw-scheduling-info.bw-scheduling-info--scheduled")

    # Schedule
    _starting_locator = (By.CSS_SELECTOR, "tr#fl-field-schedule_start > td > div")
    _ending_locator = (By.CSS_SELECTOR, "tr#fl-field-schedule_end > td > div")

    # Styles Tab
    _styles_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=general]")
    _solid_color_style_locator = (By.CSS_SELECTOR, "select[name='solid_color_style']")

    @property
    def general_tab(self):
        return self.find_element(*self._general_tab_locator)

    @property
    def card_type(self):
        card_type = self.find_elements(*self._choose_card_type_locator)
        return card_type[len(card_type)-1]

    def select_card_type(self, value):
        options = []

        select = Select(self.card_type)
        for option in select.options:
            options.append(option.get_attribute("textContent"))

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def choose_card(self):
        self.wait.until(EC.presence_of_element_located(self._choose_card_locator))
        return self.find_element(*self._choose_card_locator)

    def select_card(self, card):
        self.wait.until(EC.visibility_of_all_elements_located(self._choose_card_dropdown_locator))
        results = self.find_elements(*self._choose_card_dropdown_locator)

        _ = None

        for index, result in enumerate(results):
            try:
                if result.get_attribute("textContent").casefold() == card.casefold():
                    self.wait.until(EC.visibility_of_all_elements_located(self._choose_card_dropdown_locator))
                    # Redefining temporary list to avoid StaleElementException
                    _ = self.find_elements(*self._choose_card_dropdown_locator)
                    return _[index]
            except Exception as exception:
                raise exception

        if _ is None:
            raise NoSuchElementException

    @property
    def save(self):
        save = self.find_elements(*self._save_locator)
        return save[len(save)-1]

    @property
    def cancel(self):
        cancel = self.find_elements(*self._cancel_locator)
        return cancel[len(cancel)-1]

    @property
    def starting(self):
        return self.find_element(*self._starting_locator)

    @property
    def ending(self):
        return self.find_element(*self._ending_locator)

    @property
    def active_label(self):
        return self.find_elements(*self._active_label_locator)

    @property
    def expired_label(self):
        return self.find_elements(*self._expired_label_locator)

    @property
    def scheduled_for_label(self):
        return self.find_elements(*self._scheduled_for_label_locator)

    @property
    def schedule_picker(self):
        return SchedulePicker(self)


class SchedulePicker(Region):

    _previous_next_month_locator = (By.CSS_SELECTOR, "div.flatpickr-calendar.hasTime.animate.showTimeInput.arrowBottom.open > div.flatpickr-months > span > svg")
    _days_locator = (By.CSS_SELECTOR, "div.flatpickr-calendar.hasTime.animate.showTimeInput.arrowBottom.open > div.flatpickr-innerContainer > div > div.flatpickr-days > div > span")
    _today_locator = (By.CSS_SELECTOR, "div.flatpickr-calendar.hasTime.animate.showTimeInput.arrowBottom.open > div.flatpickr-innerContainer > div > div.flatpickr-days > div > span.flatpickr-day.today")
    _hour_minute_locator = (By.CSS_SELECTOR, "body > div.flatpickr-calendar.hasTime.animate.showTimeInput.arrowBottom.open > div.flatpickr-time > div > input")
    _am_pm_locator = (By.CSS_SELECTOR, "body > div.flatpickr-calendar.hasTime.animate.showTimeInput.open.arrowBottom > div.flatpickr-time > span.flatpickr-am-pm")


    @property
    def previous_month(self):
        month = self.find_elements(*self._previous_next_month_locator)
        return month[0]

    @property
    def next_month(self):
        month = self.find_elements(*self._previous_next_month_locator)
        return month[1]

    def days(self, index):
        days = self.find_elements(*self._days_locator)
        return days[index]

    @property
    def today(self):
        return self.find_element(*self._today_locator)

    @property
    def hour(self):
        hour = self.find_elements(*self._hour_minute_locator)
        return hour[0]

    @property
    def minute(self):
        minute = self.find_elements(*self._hour_minute_locator)
        return minute[1]

    @property
    def am_pm(self):
        return self.find_element(*self._am_pm_locator)
