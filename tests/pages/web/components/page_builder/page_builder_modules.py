from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.web.components.page_builder.page_builder_module_base import PageBuilderModuleBase


class PageBuilderSingleCard(PageBuilderModuleBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='SingleCard']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False
