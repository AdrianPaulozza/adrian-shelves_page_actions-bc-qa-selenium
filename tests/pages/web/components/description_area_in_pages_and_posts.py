from pypom import Region
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select


class Text(Region):

    _text_body_locator = (By.CSS_SELECTOR, "textarea#content")

    @property
    def text_body(self):
        return self.find_element(*self._text_body_locator)


class Visual(Region):

    _body_locator = (By.CSS_SELECTOR, "html > body#tinymce")
    _iframe_locator = (By.CSS_SELECTOR, "iframe#content_ifr")
    _related_faq_button_locator = (By.CSS_SELECTOR, "div[aria-label='Related FAQ'] > button")
    _add_library_location_button_locator = (By.CSS_SELECTOR, "div[aria-label='Add Library Location'] > button")
    _related_faq_form_locator = (By.CSS_SELECTOR, "form#related_faq_widget_fields")
    _add_library_locations_form_locator = (By.CSS_SELECTOR, "form#add-a-library-location")

    @property
    def body(self):
        self.wait.until(EC.visibility_of_element_located(self._body_locator))
        return self.find_element(*self._body_locator)

    @property
    def iframe(self):
        return self.find_element(*self._iframe_locator)

    def related_faq_button(self):
        self.wait.until(EC.element_to_be_clickable(self._related_faq_button_locator))
        element = self.find_element(*self._related_faq_button_locator)
        self.driver.execute_script("arguments[0].click()", element)

    @property
    def related_faq(self):
        return RelatedFaq(self)

    def add_library(self):
        self.wait.until(EC.element_to_be_clickable(self._add_library_location_button_locator))
        element = self.find_element(*self._add_library_location_button_locator)
        self.driver.execute_script("arguments[0].click()", element)

    @property
    def add_library_locations(self):
        return AddLibraryLocations(self)

    @property
    def is_add_library_locations_form_visible(self):
        self.wait.until(EC.invisibility_of_element_located(self._add_library_locations_form_locator))
        return False

    @property
    def is_related_faq_form_visible(self):
        self.wait.until(EC.invisibility_of_element_located(self._related_faq_form_locator))
        return False


class RelatedFaq(Region):

    _title_locator = (By.CSS_SELECTOR, "input#fw_title")
    _limit_locator = (By.CSS_SELECTOR, "input#fw_limit")
    _category_locator = (By.CSS_SELECTOR, "select#fw_category")
    _category_dropdown_options_locator = (By.CSS_SELECTOR, "select#fw_category > option")
    _insert_shortcode_locator = (By.CSS_SELECTOR, "input#fw_submit")

    @property
    def title(self):
        return self.find_element(*self._title_locator)

    @property
    def limit(self):
        return self.find_element(*self._limit_locator)

    @property
    def category(self):
        self.wait.until(EC.visibility_of_element_located(self._category_locator))
        return self.find_element(*self._category_locator)

    def select_category(self, category):
        select = Select(self.category)
        categories = self.find_elements(*self._category_dropdown_options_locator)

        for i in range(len(categories)):
            if categories[i].get_attribute("textContent").casefold() == category.casefold():
                select.select_by_visible_text(category)
                break
        else:
            raise NoSuchElementException("Option not found.")

    @property
    def insert_shortcode(self):
        return self.find_element(*self._insert_shortcode_locator)


class AddLibraryLocations(Region):

    _library_locations_locator = (By.CSS_SELECTOR, "div.add-a-library-location__panel > ul > li > label")
    _insert_into_post_locator = (By.CSS_SELECTOR, "input#add-a-library-location-insert")

    def choose_library_locations(self, libraryLocation):
        self.wait.until(EC.visibility_of_all_elements_located(self._library_locations_locator))
        locations = self.find_elements(*self._library_locations_locator)

        for location in range(len(locations)):
            try:
                if locations[location].get_attribute("textContent").casefold().strip() == libraryLocation.casefold():
                    self.wait.until(EC.visibility_of_all_elements_located(self._library_locations_locator))
                    locations = self.find_elements(*self._library_locations_locator)
                    return locations[location]
            except Exception as exception:
                raise exception
        else:
            raise NoSuchElementException

    @property
    def insert_into_post(self):
        return self.find_element(*self._insert_into_post_locator)
