from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pypom import Region


class Modules(Region):

    _single_card_locator = (By.CSS_SELECTOR, "[data-key='module-single-card']")
    _tiled_card_locator = (By.CSS_SELECTOR, "[data-key='module-tiled-card']")
    _card_collection_locator = (By.CSS_SELECTOR, "[data-key='module-card-collection']")
    _structured_card_collection_locator = (By.CSS_SELECTOR, "[data-key='module-structured-card-collection']")
    _masonry_card_collection_locator = (By.CSS_SELECTOR, "[data-key='module-masonry']")

    @property
    def single_cards(self):
        return [self.Module(self, element) for element in self.find_elements(*self._single_card_locator)]

    @property
    def tiled_cards(self):
        return [self.Module(self, element) for element in self.find_elements(*self._tiled_card_locator)]

    @property
    def card_collections(self):
        return [self.Module(self, element) for element in self.find_elements(*self._card_collection_locator)]

    @property
    def structured_card_collections(self):
        return [self.Module(self, element) for element in self.find_elements(*self._structured_card_collection_locator)]

    @property
    def masonry_card_collections(self):
        return [self.Module(self, element) for element in self.find_elements(*self._masonry_card_collection_locator)]

    class Module(Region):

        _module_heading_locator = (By.CSS_SELECTOR, "h2[class*='c-module-heading']")
        _card_locator = (By.CSS_SELECTOR, "[data-key='card']")

        @property
        def module_heading(self):
            return self.find_element(*self._module_heading_locator)

        @property
        def is_module_heading_displayed(self):
            try:
                return self.find_element(*self._module_heading_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def cards(self):
            return [self.Card(self, element) for element in self.find_elements(*self._card_locator)]

        class Card(Region):

            _card_image_locator = (By.CSS_SELECTOR , "[data-key='card-image'] > img")
            _card_content_type_locator = (By.CSS_SELECTOR, "[data-key='card-content-type']")
            _card_title_locator = (By.CSS_SELECTOR, "[data-key='card-title']")
            _card_author_locator = (By.CSS_SELECTOR, "[data-key='card-author']")
            _card_description_locator = (By.CSS_SELECTOR, "[data-key='card-description']")
            _card_tag_locator = (By.CSS_SELECTOR, "[data-key='card-tag']")
            _card_bib_author_locator = (By.CSS_SELECTOR, "[data-key='card-bib-author']")
            _card_comment_author_locator = (By.CSS_SELECTOR, "[data-key='card-comment-author']")
            _card_comment_locator = (By.CSS_SELECTOR, "[data-key='card-comment']")
            _card_read_more_locator = (By.CSS_SELECTOR, "[data-key='card-read-more']")

            @property
            def card_image(self):
                return self.find_element(*self._card_image_locator)

            @property
            def is_card_image_displayed(self):
                try:
                    return self.find_element(*self._card_image_locator).is_displayed()
                except NoSuchElementException:
                    return False

            @property
            def card_content_type(self):
                return self.find_element(*self._card_content_type_locator)

            @property
            def is_card_content_type_displayed(self):
                try:
                    return self.find_element(*self._card_content_type_locator).is_displayed()
                except NoSuchElementException:
                    return False

            @property
            def card_title(self):
                return self.find_element(*self._card_title_locator)

            @property
            def is_card_title_displayed(self):
                try:
                    return self.find_element(*self._card_title_locator).is_displayed()
                except NoSuchElementException:
                    return False

            @property
            def card_author(self):
                return self.find_element(*self._card_author_locator)

            @property
            def is_card_author_displayed(self):
                try:
                    return self.find_element(*self._card_author_locator).is_displayed()
                except NoSuchElementException:
                    return False

            @property
            def card_description(self):
                return self.find_element(*self._card_description_locator)

            @property
            def is_card_description_displayed(self):
                try:
                    return self.find_element(*self._card_description_locator)
                except NoSuchElementException:
                    return False

            @property
            def card_bib_author(self):
                return self.find_element(*self._card_bib_author_locator)

            @property
            def is_card_bib_author_displayed(self):
                try:
                    return self.find_element(*self._card_bib_author_locator)
                except NoSuchElementException:
                    return False

            @property
            def card_comment_author(self):
                return self.find_element(*self._card_comment_author_locator)

            @property
            def is_card_comment_author_displayed(self):
                try:
                    return self.find_element(*self._card_comment_author_locator)
                except NoSuchElementException:
                    return False

            @property
            def card_comment(self):
                return self.find_element(*self._card_comment_locator)

            @property
            def is_card_comment_displayed(self):
                try:
                    return self.find_element(*self._card_comment_locator)
                except NoSuchElementException:
                    return False

            @property
            def card_read_more(self):
                return self.find_element(*self._card_read_more_locator)

            @property
            def is_card_read_more_displayed(self):
                try:
                    return self.find_element(*self._card_read_more_locator)
                except NoSuchElementException:
                    return False

            @property
            def card_tags(self):
                return self.find_elements(*self._card_tag_locator)
