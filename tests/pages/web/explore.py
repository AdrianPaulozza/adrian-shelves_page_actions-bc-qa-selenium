from pages.web.staff_base import StaffBasePage
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


class ExplorePagePage(StaffBasePage):
    _explore_heading_locator = (By.CSS_SELECTOR, ".o-explore__title")

    @property
    def explore_heading(self):
        return self.find_element(*self._explore_heading_locator)

    @property
    def is_explore_heading_displayed(self):
        try:
            return self.find_element(*self._explore_heading_locator).is_displayed()
        except NoSuchElementException:
            return False

