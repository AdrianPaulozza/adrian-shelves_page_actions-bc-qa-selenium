from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.web.staff_base import StaffBasePage


class UserPage(StaffBasePage):
    _error_page_title_locator = (By.CSS_SELECTOR, "h2.entry-title")

    @property
    def is_error_title_displayed(self):
        try:
            return self.find_element(*self._error_page_title_locator).is_displayed()
        except NoSuchElementException:
            return False
