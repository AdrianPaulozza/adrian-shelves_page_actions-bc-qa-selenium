from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from .base import BasePage
from pages.core.components.overlay import Overlay

# https://<library>.<environment>.bibliocommons.com/user_lists/[ListID?]/edit
class UserListsPage(BasePage):
    _list_type_dropdown_locator = (By.CSS_SELECTOR, "[data-test-id='list-type-dropdown']")
    _list_type_personal_recommendation_locator = (By.CSS_SELECTOR, "[data-test-id='dropdown-item-personal_recommendation']")
    _edit_list_name_locator = (By.CSS_SELECTOR, "[data-test-id='edit-list-name-button']")
    _title_locator = (By.ID, "list-name")
    _edit_list_description_locator = (By.CSS_SELECTOR, "[data-test-id='edit-list-description-button']")
    _description_locator = (By.CLASS_NAME, "list-description-input")
    _add_to_list_locator = (By.CSS_SELECTOR, "[data-test-id='add-to-list-button']")
    _add_item_to_list_web_url_tab_locator = (By.CSS_SELECTOR, "[data-test-id='add-web-item-tab']")
    _add_item_to_list_web_url_locator = (By.CSS_SELECTOR, "[data-test-id='web-item-url']")
    _add_item_to_list_web_url_okay_locator = (By.CSS_SELECTOR, "[data-test-id='web-item-submit-url-button']")
    _add_item_to_list_web_item_title_locator = (By.CSS_SELECTOR, "[data-test-id='web-item-title-input']")
    _add_item_to_list_add_web_item_locator = (By.CSS_SELECTOR, "[data-test-id='add-web-item-button']")
    _finished_editing_locator = (By.CSS_SELECTOR, "[data-test-id='publish-button']")
    _overlay_publish_locator = (By.CSS_SELECTOR, "[data-test-id='publish-list-submit-button']")
    # Does this belong on an individual "List" page instead?
    _list_published_alert_locator = (By.CLASS_NAME, "cp-alert-success")
    _add_item_to_list_catalogue_item_search_locator = (By.CSS_SELECTOR, "[data-test-id='search-bar-query']")
    _add_item_to_list_catalogue_item_search_result_item_locator = (By.CLASS_NAME, "cp-user-list-search-result-item")
    _add_item_to_list_catalogue_item_search_result_add_locator = (By.CSS_SELECTOR, "[data-test-id='add-button']")

    @property
    def overlay(self):
        return Overlay(self)

    @property
    def list_type_dropdown(self):
        return self.find_element(*self._list_type_dropdown_locator)

    @property
    def list_type_personal_recommendation(self):
        return self.find_element(*self._list_type_personal_recommendation_locator)

    # Name this 'menu' since it makes more sense but look for the last type:
    @property
    def is_list_type_menu_displayed(self):
        try:
            return self.find_element(*self._list_type_personal_recommendation_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def edit_list_name(self):
        return self.find_element(*self._edit_list_name_locator)

    @property
    def title(self):
        return self.find_element(*self._title_locator)

    @property
    def edit_list_description(self):
        return self.find_element(*self._edit_list_description_locator)

    @property
    def description(self):
        return self.find_element(*self._description_locator)

    @property
    def add_to_list(self):
        # WebDriverWait(self.driver, 3).until(EC.alert_is_present())
        wait = WebDriverWait(self.driver, 10)
        # element = wait.until(EC.element_to_be_clickable((By.ID,'someid')))
        element = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "[data-test-id='add-to-list-button']")))
        return self.find_element(*self._add_to_list_locator)

    @property
    def add_item_to_list_web_url_tab(self):
        return self.find_element(*self._add_item_to_list_web_url_tab_locator)

    @property
    def add_item_to_list_web_url(self):
        return self.find_element(*self._add_item_to_list_web_url_locator)

    @property
    def add_item_to_list_web_url_okay(self):
        return self.find_element(*self._add_item_to_list_web_url_okay_locator)

    @property
    def add_item_to_list_web_item_title(self):
        return self.find_element(*self._add_item_to_list_web_item_title_locator)

    @property
    def is_add_item_to_list_web_item_title_displayed(self):
        try:
            return self.find_element(*self._add_item_to_list_web_item_title_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def add_item_to_list_add_web_item(self):
        return self.find_element(*self._add_item_to_list_add_web_item_locator)

    @property
    def finished_editing(self):
        return self.find_element(*self._finished_editing_locator)

    @property
    def overlay_publish(self):
        return self.find_element(*self._overlay_publish_locator)

    @property
    def is_overlay_publish_displayed(self):
        try:
            return self.find_element(*self._overlay_publish_locator).is_displayed()
        except NoSuchElementException:
            return False

    # @property
    # def is_overlay_publish_clickable(self):
    #     try:
    #         return EC.element_to_be_clickable((By.CSS_SELECTOR, "[data-test-id='publish-list-submit-button']"))
    #     except:
    #         return False

    @property
    def list_published_alert(self):
        return self.find_element(*self._list_published_alert_locator)

    @property
    def is_list_published_alert_displayed(self):
        try:
            return self.find_element(*self._list_published_alert_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def add_item_to_list_catalogue_item_search(self):
        return self.find_element(*self._add_item_to_list_catalogue_item_search_locator)

    @property
    def add_item_to_list_catalogue_item_search_result_items(self):
        return self.find_elements(*self._add_item_to_list_catalogue_item_search_result_item_locator)

    @property
    def add_item_to_list_catalogue_item_search_result_add(self):
        return self.find_elements(*self._add_item_to_list_catalogue_item_search_result_add_locator)
