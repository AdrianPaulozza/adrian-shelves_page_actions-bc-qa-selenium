from selenium.webdriver.common.by import By
from pages.core.base import BasePage
from pages.core.components.circulation import Circulation
from pages.core.components.community_activity import CommunityActivity
from pages.core.components.ugc_metadata import UGCMetadata
from pages.core.components.overlay import Overlay
from selenium.common.exceptions import NoSuchElementException

# https://<library>.<environment>.bibliocommons.com/item/show/[ItemID]
class BibPage(BasePage):

    URL_TEMPLATE = "/item/show/{item_id}"   # Usage: BibPage(self.driver, base_url, item_id = [ItemID]).open()
    _top_message_container_locator = (By.CSS_SELECTOR, "[data-test-id='top-message-container']")
    _user_id_locator = (By.CSS_SELECTOR, "[testid*='user_card_']")

    @property
    def circulation(self):
        return Circulation(self)

    @property
    def community_activity(self):
        return CommunityActivity(self)

    @property
    def ugc_metadata(self):
        return UGCMetadata(self)

    @property
    def overlay(self):
        return Overlay(self)

    @property
    def is_top_message_displayed(self):
        try:
            return self.find_element(*self._top_message_container_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def user_id(self):
        return self.find_element(*self._user_id_locator)
