from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By

from pages.core.base import BasePage
from pages.core.components.checked_out_item import CheckedOutItem
from pages.core.components.my_borrowing import MyBorrowing



class CheckedOutPage(BasePage):
    URL_TEMPLATE = "/v2/checkedout/out"

    _heading_locator = (By.CSS_SELECTOR, ".cp-core-external-header")
    _item_locator = (By.CSS_SELECTOR, ".cp-bib-list-item.cp-checked-out-item")
    _list_locator = (By.CSS_SELECTOR, "div.cp-checked-out-list")
    _list_empty_locator = (By.CSS_SELECTOR, "div.cp-empty-content-panel")
    _notification_success_locator = (By.CSS_SELECTOR, ".alert.cp-alert.alert-success.cp-alert-success")
    _quick_renew_locator = (By.CSS_SELECTOR, ".quick-renewal-dropdown .selected-wrapper")
    _over_due_title_locator = (By.CSS_SELECTOR, ".cp-borrowing-utility-bar-title.overdue")
    _due_later_locator = (By.CSS_SELECTOR, ".cp-borrowing-utility-bar-title.out")
    _quick_renewal_list_locator = (By.CSS_SELECTOR, "[data-key='quick-renewal-dropdown'] li")
    _quick_renew_dropdown_locator = (By.CLASS_NAME, "cp-quick-renewal")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_borrowing(self):
        return MyBorrowing(self)

    @property
    def items(self):
        return [CheckedOutItem(self, element) for element in self.find_elements(*self._item_locator)]

    @property
    def checked_out_list(self):
        return self.find_element(*self._list_locator)

    @property
    def is_list_displayed(self):
        try:
            return self.find_element(*self._list_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def list_empty(self):
        return self.find_element(*self._checked_out_list_empty_locator)

    @property
    def is_list_empty_displayed(self):
        try:
            return self.find_element(*self._checked_out_list_empty_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_checked_out_success_notification_displayed(self):
        try:
            return self.find_element(*self._notification_success_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def due_later_title(self):
        return self.find_element(*self._due_later_locator)

    @property
    def quick_renew(self):
        return self.find_element(*self._quick_renew_locator)

    @property
    def is_over_due_title_displayed(self):
        try:
            return self.find_element(*self._over_due_title_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def quick_renew_dropdown(self):
        return self.find_element(*self._quick_renew_dropdown_locator)

    def quick_renew_to(self, status):
        self.quick_renew.click()
        WebDriverWait(self.driver, 5).until(
            lambda s: self.find_element(*self._quick_renewal_list_locator).is_displayed())
        options = self.quick_renew_dropdown.find_elements(*self._quick_renewal_list_locator)
        for option in options:
            if status in option.text:
                option.click()
                break