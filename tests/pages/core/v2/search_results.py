from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.components.overlay import Overlay
from pages.core.base import BasePage
from pages.core.components.search_result_item import SearchResultItem
import re

# NERF (V2) Page:
# https://<library>.<environment>.bibliocommons.com/v2/search?[&kwargs]


class SearchResultsPage(BasePage):
    # Usage: SearchResultsPage(self.driver, base_url, query=[query], search_type=[search_type]).open()
    URL_TEMPLATE = "/v2/search?query={query}&searchType={search_type}"

    _search_result_item_locator = (By.CSS_SELECTOR, "[data-test-id='searchResultItem']")
    _search_active_filter_locator = (By.CSS_SELECTOR, "div.filter-controls button.cp-toggle-switch span.label")
    _search_active_filter_enabled_locator = (By.CSS_SELECTOR, "button.cp-toggle-switch.cp-active-filter-lock-switch.active")
    _search_active_filter_disabled_locator = (By.CSS_SELECTOR, "button.cp-toggle-switch.cp-active-filter-lock-switch.inactive")
    _shelves_availability_header_locator = (By.CLASS_NAME, "shelf-title")
    _shelves_availability_back_link_locator = (By.CLASS_NAME, "cp-shelf-link")
    _empty_results_page_locator = (By.CLASS_NAME, "cp-empty-search-result")
    _no_titles_text_locator = (By.CLASS_NAME, "empty-search-sub-title")

    # Facets names were taken from chipublib, due to the way format mapping is implemented
    # in the search results other libraries may use different facets names:
    _search_filter_switch_locator = {
        "Book": "[data-test-id='FORMAT-BOOKS-BK-filter']",
        "Paperback": "[data-test-id='FORMAT-BOOKS-PAPERBACK-filter']",
        "Ebook": "[data-test-id='FORMAT-BOOKS-EBOOK-filter']",
        "Large Print": "[data-test-id='FORMAT-BOOKS-LPRINT-filter']",
        "Board Book": "[data-test-id='FORMAT-BOOKS-BOARD_BK-filter']",
        "Book Club Kit": "[data-test-id='FORMAT-BOOKS-BOOK_CLUB_KIT-filter']",
        "Audiobook CD": "[data-test-id='FORMAT-AUDIOBOOKS_SPOKEN_WORD-BOOK_CD-filter']",
        "Downloadable Audiobook": "[data-test-id='FORMAT-AUDIOBOOKS_SPOKEN_WORD-AB-filter']",
        "Audiobook Cassette": "[data-test-id='FORMAT-AUDIOBOOKS_SPOKEN_WORD-CS-filter']",
        "Preloaded Audiobook": "[data-test-id='FORMAT-AUDIOBOOKS_SPOKEN_WORD-PLAYAWAY_AUDIOBOOK-filter']",
        "DVD": "[data-test-id='FORMAT-MOVIES_TV_VIDEO-DVD-filter']",
        "Video Cassette": "[data-test-id='FORMAT-MOVIES_TV_VIDEO-VC-filter']",
        "Streaming Video": "[data-test-id='FORMAT-MOVIES_TV_VIDEO-VIDEO_ONLINE-filter']",
        "Printed Music": "[data-test-id='FORMAT-MUSIC_SOUND-MN-filter']",
        "Music CD": "[data-test-id='FORMAT-MUSIC_SOUND-MUSIC_CD-filter']",
        "LP": "[data-test-id='FORMAT-MUSIC_SOUND-LP-filter']",
        "Streaming Music": "[data-test-id='FORMAT-MUSIC_SOUND-MUSIC_ONLINE-filter']",
        "CD or DVD": "[data-test-id='FORMAT-GAMES_INTERACTIVE_MEDIA-CDROM-filter']",
        "Magazine or Journal": "[data-test-id='FORMAT-JOURNALS_MAGAZINES-MAG-filter']",
        "Online Periodical or Article": "[data-test-id='FORMAT-JOURNALS_MAGAZINES-EJ-filter']",
        "Large Print Accessible": "[data-test-id='FORMAT-ACCESSIBLE_FORMATS-LPRINT-filter']",
        "Braille": "[data-test-id='FORMAT-ACCESSIBLE_FORMATS-BR-filter']",
        "Website or Online": "[data-test-id='FORMAT-OTHER-WEBSITE-filter']",
        "Microform": "[data-test-id='FORMAT-OTHER-MF-filter']",
        "Unknown": "[data-test-id='FORMAT-OTHER-UK-filter']",
        "Map": "[data-test-id='FORMAT-OTHER-MAP-filter']",
        "Kit": "[data-test-id='FORMAT-OTHER-KIT-filter']",
        "Painting or Graphic": "[data-test-id='FORMAT-OTHER-PAINTING-filter']",
        "Atlas": "[data-test-id='FORMAT-OTHER-ATLAS-filter']",
        "Mixed Material": "[data-test-id='FORMAT-OTHER-MIXED_MATERIAL-filter']",
        "Online Images": "[data-test-id='FORMAT-OTHER-ONLINE_IMAGE-filter']",
        "Manuscript or Typescript": "[data-test-id='FORMAT-OTHER-MANUSCRIPT-filter']",
        "Device": "[data-test-id='FORMAT-OTHER-DEVICE-filter']",
        "Equipment": "[data-test-id='FORMAT-OTHER-EQUIPMENT-filter']",
        "Pass": "[data-test-id='FORMAT-OTHER-PASS-filter']",
        "Toy": "[data-test-id='FORMAT-OTHER-TOY-filter']",
        "Photographs": "[data-test-id='FORMAT-OTHER-PHOTOGRAPH-filter']",
                                     }

    _empty_search_locator = (By.CSS_SELECTOR, "div.empty-search-sub-title")
    _search_title_container_locator = (By.CSS_SELECTOR, "[data-test-id='search-title'] > [div='search-term']")
    _search_title_locator = (By.CSS_SELECTOR, "[data-test-id='searchTitle']")
    _did_you_mean_search_locator = (By.CSS_SELECTOR, ".did-you-mean-suggestion-link>span") #"[data-test-id='search-title'] span.cp-did-you-mean a span"
    _clear_filters_empty_search_locator = (By.CSS_SELECTOR, "button.cp-btn.btn.cp-filter-token.clear-token") #"p[class=empty-search-clear-filters] a[href*=clear-filters]"
    _availability_details_overlay = (By.CSS_SELECTOR, "[data-test-id='overlay-container']") #"[data-test-id='overlay-body']")
    _pagination_text_locator = (By.CSS_SELECTOR, "[data-key='pagination-text']")
    _search_filter_buttons_locator = (By.CSS_SELECTOR, "button[data-test-id*='-active-filter']")

    @property
    def loaded(self):
        match = re.search(r"(\/v2\/search\?)", self.driver.current_url)
        if match is not None:
            return True
        else:
            return False

    @property
    def overlay(self):
        return Overlay(self)

    @property
    def search_result_items(self):
        return [SearchResultItem(self, element) for element in self.find_elements(*self._search_result_item_locator)]

    @property
    def active_filter_toggle_enabled(self):
        return self.find_element(*self._search_active_filter_enabled_locator)

    @property
    def active_filter_toggle_disabled(self):
        return self.find_element(*self._search_active_filter_disabled_locator)

    def filter_search_results(self, search_filter):
        return self.find_element(By.CSS_SELECTOR, self._search_filter_switch_locator.get(search_filter))

    @property
    def did_you_mean_link(self):
        return self.find_element(*self._did_you_mean_search_locator)

    @property
    def clear_filters_empty_results(self):
        return self.find_element(*self._clear_filters_empty_search_locator)

    @property
    def empty_search_result(self):
        return self.find_element(*self._empty_search_locator)

    @property
    def search_title(self):
        return self.find_element(*self._search_title_locator)

    @property
    def pagination_text(self):
        return self.find_element(*self._pagination_text_locator)

    @property
    def search_filter_buttons(self):
        return self.find_elements(*self._search_filter_buttons_locator)

    @property
    def search_item_availability_overlay(self):
        return self.find_element(*self._availability_details_overlay)

    @property
    def shelves_availability_header(self):
        return self.find_element(*self._shelves_availability_header_locator)

    @property
    def shelves_availability_back_link(self):
        return self.find_element(*self._shelves_availability_back_link_locator)

    @property
    def is_no_titles_text_displayed(self):
        try:
            return self.find_element(*self._no_titles_text_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def no_titles_text(self):
        return self.find_element(*self._no_titles_text_locator)
