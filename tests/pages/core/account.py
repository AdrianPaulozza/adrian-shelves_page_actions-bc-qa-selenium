from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.base import BasePage

# https://<library>.<environment>.bibliocommons.com/account/{section}
class AccountPage(BasePage):

    URL_TEMPLATE = "/account/"

    _heading_locator = (By.ID, "content-start") # "My Settings" heading
    _change_pickup_location_locator = (By.CSS_SELECTOR, "[href='/account/pickup_location']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def change_pickup_location(self):
        return self.find_element(*self._change_pickup_location_locator)
