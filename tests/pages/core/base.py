from pypom import Page, Region
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException, NoSuchElementException, WebDriverException
from selenium.webdriver.common.keys import Keys
import pretty_errors
import time

class BasePage(Page):

    @property
    def header(self):
        return self.Header(self)

    class Header(Region):

        _login_state_user_logged_out_locator = (By.CSS_SELECTOR, "[data-test-id='user_state_logged_out']")
        _log_in_button_locator = (By.CSS_SELECTOR, "[testid='biblionav_login']")
        _login_state_user_logged_in_locator = (By.CLASS_NAME, "logged_in")
        _log_out_locator = (By.CSS_SELECTOR, "[testid='biblionav_logout']")
        _collapsible_search_trigger_locator = (By.CLASS_NAME, "header_collapsible_search_trigger")
        _main_search_input_locator = (By.CSS_SELECTOR, "[testid='main_search_input']")
        # _main_search_button_locator = (By.CSS_SELECTOR, "[type='submit']")
        _advanced_search_locator = (By.CSS_SELECTOR, "[test_id='link_advancedsearch']")
        _my_library_dashboard_locator = (By.CSS_SELECTOR, "[testid='biblionav_mylibrary']")
        _on_hold_locator = (By.CSS_SELECTOR, "[testid='biblionav_holds']")
        _fees_locator = (By.CSS_SELECTOR, "[testid='biblionav_fines']")
        _for_later_shelf_locator = (By.CSS_SELECTOR, "[testid='biblionav_shelves_future']")
        _completed_shelf_locator = (By.CSS_SELECTOR, "[testid='biblionav_shelves_past']")
        _in_progress_shelf_locator = (By.CSS_SELECTOR, "[testid='biblionav_shelves_present']")
        _my_collections_lists_locator = (By.CSS_SELECTOR, "[test_id='biblionav_lists']")
        _my_settings_locator = (By.CSS_SELECTOR, "[testid='biblionav_mysettings']")
        # _search_type_locator = (By.CSS_SELECTOR, "[data-test-id='header-search-type-dropdown']")
        _search_type_locator = (By.CSS_SELECTOR, "[id='desktop_search_form'] [data-test-id='header-search-type-dropdown']")
        # Terrible identifier but not much else that can be used currently...
        _search_type_author_locator = (By.CSS_SELECTOR, "li[data-original-index='2'] > a")
        _messages_locator = (By.CSS_SELECTOR, "[testid='biblionav_messages']")
        _browse_locator = (By.CSS_SELECTOR, "a[href='#browse_menu']")
        _explore_locator = (By.CSS_SELECTOR, "a[href='#explore_menu']")
        _new_titles_locator = (By.CSS_SELECTOR, "a[href*='/explore/recent_arrivals']")
        _staff_picks_locator = (By.CSS_SELECTOR, "a[href*='/explore/featured_lists/staff_picks']")
        _admin_dropdown_locator = (By.CLASS_NAME, "admin_nav_trigger")
        _admin_suggested_purchases_locator = (By.CSS_SELECTOR, "a[href*='/bcadmin/suggested_purchases']")

        _navigation_menu_toggle_locator = (By.CSS_SELECTOR, "[data-test-id*='user_state_logged_']")

        # @property
        # def login_state_user_logged_out(self):
        #     return self.find_element(*self._login_state_user_logged_out_locator)
        #
        @property
        def login_state_user_logged_in(self):
            return self.find_element(*self._login_state_user_logged_in_locator)

        @property
        def my_library_dashboard(self):
            return self.find_element(*self._my_library_dashboard_locator)

        @property
        def on_hold(self):
            return self.find_element(*self._on_hold_locator)

        @property
        def fees(self):
            self.wait.until(lambda element_displayed: self.find_element(*self._fees_locator).is_displayed())
            return self.find_element(*self._fees_locator)

        @property
        def for_later_shelf(self):
            return self.find_element(*self._for_later_shelf_locator)

        @property
        def completed_shelf(self):
            return self.find_element(*self._completed_shelf_locator)

        @property
        def in_progress_shelf(self):
            return self.find_element(*self._in_progress_shelf_locator)

        @property
        def my_collections_lists(self):
            return self.find_element(*self._my_collections_lists_locator)

        @property
        def my_settings(self):
            return self.find_element(*self._my_settings_locator)

        def log_in(self, user_name, password):
            self.find_element(*self._login_state_user_logged_out_locator).click()
            self.find_element(*self._log_in_button_locator).click()
            from pages.core.login import LoginPage
            login_page = LoginPage(self.driver)
            login_page.username_or_barcode.send_keys(user_name)
            login_page.password.send_keys(password)
            login_page.log_in_button.click()
            self.wait.until(lambda s: self.is_element_present(*self._login_state_user_logged_in_locator))

        def log_out(self):
            # _login_state_user_logged_in_locator = (By.CLASS_NAME, "logged_in")
            # WebDriverWait(self.driver, 3).until(EC.element_to_be_clickable((By.CLASS_NAME, "logged_in")))
            # try:
            #     self.find_element(*self._login_state_user_logged_in_locator).click()
            # except WebDriverException:
            #     self.find_element(*self._login_state_user_logged_in_locator).click()
            # self.find_element(*self._log_out_locator).click()

            self.navigation_menu_toggle.click()
            wait = WebDriverWait(self.driver, 5, poll_frequency=1, ignored_exceptions=[WebDriverException])
            log_out_link = self.find_element(*self._log_out_locator)
            wait.until(lambda condition: log_out_link.is_enabled())
            log_out_link.click()
            # Wait until the navigation menu returns to the "Logged Out" state:
            WebDriverWait(self.driver, 5).until(
                lambda condition: self.navigation_menu_toggle.get_attribute('data-test-id') == 'user_state_logged_out'
            )


        @property
        def collapsible_search_trigger(self):
            return self.find_element(*self._collapsible_search_trigger_locator)

        @property
        def main_search_input(self):
            return self.find_element(*self._main_search_input_locator)

        # @property
        # def main_search_button(self):
        #     return self.find_element(*self._main_search_button_locator)

        @property
        def is_collapsible_search_trigger_displayed(self):
            try:
                return self.find_element(*self._collapsible_search_trigger_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def advanced_search(self):
            return self.find_element(*self._advanced_search_locator)

        @property
        def search_type(self):
            return self.find_element(*self._search_type_locator)

        @property
        def search_type_author(self):
            return self.search_type.find_element(*self._search_type_author_locator)

        @property
        def messages(self):
            return self.find_element(*self._messages_locator)

        @property
        def browse(self):
            return self.find_element(*self._browse_locator)

        @property
        def explore(self):
            return self.find_element(*self._explore_locator)

        @property
        def new_titles(self):
            self.wait.until(lambda link_displayed: self.find_element(*self._new_titles_locator).is_displayed())
            return self.find_element(*self._new_titles_locator)

        @property
        def staff_picks(self):
            self.wait.until(lambda link_displayed: self.find_element(*self._staff_picks_locator).is_displayed())
            return self.find_element(*self._staff_picks_locator)

        @property
        def admin_dropdown(self):
            return self.find_element(*self._admin_dropdown_locator)

        @property
        def admin_suggested_purchases(self):
            return self.find_element(*self._admin_suggested_purchases_locator)

        @property
        def is_admin_suggested_purchases_displayed(self):
            try:
                return self.find_element(*self._admin_suggested_purchases_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def navigation_menu_toggle(self):
            return self.find_element(*self._navigation_menu_toggle_locator)

        def set_search_type_to(self, type):
            self.search_type.click()
            WebDriverWait(self.driver, 3).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "[id='desktop_search_form'] li[data-original-index='1']")))
            options = self.search_type.find_elements(By.CSS_SELECTOR, "li[data-original-index]")
            for option in options:
                if option.text == type:
                    option.click()
                    break

        def search_for(self, term, advanced_search = False):
            # Check if the collapsible search trigger that some libraries have configured
            # e.g (chipublib) is visible and if so click it first:
            if self.is_collapsible_search_trigger_displayed:
                self.collapsible_search_trigger.click()
            # If the 'advanced_search' argument is set to True (default: False), initiate The
            # Advanced Search flow instead:
            if advanced_search:
                self.advanced_search.click()
                from pages.core.search import SearchPage
                search_page = SearchPage(self.driver)
                search_page.search_query.send_keys(" ") # type a single space to trigger the alert
                try:
                    WebDriverWait(self.driver, 3).until(EC.alert_is_present())
                    alert = self.driver.switch_to.alert
                    alert.accept()
                    search_page.search_query.send_keys(term)
                except TimeoutException:
                    print("The 'If you edit the query directly, the form below will be disabled.' alert did NOT appear.")
                pre_search_URL = self.driver.current_url
                search_page.search_button.click()
            else:
                pre_search_URL = self.driver.current_url
                self.main_search_input.send_keys(term, Keys.RETURN)
                # self.main_search_button.click()
            WebDriverWait(self.driver, 10).until(lambda wait_for_URL_change: self.driver.current_url != pre_search_URL)
            if "/v2/search" in self.driver.current_url:
                from pages.core.v2.search_results import SearchResultsPage
                search_results_page = SearchResultsPage(self.driver)
                search_results_page.wait_for_page_to_load
                return search_results_page
            elif "/user_profile/" in self.driver.current_url:
                from pages.core.user_profile import UserProfilePage
                return UserProfilePage(self.driver)

        def clear_search(self):
            if self.is_collapsible_search_trigger_displayed:
                self.collapsible_search_trigger.click()

            self.main_search_input.clear()

        def navigation_menu(self, option):
            # .header.navigation_menu('On Hold'):
            if option == 'On Hold':
                self.navigation_menu_toggle.click()
                WebDriverWait(self.driver, 5).until(lambda condition: self.on_hold.is_displayed())
                self.on_hold.click()
                from pages.core.v2.holds import HoldsPage
                return HoldsPage(self.driver)
