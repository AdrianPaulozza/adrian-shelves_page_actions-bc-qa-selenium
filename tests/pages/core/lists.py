from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from .base import BasePage
from pypom import Region

# https://<library>.<environment>.bibliocommons.com/lists/show/[UserID?]
class ListsPage(BasePage):
    _my_lists_header_locator = (By.CSS_SELECTOR, "[testid='user_header_context']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._my_lists_header_locator).is_displayed()
        except NoSuchElementException:
            return False

    _draft_list_locator = (By.CSS_SELECTOR, "div[data-js='draft_lists_table'] tr")
    # _published_list_locator = (By.CSS_SELECTOR, "tr[data-test-id*='published-list-']")
    _published_list_locator = (By.CSS_SELECTOR, "table[class='table table-striped lists_table'] tr")

    # Draft and Published Lists share the same controls so we define one Region where we
    # specify relevant elements, generically calling it 'List':
    class List(Region):
        _name_locator = (By.CLASS_NAME, " list_name_column")
        # _type_locator
        # _date_locator
        # _size_locator
        _edit_locator = (By.CSS_SELECTOR, "a[testid*='edit_list_button_']")
        _delete_locator = (By.CSS_SELECTOR, "button[data-test-id*='delete-list-button-']")

        @property
        def name(self):
            return self.find_element(*self._name_locator)

        @property
        def edit(self):
            return self.find_element(*self._edit_locator)

        @property
        def delete(self):
            return self.find_element(*self._delete_locator)

    @property
    def draft_lists(self):
        return [self.List(self, element) for element in self.find_elements(*self._draft_list_locator)]

    @property
    def published_lists(self):
        return [self.List(self, element) for element in self.find_elements(*self._published_list_locator)]
