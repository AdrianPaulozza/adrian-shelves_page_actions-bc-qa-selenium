from selenium.webdriver.common.by import By
from .base import BasePage

class SearchPage(BasePage):

    _search_query_locator = (By.CSS_SELECTOR, "[testid='field_boolquery']")
    _search_button_locator = (By.CSS_SELECTOR, "[testid ='button_boolsearch']")

    @property
    def search_query(self):
        return self.find_element(*self._search_query_locator)

    @property
    def search_button(self):
        return self.find_element(*self._search_button_locator)
