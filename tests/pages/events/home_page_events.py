from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.base import BasePage
from pypom import Region


class EventsHome(BasePage):

    URL_TEMPLATE = "/events"

    _search_textbox_locator = (By.CSS_SELECTOR, "[testid='main_search_input']")
    _search_box_icon_locator = (By.CSS_SELECTOR, ".header_collapsible_search_trigger")
    _show_more_locator = (By.CSS_SELECTOR, ".btn-lg")
    _search_results_locator = (By.CSS_SELECTOR, "div.row:nth-child(12)")
    _search_textbox_icon_locator = (By.CSS_SELECTOR, "div.header_search_box:nth-child(2)>span:nth-child(3)>button:nth-child(1)")
    _search_for_another_locator = (By.CSS_SELECTOR, ".clear-filters-btn")
    _header_hours_and_location_link_locator = (By.CSS_SELECTOR, "#header_locations_dropdown_trigger [aria-hidden='true']:nth-child(2)")
    _header_hours_and_location_textbox_locator = (By.CSS_SELECTOR, "[name='near_addr']")
    _header_hours_and_location_search_link_locator = (By.CSS_SELECTOR, ".icon-search")
    _header_hours_and_location_all_location_link_locator = (By.CSS_SELECTOR, "[testid='header_locations_see_all']")
    _clear_filters_search_result_link_locator = (By.CSS_SELECTOR, ".secondary-link")
    _number_of_search_results_locator = (By.CSS_SELECTOR, ".last-word")
    _events_locator = (By.CSS_SELECTOR, "div.row.event-row")

    @property
    def search_box_icon(self):
        return self.find_element(*self._search_box_icon_locator)

    @property
    def is_show_more_displayed(self):
        try:
            return self.find_element(*self._show_more_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def search_textbox(self):
        return self.find_element(*self._search_textbox_locator)

    @property
    def search_textbox_icon(self):
        return self.find_element(*self._search_textbox_icon_locator)

    @property
    def search_for_another(self):
        return self.find_element(*self._search_for_another_locator)

    @property
    def is_search_for_another_displayed(self):
        try:
            return self.find_element(*self._search_for_another_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def hours_and_location_link(self):
        return self.find_element(*self._header_hours_and_location_link_locator)

    @property
    def is_hours_and_location_link_displayed(self):
        try:
            return self.find_element(*self._header_hours_and_location_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def hours_and_location_textbox(self):
        return self.find_element(*self._header_hours_and_location_textbox_locator)

    @property
    def is_hours_and_location_textbox_displayed(self):
        try:
            return self.find_element(*self._header_hours_and_location_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def hours_and_location_search_link(self):
        return self.find_element(*self._header_hours_and_location_search_link_locator)

    @property
    def is_hours_and_location_search_link_displayed(self):
        try:
            return self.find_element(*self._header_hours_and_location_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def hours_and_location_all_location(self):
        return self.find_element(*self._header_hours_and_location_all_location_link_locator)

    @property
    def is_clear_filters_search_result_link_displayed(self):
        try:
            return self.find_element(*self._clear_filters_search_result_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def events(self):
        return [self.EventRow(self, element) for element in self.find_elements(*self._events_locator)]

    class EventRow(Region):

        _title_locator = (By.CSS_SELECTOR, "div.row.event-row > div > div > div > h2 > a")

        @property
        def title(self):
            return self.find_element(*self._title_locator)
