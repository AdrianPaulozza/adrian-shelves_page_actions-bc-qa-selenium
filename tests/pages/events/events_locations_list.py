from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.base import BasePage


class EventLocationListPage(BasePage):

    _library_location_information_list_locator = (By.CSS_SELECTOR, ".location-information.row")
    _list_dropdown_locator = (By.CSS_SELECTOR, "[name='location-byname']")
    _library_list_has_hours_locator = (By.CSS_SELECTOR, ".col-md-4.location-hours")
    _library_phone_numbers_locators = (By.CSS_SELECTOR, ".location-phone")
    _library_emails_locators = (By.CSS_SELECTOR, ".location-email")
    _library_features_and_facilities_locator = (By.CSS_SELECTOR, ".location-facilities")
    _first_location_link_locator = (By.CSS_SELECTOR, "[itemprop='name']")
    _detail_location_page_heading_locator = (By.CSS_SELECTOR, ".o-heading--giant")

    @property
    def is_events_location_list_dropdown_displayed(self):
        try:
            return self.find_element(*self._list_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def location_information_list(self):
        return self.find_elements(*self._library_location_information_list_locator)

    @property
    def is_location_information_list_displayed(self):
        try:
            return self.find_element(*self._library_location_information_list_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def library_list_of_hours(self):
        return self.find_elements(*self._library_list_has_hours_locator)

    @property
    def is_library_list_of_hours_displayed(self):
        try:
            return self.find_element(*self._library_list_has_hours_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def library_emails_list(self):
        return self.find_elements(*self._library_emails_locators)

    @property
    def is_library_emails_list_displayed(self):
        try:
            return self.find_element(*self._library_emails_locators).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def library_phone_numbers_list(self):
        return self.find_elements(*self._library_phone_numbers_locators)

    @property
    def is_library_phone_numbers_list_displayed(self):
        try:
            return self.find_element(*self._library_phone_numbers_locators).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def library_features_and_facilities_list(self):
        return self.find_elements(*self._library_features_and_facilities_locator)

    @property
    def is_library_features_and_facilities_displayed(self):
        try:
            return self.find_element(*self._library_features_and_facilities_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_detail_location_page_heading_displayed(self):
        try:
            return self.find_element(*self._detail_location_page_heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def detail_location_page_heading(self):
        return self.find_element(*self._detail_location_page_heading_locator)

    @property
    def first_location_link(self):
        return self.find_elements(*self._first_location_link_locator)
