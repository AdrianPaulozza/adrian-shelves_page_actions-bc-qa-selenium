from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.base import BasePage
from tenacity import*


class AdminEventPage(BasePage):

    _admin_link_locator = (By.CSS_SELECTOR, ".admin-link .primary-link")
    _new_revised_locator = (By.CSS_SELECTOR, "[data-ember-action='1558']")
    _previous_page_locator = (By.CSS_SELECTOR, ".glyphicon-chevron-left")
    _create_event_button_locator = (By.CSS_SELECTOR, ".btn-info")
    _title_textbox_locator = (By.CSS_SELECTOR, ".ember-view.ember-text-field.form-control.event-edit-title")
    _type_dropdown_locator = (By.CSS_SELECTOR, "ul.chosen-choices")
    _type_dropdown_business_locator = (By.CSS_SELECTOR, "li.active-result:nth-child(5)")
    _audience_dropdown_locator = (By.CSS_SELECTOR, "Select[data-placeholder='Select Audiences'] + div")
    _audience_dropdown_families_locator = (By.CSS_SELECTOR, "[data-option-array-index='3']")
    _description_field_locator = (By.CSS_SELECTOR, ".redactor-in-0")
    _location_dropdown_locator = (By.CSS_SELECTOR, "div.controls.location-section")
    _location_dropdown_archer_height = (By.CSS_SELECTOR, "div.controls.location-section *:nth-child(4)")
    _start_date_button_locator = (By.CSS_SELECTOR, ".ember-view.form-control.start-date.picker__input")
    _calendar_next_month_button_locator = (By.CSS_SELECTOR, ".picker__nav--next")
    _calendar_start_end_date_select_locator = (By.CSS_SELECTOR, ".picker__day.picker__day--infocus")
    _end_date_button_locator = (By.CSS_SELECTOR, ".ember-view.form-control.expire-date.picker__input")
    _time_selector_locator = (By.CSS_SELECTOR, "input[class*='start-time']")
    _start_time_selector_locator = (By.CSS_SELECTOR, ".picker__list-item")
    _save_and_publish_locator = (By.CSS_SELECTOR, ".ember-view.progress-button.btn.btn-success.btn-publish")
    _event_link_text_locator = (By.CSS_SELECTOR, ".event-title .ember-view")
    _event_link_text_test_C67848_locator = (By.PARTIAL_LINK_TEXT, "test_C67848")
    _published_tab_locator = (By.PARTIAL_LINK_TEXT, "Published")
    _admin_events_search_textbox_locator = (By.CSS_SELECTOR, ".ember-view.ember-text-field.form-control.form-control-enhanced")
    _admin_event_title_locator = (By.CSS_SELECTOR, ".admin-summary-title .ember-view")
    _admin_delete_buttons_locator = (By.CSS_SELECTOR, "[role='row'] :nth-of-type(2) .events-admin-action:nth-of-type(7) .primary-link")
    _confirm_delete_button_locator = (By.CSS_SELECTOR, ".progress-button")
    _close_delete_overlay_button_locator = (By.CSS_SELECTOR, "span.glyphicon.glyphicon-remove-2")
    _no_results_displayed_text_locator = (By.CSS_SELECTOR, "div.well.well-lg.no-results")
    _left_chevron_admin_events_page_button_locator = (By.CSS_SELECTOR, ".glyphicon-chevron-left")
    _events_location_show_locations_with_filter_button_locator = (By.CSS_SELECTOR, "[data-bindattr-654]")
    _event_creation_page_this_event_happens_dropdown_locator = (By.CSS_SELECTOR, ".ember-view.ember-select.cp-styled-selector.cp-generator-selector")
    _event_creation_page_this_event_happens_daily_locator = (By.CSS_SELECTOR, "[value='daily']")
    _add_another_date_date_time_pattern_button_locator = (By.CSS_SELECTOR, ".add-pattern-btn")
    _delete_this_and_subsequent_locator = (By.CSS_SELECTOR, "[role='row']:nth-of-type(2) .events-admin-action:nth-of-type(7) [role='presentation']:nth-of-type(3) .ember-view")
    _search_progress_spinner_locator = (By.CSS_SELECTOR, ".progress-spinner")
    _exclusion_buttons_locator = (By.CSS_SELECTOR, "button.btn-exclusions")
    _exclusion_list_items_locator = (By.CSS_SELECTOR, ".exclusion-checkbox")
    _exclusion_done_button_locator = (By.CSS_SELECTOR, "button.btn.btn-default")
    _date_and_time_published_tab_locator = (By.CSS_SELECTOR, "dd")

    @property
    def admin_link(self):
        return self.find_element(*self._admin_link_locator)

    @property
    def new_revised(self):
        return self.find_element(*self._new_revised_locator)

    @property
    def is_new_revised_is_displayed(self):
        try:
            return self.find_element(*self._new_revised_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_previous_page_icon_displayed(self):
        try:
            return self.find_element(*self._previous_page_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def create_event_button(self):
        return self.find_element(*self._create_event_button_locator)

    @property
    def title_textbox(self):
        return self.find_element(*self._title_textbox_locator)

    @property
    def is_title_textbox_displayed(self):
        try:
            return self.find_element(*self._title_textbox_locator).is_displayed()
        except NoSuchElementException:
            return False

    # this covers type, program, audience, language and date picker drop-downs
    @property
    def type_dropdown(self):
        return self.find_element(*self._type_dropdown_locator)

    @property
    def type_dropdown_business(self):
        return self.find_element(*self._type_dropdown_business_locator)

    @property
    def is_type_dropdown_business_displayed(self):
        try:
            return self.find_element(*self._type_dropdown_business_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def audience_dropdown(self):
        return self.find_element(*self._audience_dropdown_locator)

    @property
    def is_type_audience_dropdown_displayed(self):
        try:
            return self.find_element(*self._audience_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_type_audience_dropdown_families_displayed(self):
        try:
            return self.find_element(*self._audience_dropdown_families_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def type_audience_dropdown_families(self):
        return self.find_element(*self._audience_dropdown_families_locator)

    @property
    def description_field(self):
        return self.find_element(*self._description_field_locator)

    @property
    def location_dropdown(self):
        return self.find_element(*self._location_dropdown_locator)

    @property
    def is_location_dropdown_displayed(self):
        return self.find_element(*self._location_dropdown_locator).is_displayed()

    @property
    def location_dropdown_archer(self):
        return self.find_element(*self._location_dropdown_archer_height)

    @property
    def is_location_dropdown_archer_height_displayed(self):
        try:
            return self.find_element(*self._location_dropdown_archer_height).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def time_selector(self):
        return self.find_element(*self._time_selector_locator)

    @property
    def is_time_selector_displayed(self):
        try:
            return self.find_element(*self._time_selector_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def start_time(self):
        return self.find_elements(*self._start_time_selector_locator)

    @property
    def is_save_and_publish_displayed(self):
        try:
            return self.find_element(*self._save_and_publish_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def time_selectors(self):
        return self.find_elements(*self._time_selector_locator)

    @property
    def save_and_publish(self):
        return self.find_element(*self._save_and_publish_locator)

    @property
    def event_name_last_word(self):
        return self.find_element(*self._event_link_text_locator)

    @property
    def is_event_name_last_word_displayed(self):
        try:
            return self.find_element(*self._event_link_text_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def event_name_last_words(self):
        return self.find_elements(*self._event_link_text_locator)

    @property
    def admin_published_tab(self):
        return self.find_element(*self._published_tab_locator)

    @property
    def is_admin_published_tab_displayed(self):
        return self.find_element(*self._published_tab_locator)

    @property
    def admin_events_search_textbox(self):
        return self.find_element(*self._admin_events_search_textbox_locator)

    @property
    def is_admin_events_search_textbox_displayed(self):
        try:
            return self.find_element(*self._admin_events_search_textbox_locator).is_displayed()

        except NoSuchElementException:
            return False

    @property
    def admin_event_title(self):
        return self.find_element(*self._admin_event_title_locator)

    @property
    def is_admin_event_title_displayed(self):
        try:
            return self.find_element(*self._admin_event_title_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def admin_delete_button(self):
        return self.find_element(*self._admin_delete_buttons_locator)

    @property
    def admin_delete_buttons(self):
        return self.find_elements(*self._admin_delete_buttons_locator)

    @property
    def is_admin_delete_button_displayed(self):
        try:
            return self.find_element(*self._admin_delete_buttons_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def admin_event_titles(self):
        return self.find_elements(*self._admin_event_title_locator)

    @property
    def confirm_delete_button(self):
        return self.find_element(*self._confirm_delete_button_locator)

    @property
    def is_confirm_delete_button_displayed(self):
        try:
            return self.find_element(*self._confirm_delete_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def close_delete_overlay_button(self):
        return self.find_element(*self._close_delete_overlay_button_locator)

    @property
    def is_close_delete_overlay_button_displayed(self):
        try:
            return self.find_element(*self._close_delete_overlay_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def no_results_displayed(self):
        return self.find_element(*self._no_results_displayed_text_locator).text

    def assert_is_no_results_displayed(self):
        try:
            return self.find_element(*self._no_results_displayed_text_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_left_chevron_button_displayed(self):
        try:
            return self.find_element(*self._left_chevron_admin_events_page_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def event_creation_page_this_event_happens_dropdown(self):
        return self.find_element(*self._event_creation_page_this_event_happens_dropdown_locator)

    @property
    def is_event_creation_page_this_event_happens_dropdown_displayed(self):
        try:
            return self.find_element(*self._event_creation_page_this_event_happens_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def start_date_button(self):
        return self.find_element(*self._start_date_button_locator)

    @property
    def is_event_start_date_button_displayed(self):
        try:
            return self.find_element(*self._start_date_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def calendar_next_month_button(self):
        return self.find_elements(*self._calendar_next_month_button_locator)

    @property
    def calendar_start_end_date_select(self):
        return self.find_elements(*self._calendar_start_end_date_select_locator)

    @property
    def end_date_button(self):
        return self.find_element(*self._end_date_button_locator)

    @property
    def is_end_date_button_displayed(self):
        try:
            return self.find_element(*self._end_date_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def add_another_date_date_time_pattern(self):
        return self.find_element(*self._add_another_date_date_time_pattern_button_locator)

    @property
    def is_add_another_date_date_time_pattern_displayed(self):
        try:
            return self.find_element(*self._add_another_date_date_time_pattern_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def this_event_happens_dropdown_pattern_2_find(self):
        return self.find_elements(*self._event_creation_page_this_event_happens_dropdown_locator)

    @property
    def event_creation_page_this_event_happens_dropdown_pattern_2(self):
        return self.this_event_happens_dropdown_pattern_2_find[1]

    @property
    def is_event_creation_page_this_event_happens_dropdown_pattern_2_displayed(self):
        try:
            return self.event_creation_page_this_event_happens_dropdown_pattern_2.is_displayed()
        except NoSuchElementException:
            return False

    @property
    def start_date_button_2nd_find(self):
        return self.find_elements(*self._start_date_button_locator)

    @property
    def end_date_button_2nd_find(self):
        return self.find_elements(*self._end_date_button_locator)

    @property
    def delete_this_and_subsequent(self):
        return self.find_element(*self._delete_this_and_subsequent_locator)

    @property
    def exclusion_buttons(self):
        return self.find_element(*self._exclusion_buttons_locator)

    @property
    def is_exclusion_buttons_displayed(self):
        try:
            return self.exclusion_buttons.is_displayed()
        except NoSuchElementException:
            return False

    @property
    def exclusion_buttons_find(self):
        return self.find_elements(*self._exclusion_list_items_locator)

    @property
    def exclusion_done_buttons_find(self):
        return self.find_elements(*self._exclusion_done_button_locator)

    @property
    def search_progress_spinner(self):
        return self.find_element(*self._search_progress_spinner_locator)

    @property
    def is_search_progress_spinner_displayed(self):
        try:
            return self.search_progress_spinner.is_displayed()
        except NoSuchElementException:
            return False

    @property
    def date_and_time_published_tab(self):
        return self.find_elements(*self._date_and_time_published_tab_locator)

    @retry(stop=stop_after_attempt(10), wait=wait_fixed(1))
    def retry_search_until_results_appear(self, title_name):
        self.admin_events_search_textbox.clear()
        self.admin_events_search_textbox.send_keys(title_name)
        events_name = self.admin_event_title.text
        self.wait.until(lambda s: events_name == title_name)
        if events_name != title_name:
            raise Exception
