from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.base import BasePage


class EventLocationPage(BasePage):

    _invalid_search_locator = (By.CSS_SELECTOR, ".locations-search-error")
    _next_page_button_locator = (By.CSS_SELECTOR, "button.pagination-button.locations-search-next-button.clear-border")
    _search_results_has_get_direction_links_locator = (By.CSS_SELECTOR, ".ember-view.directions-link")
    _library_information_locator = (By.CSS_SELECTOR, ".col-md-12.location-summary")
    _google_pin_clicked_library_name_locator = (By.CSS_SELECTOR, "a.primary-link")
    _google_pin_expanded_get_direction_link_locator = (By.CSS_SELECTOR, "[role='dialog'] [target]")
    _first_library_in_list_locator = (By.CSS_SELECTOR, ".ember-view.location-status.location-status-outline")
    _austin_library_google_pin_locator = (By.CSS_SELECTOR, "map#gmimap3")
    _google_map_frame = (By.CSS_SELECTOR, ".locations-map [role='application']")
    _a_to_z_locations_link_locator = (By.CSS_SELECTOR, ".locations-all-link")
    _google_pin_expanded_email_locator = (By.CSS_SELECTOR, ".location-email li")
    _show_locations_with_filter_button_locator = (By.CSS_SELECTOR, ".glyphicon.glyphicon-expand")
    _show_locations_list_with_checkbox_locator = (By.CSS_SELECTOR, ".ember-view.ember-checkbox")
    _show_locations_list_selected_clear_filters_link_locator = (By.CSS_SELECTOR, "#clear-filters")
    _search_results_library_name_locator = (By.CSS_SELECTOR, ".locations-search-name")
    _search_results_returned_count_locator = (By.CSS_SELECTOR, ".results-title")

    @property
    def events_location_invalid_search_text(self):
        return self.find_element(*self._invalid_search_locator)

    @property
    def is_events_location_invalid_search_displayed(self):
        try:
            return self.find_element(*self._invalid_search_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_location_next_page_button_displayed(self):
        try:
            return self.find_element(*self._next_page_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def location_library_information(self):
        return self.find_elements(*self._library_information_locator)

    @property
    def is_google_pin_expanded_library_name_displayed(self):
        try:
            return self.find_element(*self._google_pin_clicked_library_name_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def assert_is_events_location_google_pin_expanded_get_direction_displayed(self):
        try:
            return self.find_element(*self._google_pin_expanded_get_direction_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_events_location_first_library_in_list_displayed(self):
        try:
            return self.find_element(*self._first_library_in_list_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def austin_library_google_pin(self):
        return self.find_element(*self._austin_library_google_pin_locator)

    @property
    def is_austin_library_google_pin_displayed(self):
        try:
            return self.find_element(*self._austin_library_google_pin_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def google_map_frame(self):
        return self.find_element(*self._google_map_frame)

    @property
    def a_to_z_locations_link(self):
        return self.find_element(*self._a_to_z_locations_link_locator)

    @property
    def is_events_location_google_pin_expanded_email_displayed(self):
        try:
            return self.find_element(*self._google_pin_expanded_email_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_show_locations_with_filter_button_displayed(self):
        try:
            return self.find_element(*self._show_locations_with_filter_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def locations_with_filter_button(self):
        return self.find_element(*self._show_locations_with_filter_button_locator)

    @property
    def show_locations_list_with_checkbox(self):
        return self.find_elements(*self._show_locations_list_with_checkbox_locator)

    @property
    def is_show_locations_list_selected_clear_filters_link_displayed(self):
        try:
            return self.find_element(*self._show_locations_list_selected_clear_filters_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def library_in_list_name(self):
        return self.find_elements(*self._search_results_library_name_locator)

    @property
    def location_search_results_has_get_direction_links(self):
        return self.find_elements(*self._search_results_has_get_direction_links_locator)

    @property
    def search_results_returned_count(self):
        return self.find_element(*self._search_results_returned_count_locator)

    @property
    def is_search_results_returned_count_displayed(self):
        try:
            return self.find_element(*self._search_results_returned_count_locator).is_displayed()
        except NoSuchElementException:
            return False
